﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Thread_.NET.DAL.Migrations
{
    public partial class Add_Reaction_Seed_Data : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
                table: "CommentReactions",
                columns: new[] { "Id", "CommentId", "CreatedAt", "IsLike", "UpdatedAt", "UserId" },
                values: new object[,]
                {
                    { 1, 13, new DateTime(2020, 6, 6, 13, 4, 30, 496, DateTimeKind.Local).AddTicks(2487), false, new DateTime(2020, 6, 6, 13, 4, 30, 496, DateTimeKind.Local).AddTicks(3055), 3 },
                    { 19, 16, new DateTime(2020, 6, 6, 13, 4, 30, 496, DateTimeKind.Local).AddTicks(3923), false, new DateTime(2020, 6, 6, 13, 4, 30, 496, DateTimeKind.Local).AddTicks(3927), 16 },
                    { 18, 3, new DateTime(2020, 6, 6, 13, 4, 30, 496, DateTimeKind.Local).AddTicks(3866), true, new DateTime(2020, 6, 6, 13, 4, 30, 496, DateTimeKind.Local).AddTicks(3870), 14 },
                    { 17, 8, new DateTime(2020, 6, 6, 13, 4, 30, 496, DateTimeKind.Local).AddTicks(3843), false, new DateTime(2020, 6, 6, 13, 4, 30, 496, DateTimeKind.Local).AddTicks(3847), 15 },
                    { 16, 17, new DateTime(2020, 6, 6, 13, 4, 30, 496, DateTimeKind.Local).AddTicks(3818), false, new DateTime(2020, 6, 6, 13, 4, 30, 496, DateTimeKind.Local).AddTicks(3822), 9 },
                    { 15, 18, new DateTime(2020, 6, 6, 13, 4, 30, 496, DateTimeKind.Local).AddTicks(3793), true, new DateTime(2020, 6, 6, 13, 4, 30, 496, DateTimeKind.Local).AddTicks(3797), 11 },
                    { 14, 6, new DateTime(2020, 6, 6, 13, 4, 30, 496, DateTimeKind.Local).AddTicks(3769), true, new DateTime(2020, 6, 6, 13, 4, 30, 496, DateTimeKind.Local).AddTicks(3773), 21 },
                    { 13, 7, new DateTime(2020, 6, 6, 13, 4, 30, 496, DateTimeKind.Local).AddTicks(3745), true, new DateTime(2020, 6, 6, 13, 4, 30, 496, DateTimeKind.Local).AddTicks(3749), 3 },
                    { 12, 6, new DateTime(2020, 6, 6, 13, 4, 30, 496, DateTimeKind.Local).AddTicks(3721), false, new DateTime(2020, 6, 6, 13, 4, 30, 496, DateTimeKind.Local).AddTicks(3726), 4 },
                    { 11, 16, new DateTime(2020, 6, 6, 13, 4, 30, 496, DateTimeKind.Local).AddTicks(3696), true, new DateTime(2020, 6, 6, 13, 4, 30, 496, DateTimeKind.Local).AddTicks(3700), 10 },
                    { 20, 13, new DateTime(2020, 6, 6, 13, 4, 30, 496, DateTimeKind.Local).AddTicks(3948), false, new DateTime(2020, 6, 6, 13, 4, 30, 496, DateTimeKind.Local).AddTicks(3952), 8 },
                    { 9, 2, new DateTime(2020, 6, 6, 13, 4, 30, 496, DateTimeKind.Local).AddTicks(3649), true, new DateTime(2020, 6, 6, 13, 4, 30, 496, DateTimeKind.Local).AddTicks(3653), 10 },
                    { 8, 2, new DateTime(2020, 6, 6, 13, 4, 30, 496, DateTimeKind.Local).AddTicks(3626), true, new DateTime(2020, 6, 6, 13, 4, 30, 496, DateTimeKind.Local).AddTicks(3630), 7 },
                    { 7, 4, new DateTime(2020, 6, 6, 13, 4, 30, 496, DateTimeKind.Local).AddTicks(3601), false, new DateTime(2020, 6, 6, 13, 4, 30, 496, DateTimeKind.Local).AddTicks(3605), 16 },
                    { 6, 16, new DateTime(2020, 6, 6, 13, 4, 30, 496, DateTimeKind.Local).AddTicks(3576), false, new DateTime(2020, 6, 6, 13, 4, 30, 496, DateTimeKind.Local).AddTicks(3580), 11 },
                    { 5, 5, new DateTime(2020, 6, 6, 13, 4, 30, 496, DateTimeKind.Local).AddTicks(3551), true, new DateTime(2020, 6, 6, 13, 4, 30, 496, DateTimeKind.Local).AddTicks(3555), 17 },
                    { 4, 7, new DateTime(2020, 6, 6, 13, 4, 30, 496, DateTimeKind.Local).AddTicks(3526), true, new DateTime(2020, 6, 6, 13, 4, 30, 496, DateTimeKind.Local).AddTicks(3530), 10 },
                    { 3, 18, new DateTime(2020, 6, 6, 13, 4, 30, 496, DateTimeKind.Local).AddTicks(3500), true, new DateTime(2020, 6, 6, 13, 4, 30, 496, DateTimeKind.Local).AddTicks(3505), 10 },
                    { 2, 7, new DateTime(2020, 6, 6, 13, 4, 30, 496, DateTimeKind.Local).AddTicks(3456), true, new DateTime(2020, 6, 6, 13, 4, 30, 496, DateTimeKind.Local).AddTicks(3470), 2 },
                    { 10, 4, new DateTime(2020, 6, 6, 13, 4, 30, 496, DateTimeKind.Local).AddTicks(3673), true, new DateTime(2020, 6, 6, 13, 4, 30, 496, DateTimeKind.Local).AddTicks(3677), 21 }
                });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "Body", "CreatedAt", "UpdatedAt" },
                values: new object[] { "Doloremque nostrum voluptates velit ut enim voluptatem dignissimos ipsum.", new DateTime(2020, 6, 6, 13, 4, 30, 485, DateTimeKind.Local).AddTicks(735), new DateTime(2020, 6, 6, 13, 4, 30, 485, DateTimeKind.Local).AddTicks(1357) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 4, "Libero facilis dolorem aut.", new DateTime(2020, 6, 6, 13, 4, 30, 485, DateTimeKind.Local).AddTicks(2214), 13, new DateTime(2020, 6, 6, 13, 4, 30, 485, DateTimeKind.Local).AddTicks(2231) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 15, "Rerum laudantium excepturi tempore voluptas fugit non maxime.", new DateTime(2020, 6, 6, 13, 4, 30, 485, DateTimeKind.Local).AddTicks(2308), 10, new DateTime(2020, 6, 6, 13, 4, 30, 485, DateTimeKind.Local).AddTicks(2314) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 14, "Sint et nemo sit eligendi ullam quas eos amet magni.", new DateTime(2020, 6, 6, 13, 4, 30, 485, DateTimeKind.Local).AddTicks(2380), 2, new DateTime(2020, 6, 6, 13, 4, 30, 485, DateTimeKind.Local).AddTicks(2385) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 2, "Delectus ut saepe nostrum fugit consectetur atque voluptas ab.", new DateTime(2020, 6, 6, 13, 4, 30, 485, DateTimeKind.Local).AddTicks(2446), 4, new DateTime(2020, 6, 6, 13, 4, 30, 485, DateTimeKind.Local).AddTicks(2450) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 12, "Sint dolorum culpa dicta autem.", new DateTime(2020, 6, 6, 13, 4, 30, 485, DateTimeKind.Local).AddTicks(2498), 8, new DateTime(2020, 6, 6, 13, 4, 30, 485, DateTimeKind.Local).AddTicks(2502) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 7,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 11, "Enim numquam eaque eveniet est hic accusamus totam enim dignissimos.", new DateTime(2020, 6, 6, 13, 4, 30, 485, DateTimeKind.Local).AddTicks(2609), 1, new DateTime(2020, 6, 6, 13, 4, 30, 485, DateTimeKind.Local).AddTicks(2614) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 8,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 14, "Minus nihil temporibus modi ut.", new DateTime(2020, 6, 6, 13, 4, 30, 485, DateTimeKind.Local).AddTicks(2662), 10, new DateTime(2020, 6, 6, 13, 4, 30, 485, DateTimeKind.Local).AddTicks(2667) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 9,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 3, "Magni laudantium porro.", new DateTime(2020, 6, 6, 13, 4, 30, 485, DateTimeKind.Local).AddTicks(2704), 14, new DateTime(2020, 6, 6, 13, 4, 30, 485, DateTimeKind.Local).AddTicks(2709) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 10,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 12, "Cum et omnis vel.", new DateTime(2020, 6, 6, 13, 4, 30, 485, DateTimeKind.Local).AddTicks(2751), 6, new DateTime(2020, 6, 6, 13, 4, 30, 485, DateTimeKind.Local).AddTicks(2755) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 11,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 14, "Molestias facere et repellat.", new DateTime(2020, 6, 6, 13, 4, 30, 485, DateTimeKind.Local).AddTicks(2799), 4, new DateTime(2020, 6, 6, 13, 4, 30, 485, DateTimeKind.Local).AddTicks(2804) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 12,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 21, "Sit blanditiis id sunt ipsum.", new DateTime(2020, 6, 6, 13, 4, 30, 485, DateTimeKind.Local).AddTicks(2848), 16, new DateTime(2020, 6, 6, 13, 4, 30, 485, DateTimeKind.Local).AddTicks(2852) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 13,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "UpdatedAt" },
                values: new object[] { 21, "Autem et qui non officiis tenetur eos placeat quia omnis.", new DateTime(2020, 6, 6, 13, 4, 30, 485, DateTimeKind.Local).AddTicks(2913), new DateTime(2020, 6, 6, 13, 4, 30, 485, DateTimeKind.Local).AddTicks(2918) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 14,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 16, "Perspiciatis provident harum rerum.", new DateTime(2020, 6, 6, 13, 4, 30, 485, DateTimeKind.Local).AddTicks(2985), 5, new DateTime(2020, 6, 6, 13, 4, 30, 485, DateTimeKind.Local).AddTicks(3144) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 15,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 10, "Tenetur quis modi officia.", new DateTime(2020, 6, 6, 13, 4, 30, 485, DateTimeKind.Local).AddTicks(3183), 4, new DateTime(2020, 6, 6, 13, 4, 30, 485, DateTimeKind.Local).AddTicks(3187) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 16,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 14, "Tempore vero qui hic quo reprehenderit omnis consequuntur repudiandae libero.", new DateTime(2020, 6, 6, 13, 4, 30, 485, DateTimeKind.Local).AddTicks(3245), 9, new DateTime(2020, 6, 6, 13, 4, 30, 485, DateTimeKind.Local).AddTicks(3249) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 17,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 15, "Consequatur alias dolor eius.", new DateTime(2020, 6, 6, 13, 4, 30, 485, DateTimeKind.Local).AddTicks(3287), 8, new DateTime(2020, 6, 6, 13, 4, 30, 485, DateTimeKind.Local).AddTicks(3291) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 18,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 10, "Rem molestiae ut et.", new DateTime(2020, 6, 6, 13, 4, 30, 485, DateTimeKind.Local).AddTicks(3329), 15, new DateTime(2020, 6, 6, 13, 4, 30, 485, DateTimeKind.Local).AddTicks(3333) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 19,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "UpdatedAt" },
                values: new object[] { 10, "Sapiente molestias rerum omnis totam porro ut.", new DateTime(2020, 6, 6, 13, 4, 30, 485, DateTimeKind.Local).AddTicks(3379), new DateTime(2020, 6, 6, 13, 4, 30, 485, DateTimeKind.Local).AddTicks(3383) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 20,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 9, "Eum aliquam eum molestiae.", new DateTime(2020, 6, 6, 13, 4, 30, 485, DateTimeKind.Local).AddTicks(3491), 13, new DateTime(2020, 6, 6, 13, 4, 30, 485, DateTimeKind.Local).AddTicks(3496) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 6, 13, 4, 30, 299, DateTimeKind.Local).AddTicks(5388), "https://s3.amazonaws.com/uifaces/faces/twitter/vanchesz/128.jpg", new DateTime(2020, 6, 6, 13, 4, 30, 299, DateTimeKind.Local).AddTicks(9171) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 6, 13, 4, 30, 299, DateTimeKind.Local).AddTicks(9852), "https://s3.amazonaws.com/uifaces/faces/twitter/jayphen/128.jpg", new DateTime(2020, 6, 6, 13, 4, 30, 299, DateTimeKind.Local).AddTicks(9876) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 6, 13, 4, 30, 299, DateTimeKind.Local).AddTicks(9904), "https://s3.amazonaws.com/uifaces/faces/twitter/bublienko/128.jpg", new DateTime(2020, 6, 6, 13, 4, 30, 299, DateTimeKind.Local).AddTicks(9909) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 6, 13, 4, 30, 299, DateTimeKind.Local).AddTicks(9929), "https://s3.amazonaws.com/uifaces/faces/twitter/baires/128.jpg", new DateTime(2020, 6, 6, 13, 4, 30, 299, DateTimeKind.Local).AddTicks(9933) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 6, 13, 4, 30, 299, DateTimeKind.Local).AddTicks(9950), "https://s3.amazonaws.com/uifaces/faces/twitter/borges_marcos/128.jpg", new DateTime(2020, 6, 6, 13, 4, 30, 299, DateTimeKind.Local).AddTicks(9954) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 6, 13, 4, 30, 299, DateTimeKind.Local).AddTicks(9971), "https://s3.amazonaws.com/uifaces/faces/twitter/okansurreel/128.jpg", new DateTime(2020, 6, 6, 13, 4, 30, 299, DateTimeKind.Local).AddTicks(9976) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 7,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 6, 13, 4, 30, 299, DateTimeKind.Local).AddTicks(9992), "https://s3.amazonaws.com/uifaces/faces/twitter/muridrahhal/128.jpg", new DateTime(2020, 6, 6, 13, 4, 30, 299, DateTimeKind.Local).AddTicks(9996) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 8,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 6, 13, 4, 30, 300, DateTimeKind.Local).AddTicks(13), "https://s3.amazonaws.com/uifaces/faces/twitter/ryhanhassan/128.jpg", new DateTime(2020, 6, 6, 13, 4, 30, 300, DateTimeKind.Local).AddTicks(18) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 9,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 6, 13, 4, 30, 300, DateTimeKind.Local).AddTicks(34), "https://s3.amazonaws.com/uifaces/faces/twitter/markjenkins/128.jpg", new DateTime(2020, 6, 6, 13, 4, 30, 300, DateTimeKind.Local).AddTicks(38) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 10,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 6, 13, 4, 30, 300, DateTimeKind.Local).AddTicks(54), "https://s3.amazonaws.com/uifaces/faces/twitter/ninjad3m0/128.jpg", new DateTime(2020, 6, 6, 13, 4, 30, 300, DateTimeKind.Local).AddTicks(58) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 11,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 6, 13, 4, 30, 300, DateTimeKind.Local).AddTicks(75), "https://s3.amazonaws.com/uifaces/faces/twitter/aaronalfred/128.jpg", new DateTime(2020, 6, 6, 13, 4, 30, 300, DateTimeKind.Local).AddTicks(79) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 12,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 6, 13, 4, 30, 300, DateTimeKind.Local).AddTicks(95), "https://s3.amazonaws.com/uifaces/faces/twitter/victorquinn/128.jpg", new DateTime(2020, 6, 6, 13, 4, 30, 300, DateTimeKind.Local).AddTicks(99) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 13,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 6, 13, 4, 30, 300, DateTimeKind.Local).AddTicks(117), "https://s3.amazonaws.com/uifaces/faces/twitter/dmackerman/128.jpg", new DateTime(2020, 6, 6, 13, 4, 30, 300, DateTimeKind.Local).AddTicks(121) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 14,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 6, 13, 4, 30, 300, DateTimeKind.Local).AddTicks(142), "https://s3.amazonaws.com/uifaces/faces/twitter/ifarafonow/128.jpg", new DateTime(2020, 6, 6, 13, 4, 30, 300, DateTimeKind.Local).AddTicks(146) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 15,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 6, 13, 4, 30, 300, DateTimeKind.Local).AddTicks(162), "https://s3.amazonaws.com/uifaces/faces/twitter/josep_martins/128.jpg", new DateTime(2020, 6, 6, 13, 4, 30, 300, DateTimeKind.Local).AddTicks(166) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 16,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 6, 13, 4, 30, 300, DateTimeKind.Local).AddTicks(182), "https://s3.amazonaws.com/uifaces/faces/twitter/runningskull/128.jpg", new DateTime(2020, 6, 6, 13, 4, 30, 300, DateTimeKind.Local).AddTicks(186) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 17,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 6, 13, 4, 30, 300, DateTimeKind.Local).AddTicks(202), "https://s3.amazonaws.com/uifaces/faces/twitter/michzen/128.jpg", new DateTime(2020, 6, 6, 13, 4, 30, 300, DateTimeKind.Local).AddTicks(206) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 18,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 6, 13, 4, 30, 300, DateTimeKind.Local).AddTicks(223), "https://s3.amazonaws.com/uifaces/faces/twitter/martip07/128.jpg", new DateTime(2020, 6, 6, 13, 4, 30, 300, DateTimeKind.Local).AddTicks(227) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 19,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 6, 13, 4, 30, 300, DateTimeKind.Local).AddTicks(244), "https://s3.amazonaws.com/uifaces/faces/twitter/missaaamy/128.jpg", new DateTime(2020, 6, 6, 13, 4, 30, 300, DateTimeKind.Local).AddTicks(248) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 20,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 6, 13, 4, 30, 300, DateTimeKind.Local).AddTicks(264), "https://s3.amazonaws.com/uifaces/faces/twitter/simobenso/128.jpg", new DateTime(2020, 6, 6, 13, 4, 30, 300, DateTimeKind.Local).AddTicks(268) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 21,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 6, 13, 4, 30, 304, DateTimeKind.Local).AddTicks(2600), "https://picsum.photos/640/480/?image=545", new DateTime(2020, 6, 6, 13, 4, 30, 304, DateTimeKind.Local).AddTicks(3227) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 22,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 6, 13, 4, 30, 304, DateTimeKind.Local).AddTicks(3524), "https://picsum.photos/640/480/?image=1081", new DateTime(2020, 6, 6, 13, 4, 30, 304, DateTimeKind.Local).AddTicks(3550) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 23,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 6, 13, 4, 30, 304, DateTimeKind.Local).AddTicks(3572), "https://picsum.photos/640/480/?image=938", new DateTime(2020, 6, 6, 13, 4, 30, 304, DateTimeKind.Local).AddTicks(3577) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 24,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 6, 13, 4, 30, 304, DateTimeKind.Local).AddTicks(3594), "https://picsum.photos/640/480/?image=25", new DateTime(2020, 6, 6, 13, 4, 30, 304, DateTimeKind.Local).AddTicks(3598) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 25,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 6, 13, 4, 30, 304, DateTimeKind.Local).AddTicks(3615), "https://picsum.photos/640/480/?image=1008", new DateTime(2020, 6, 6, 13, 4, 30, 304, DateTimeKind.Local).AddTicks(3619) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 26,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 6, 13, 4, 30, 304, DateTimeKind.Local).AddTicks(3635), "https://picsum.photos/640/480/?image=706", new DateTime(2020, 6, 6, 13, 4, 30, 304, DateTimeKind.Local).AddTicks(3639) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 27,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 6, 13, 4, 30, 304, DateTimeKind.Local).AddTicks(3655), "https://picsum.photos/640/480/?image=268", new DateTime(2020, 6, 6, 13, 4, 30, 304, DateTimeKind.Local).AddTicks(3659) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 28,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 6, 13, 4, 30, 304, DateTimeKind.Local).AddTicks(3675), "https://picsum.photos/640/480/?image=789", new DateTime(2020, 6, 6, 13, 4, 30, 304, DateTimeKind.Local).AddTicks(3679) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 29,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 6, 13, 4, 30, 304, DateTimeKind.Local).AddTicks(3695), "https://picsum.photos/640/480/?image=238", new DateTime(2020, 6, 6, 13, 4, 30, 304, DateTimeKind.Local).AddTicks(3698) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 30,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 6, 13, 4, 30, 304, DateTimeKind.Local).AddTicks(3715), "https://picsum.photos/640/480/?image=596", new DateTime(2020, 6, 6, 13, 4, 30, 304, DateTimeKind.Local).AddTicks(3718) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 31,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 6, 13, 4, 30, 304, DateTimeKind.Local).AddTicks(3734), "https://picsum.photos/640/480/?image=410", new DateTime(2020, 6, 6, 13, 4, 30, 304, DateTimeKind.Local).AddTicks(3738) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 32,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 6, 13, 4, 30, 304, DateTimeKind.Local).AddTicks(3754), "https://picsum.photos/640/480/?image=85", new DateTime(2020, 6, 6, 13, 4, 30, 304, DateTimeKind.Local).AddTicks(3758) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 33,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 6, 13, 4, 30, 304, DateTimeKind.Local).AddTicks(3801), "https://picsum.photos/640/480/?image=1059", new DateTime(2020, 6, 6, 13, 4, 30, 304, DateTimeKind.Local).AddTicks(3805) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 34,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 6, 13, 4, 30, 304, DateTimeKind.Local).AddTicks(3823), "https://picsum.photos/640/480/?image=655", new DateTime(2020, 6, 6, 13, 4, 30, 304, DateTimeKind.Local).AddTicks(3827) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 35,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 6, 13, 4, 30, 304, DateTimeKind.Local).AddTicks(3843), "https://picsum.photos/640/480/?image=169", new DateTime(2020, 6, 6, 13, 4, 30, 304, DateTimeKind.Local).AddTicks(3847) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 36,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 6, 13, 4, 30, 304, DateTimeKind.Local).AddTicks(3862), "https://picsum.photos/640/480/?image=279", new DateTime(2020, 6, 6, 13, 4, 30, 304, DateTimeKind.Local).AddTicks(3866) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 37,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 6, 13, 4, 30, 304, DateTimeKind.Local).AddTicks(3882), "https://picsum.photos/640/480/?image=81", new DateTime(2020, 6, 6, 13, 4, 30, 304, DateTimeKind.Local).AddTicks(3885) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 38,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 6, 13, 4, 30, 304, DateTimeKind.Local).AddTicks(3901), "https://picsum.photos/640/480/?image=617", new DateTime(2020, 6, 6, 13, 4, 30, 304, DateTimeKind.Local).AddTicks(3904) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 39,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 6, 13, 4, 30, 304, DateTimeKind.Local).AddTicks(3920), "https://picsum.photos/640/480/?image=65", new DateTime(2020, 6, 6, 13, 4, 30, 304, DateTimeKind.Local).AddTicks(3924) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 40,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 6, 13, 4, 30, 304, DateTimeKind.Local).AddTicks(3940), "https://picsum.photos/640/480/?image=1060", new DateTime(2020, 6, 6, 13, 4, 30, 304, DateTimeKind.Local).AddTicks(3944) });

            migrationBuilder.InsertData(
                table: "PostReactions",
                columns: new[] { "Id", "CreatedAt", "IsLike", "PostId", "UpdatedAt", "UserId" },
                values: new object[,]
                {
                    { 11, new DateTime(2020, 6, 6, 13, 4, 30, 491, DateTimeKind.Local).AddTicks(2552), true, 2, new DateTime(2020, 6, 6, 13, 4, 30, 491, DateTimeKind.Local).AddTicks(2557), 15 },
                    { 10, new DateTime(2020, 6, 6, 13, 4, 30, 491, DateTimeKind.Local).AddTicks(2528), false, 9, new DateTime(2020, 6, 6, 13, 4, 30, 491, DateTimeKind.Local).AddTicks(2532), 9 },
                    { 9, new DateTime(2020, 6, 6, 13, 4, 30, 491, DateTimeKind.Local).AddTicks(2500), true, 3, new DateTime(2020, 6, 6, 13, 4, 30, 491, DateTimeKind.Local).AddTicks(2505), 19 },
                    { 8, new DateTime(2020, 6, 6, 13, 4, 30, 491, DateTimeKind.Local).AddTicks(2439), true, 14, new DateTime(2020, 6, 6, 13, 4, 30, 491, DateTimeKind.Local).AddTicks(2443), 4 },
                    { 6, new DateTime(2020, 6, 6, 13, 4, 30, 491, DateTimeKind.Local).AddTicks(2389), false, 17, new DateTime(2020, 6, 6, 13, 4, 30, 491, DateTimeKind.Local).AddTicks(2392), 21 },
                    { 3, new DateTime(2020, 6, 6, 13, 4, 30, 491, DateTimeKind.Local).AddTicks(2311), true, 19, new DateTime(2020, 6, 6, 13, 4, 30, 491, DateTimeKind.Local).AddTicks(2316), 1 },
                    { 5, new DateTime(2020, 6, 6, 13, 4, 30, 491, DateTimeKind.Local).AddTicks(2364), false, 11, new DateTime(2020, 6, 6, 13, 4, 30, 491, DateTimeKind.Local).AddTicks(2368), 19 },
                    { 4, new DateTime(2020, 6, 6, 13, 4, 30, 491, DateTimeKind.Local).AddTicks(2338), true, 11, new DateTime(2020, 6, 6, 13, 4, 30, 491, DateTimeKind.Local).AddTicks(2343), 1 },
                    { 12, new DateTime(2020, 6, 6, 13, 4, 30, 491, DateTimeKind.Local).AddTicks(2577), true, 7, new DateTime(2020, 6, 6, 13, 4, 30, 491, DateTimeKind.Local).AddTicks(2581), 1 },
                    { 1, new DateTime(2020, 6, 6, 13, 4, 30, 491, DateTimeKind.Local).AddTicks(1272), false, 7, new DateTime(2020, 6, 6, 13, 4, 30, 491, DateTimeKind.Local).AddTicks(1815), 7 },
                    { 2, new DateTime(2020, 6, 6, 13, 4, 30, 491, DateTimeKind.Local).AddTicks(2261), true, 19, new DateTime(2020, 6, 6, 13, 4, 30, 491, DateTimeKind.Local).AddTicks(2276), 19 },
                    { 7, new DateTime(2020, 6, 6, 13, 4, 30, 491, DateTimeKind.Local).AddTicks(2414), false, 6, new DateTime(2020, 6, 6, 13, 4, 30, 491, DateTimeKind.Local).AddTicks(2418), 13 },
                    { 13, new DateTime(2020, 6, 6, 13, 4, 30, 491, DateTimeKind.Local).AddTicks(2602), true, 14, new DateTime(2020, 6, 6, 13, 4, 30, 491, DateTimeKind.Local).AddTicks(2605), 14 },
                    { 18, new DateTime(2020, 6, 6, 13, 4, 30, 491, DateTimeKind.Local).AddTicks(2724), false, 16, new DateTime(2020, 6, 6, 13, 4, 30, 491, DateTimeKind.Local).AddTicks(2728), 14 },
                    { 15, new DateTime(2020, 6, 6, 13, 4, 30, 491, DateTimeKind.Local).AddTicks(2650), false, 14, new DateTime(2020, 6, 6, 13, 4, 30, 491, DateTimeKind.Local).AddTicks(2654), 9 },
                    { 16, new DateTime(2020, 6, 6, 13, 4, 30, 491, DateTimeKind.Local).AddTicks(2676), true, 1, new DateTime(2020, 6, 6, 13, 4, 30, 491, DateTimeKind.Local).AddTicks(2680), 16 },
                    { 20, new DateTime(2020, 6, 6, 13, 4, 30, 491, DateTimeKind.Local).AddTicks(2773), false, 20, new DateTime(2020, 6, 6, 13, 4, 30, 491, DateTimeKind.Local).AddTicks(2777), 19 },
                    { 17, new DateTime(2020, 6, 6, 13, 4, 30, 491, DateTimeKind.Local).AddTicks(2700), false, 6, new DateTime(2020, 6, 6, 13, 4, 30, 491, DateTimeKind.Local).AddTicks(2704), 7 },
                    { 19, new DateTime(2020, 6, 6, 13, 4, 30, 491, DateTimeKind.Local).AddTicks(2749), true, 3, new DateTime(2020, 6, 6, 13, 4, 30, 491, DateTimeKind.Local).AddTicks(2752), 18 },
                    { 14, new DateTime(2020, 6, 6, 13, 4, 30, 491, DateTimeKind.Local).AddTicks(2626), true, 13, new DateTime(2020, 6, 6, 13, 4, 30, 491, DateTimeKind.Local).AddTicks(2630), 9 }
                });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 4, "delectus", new DateTime(2020, 6, 6, 13, 4, 30, 478, DateTimeKind.Local).AddTicks(5616), 31, new DateTime(2020, 6, 6, 13, 4, 30, 478, DateTimeKind.Local).AddTicks(6148) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 10, @"Id quos suscipit debitis ducimus excepturi accusantium.
Laboriosam dolorem modi repellendus cum quidem.
Quia consectetur facilis.
Dolore quasi ab odio error.
Qui assumenda necessitatibus minima tempora et facere.", new DateTime(2020, 6, 6, 13, 4, 30, 479, DateTimeKind.Local).AddTicks(2437), 35, new DateTime(2020, 6, 6, 13, 4, 30, 479, DateTimeKind.Local).AddTicks(2464) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 6, "Non eaque voluptatibus quo aut cumque non eius ipsum.", new DateTime(2020, 6, 6, 13, 4, 30, 479, DateTimeKind.Local).AddTicks(3184), 22, new DateTime(2020, 6, 6, 13, 4, 30, 479, DateTimeKind.Local).AddTicks(3198) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 7, "eos", new DateTime(2020, 6, 6, 13, 4, 30, 479, DateTimeKind.Local).AddTicks(3242), 36, new DateTime(2020, 6, 6, 13, 4, 30, 479, DateTimeKind.Local).AddTicks(3247) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "UpdatedAt" },
                values: new object[] { 18, @"Iusto nobis illo assumenda ex fuga.
Dolores magni aut sapiente totam beatae non neque.
Et sequi distinctio excepturi odio.", new DateTime(2020, 6, 6, 13, 4, 30, 479, DateTimeKind.Local).AddTicks(3480), new DateTime(2020, 6, 6, 13, 4, 30, 479, DateTimeKind.Local).AddTicks(3486) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 12, "reprehenderit", new DateTime(2020, 6, 6, 13, 4, 30, 479, DateTimeKind.Local).AddTicks(3519), 30, new DateTime(2020, 6, 6, 13, 4, 30, 479, DateTimeKind.Local).AddTicks(3523) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 7,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 20, @"Ut ex non ut doloremque temporibus.
Nesciunt soluta ea explicabo libero sunt aspernatur aliquam.
Voluptatibus quae repellat debitis unde ipsum nulla quo.
Et nulla fugiat.
Saepe inventore eos iure facilis non.", new DateTime(2020, 6, 6, 13, 4, 30, 479, DateTimeKind.Local).AddTicks(3728), 33, new DateTime(2020, 6, 6, 13, 4, 30, 479, DateTimeKind.Local).AddTicks(3734) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 8,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 21, "Excepturi vitae rem corporis quam assumenda. Maxime delectus eligendi atque sint est officiis recusandae minus. Illum sed sapiente earum sint dolor temporibus.", new DateTime(2020, 6, 6, 13, 4, 30, 479, DateTimeKind.Local).AddTicks(4767), 26, new DateTime(2020, 6, 6, 13, 4, 30, 479, DateTimeKind.Local).AddTicks(4780) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 9,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 6, "Necessitatibus est omnis eos et dolorum commodi laudantium optio perspiciatis.", new DateTime(2020, 6, 6, 13, 4, 30, 479, DateTimeKind.Local).AddTicks(4886), 26, new DateTime(2020, 6, 6, 13, 4, 30, 479, DateTimeKind.Local).AddTicks(4891) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 10,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 12, "Accusamus nobis velit.", new DateTime(2020, 6, 6, 13, 4, 30, 479, DateTimeKind.Local).AddTicks(4933), 31, new DateTime(2020, 6, 6, 13, 4, 30, 479, DateTimeKind.Local).AddTicks(4938) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 11,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 4, @"Aspernatur consequuntur rerum eius et mollitia.
Nobis alias laudantium corrupti accusamus.
Ipsam non nisi est doloribus molestias totam aperiam delectus.
Itaque est voluptas ab explicabo autem possimus.
Voluptas numquam velit omnis harum dolores.", new DateTime(2020, 6, 6, 13, 4, 30, 479, DateTimeKind.Local).AddTicks(5154), 33, new DateTime(2020, 6, 6, 13, 4, 30, 479, DateTimeKind.Local).AddTicks(5160) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 12,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 15, "Ut eum est. Dolore omnis facere et saepe ut. Et dignissimos dolores.", new DateTime(2020, 6, 6, 13, 4, 30, 479, DateTimeKind.Local).AddTicks(5247), 33, new DateTime(2020, 6, 6, 13, 4, 30, 479, DateTimeKind.Local).AddTicks(5252) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 13,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 1, @"Quia nemo dolore aperiam est expedita ducimus.
Ullam nulla alias et maxime corporis voluptatem nihil eligendi.
Quis mollitia dolores nisi.
Eos necessitatibus omnis ut delectus.", new DateTime(2020, 6, 6, 13, 4, 30, 479, DateTimeKind.Local).AddTicks(5442), 29, new DateTime(2020, 6, 6, 13, 4, 30, 479, DateTimeKind.Local).AddTicks(5447) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 14,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 8, "Eius a nihil exercitationem autem ab placeat. Temporibus blanditiis modi saepe id voluptatem itaque quam impedit. Sed et asperiores ab et repellat eum qui delectus nobis.", new DateTime(2020, 6, 6, 13, 4, 30, 479, DateTimeKind.Local).AddTicks(5573), 30, new DateTime(2020, 6, 6, 13, 4, 30, 479, DateTimeKind.Local).AddTicks(5578) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 15,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 7, "Aut quia cupiditate.", new DateTime(2020, 6, 6, 13, 4, 30, 479, DateTimeKind.Local).AddTicks(5619), 27, new DateTime(2020, 6, 6, 13, 4, 30, 479, DateTimeKind.Local).AddTicks(5624) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 16,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 12, "ut", new DateTime(2020, 6, 6, 13, 4, 30, 479, DateTimeKind.Local).AddTicks(5650), 23, new DateTime(2020, 6, 6, 13, 4, 30, 479, DateTimeKind.Local).AddTicks(5654) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 17,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 8, "Sunt delectus sequi dolores doloremque unde impedit quia voluptatem debitis. Dolorem explicabo nihil voluptates est dicta et. Ducimus consectetur ipsum perferendis provident. Deserunt aut qui consequatur quam nihil velit nisi. Voluptates esse doloribus totam et recusandae harum qui repellendus consequatur.", new DateTime(2020, 6, 6, 13, 4, 30, 479, DateTimeKind.Local).AddTicks(5910), 33, new DateTime(2020, 6, 6, 13, 4, 30, 479, DateTimeKind.Local).AddTicks(5916) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 18,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 21, "Numquam magnam doloremque rem voluptatum quaerat eligendi.", new DateTime(2020, 6, 6, 13, 4, 30, 479, DateTimeKind.Local).AddTicks(5992), 39, new DateTime(2020, 6, 6, 13, 4, 30, 479, DateTimeKind.Local).AddTicks(5997) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 19,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 5, @"Ducimus consequuntur fugit nulla ipsum ea.
Fugit quibusdam quos distinctio et velit iure illum quae.
Consequatur non vel laboriosam corrupti consequatur.
Iure incidunt quisquam dicta quis asperiores laudantium consequatur impedit.
Corrupti molestias accusantium earum.", new DateTime(2020, 6, 6, 13, 4, 30, 479, DateTimeKind.Local).AddTicks(6175), 37, new DateTime(2020, 6, 6, 13, 4, 30, 479, DateTimeKind.Local).AddTicks(6181) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 20,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 18, @"Est officia esse veritatis sunt odio facilis provident hic.
Vel aspernatur sapiente omnis ea blanditiis itaque nobis.", new DateTime(2020, 6, 6, 13, 4, 30, 479, DateTimeKind.Local).AddTicks(6270), 35, new DateTime(2020, 6, 6, 13, 4, 30, 479, DateTimeKind.Local).AddTicks(6275) });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 1, new DateTime(2020, 6, 6, 13, 4, 30, 333, DateTimeKind.Local).AddTicks(4904), "Ralph37@hotmail.com", "2bv68z9XzyeyRdJcn/6wVhwxx/urD8hX9RKYAmw6qpg=", "/WagXrlo1qQDaRWDcU9+QUBRXscHZpFT/SoGEaGBFb0=", new DateTime(2020, 6, 6, 13, 4, 30, 333, DateTimeKind.Local).AddTicks(5574), "Sylvan_Senger94" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 14, new DateTime(2020, 6, 6, 13, 4, 30, 340, DateTimeKind.Local).AddTicks(83), "Tyson.Cole94@hotmail.com", "p0rpNFnPQqngLo1gao9xHkk5Mhv8f+83Z8XKLIuJ2d4=", "dffdTkgKK7xo3kmUpuM4bcWJ1pKXRZ4LYxHXB01qlEA=", new DateTime(2020, 6, 6, 13, 4, 30, 340, DateTimeKind.Local).AddTicks(114), "Roman46" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 2, new DateTime(2020, 6, 6, 13, 4, 30, 347, DateTimeKind.Local).AddTicks(9312), "Norris78@hotmail.com", "3aGnqaG/KbPOFjGp7yoyP+p/PPVzqpuFTkhe+YPzWgI=", "2NJ62U//qvIn/PUGdIKmq6OjaXKgCooXnqoHB0J9P/o=", new DateTime(2020, 6, 6, 13, 4, 30, 347, DateTimeKind.Local).AddTicks(9391), "Horace.Swift77" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 4, new DateTime(2020, 6, 6, 13, 4, 30, 358, DateTimeKind.Local).AddTicks(5448), "Lulu77@gmail.com", "mdSkYUGtRLEXr9nfQTvas5977C8xBBwpkccr9nydDyg=", "PGreumkHftkrjZYa6xJytfuRR62ZaqmHOlH7yNpJQiA=", new DateTime(2020, 6, 6, 13, 4, 30, 358, DateTimeKind.Local).AddTicks(5511), "Anna_Hagenes" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 18, new DateTime(2020, 6, 6, 13, 4, 30, 364, DateTimeKind.Local).AddTicks(9530), "Cayla_Murphy@yahoo.com", "crWevl2Z7Qx0ltUDSdLQSgIFvdFXFJh6c1Fj7jVnpCA=", "PM1abgkpreO6fQN8Z4aSKn2RG5J9ZaNYsfuD1shB3RA=", new DateTime(2020, 6, 6, 13, 4, 30, 364, DateTimeKind.Local).AddTicks(9569), "Carlie.Lindgren7" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 7, new DateTime(2020, 6, 6, 13, 4, 30, 371, DateTimeKind.Local).AddTicks(3496), "Sylvia.Harber73@yahoo.com", "GX+yjHDbGuj9bIkuA5OMZEgVN/IvWsg7zGbh8xGSaj0=", "kve5z0DQ+u2pVkQfP7qXdwFybpdHhOsGfi2pnZ+BsdY=", new DateTime(2020, 6, 6, 13, 4, 30, 371, DateTimeKind.Local).AddTicks(3511), "Valentine.Nicolas98" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 7,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 10, new DateTime(2020, 6, 6, 13, 4, 30, 377, DateTimeKind.Local).AddTicks(9577), "Nellie_Padberg77@gmail.com", "PS9s7SWK+mIDf6A4dyjzJ2WjtkRm6kYqFHJQ0hIazTU=", "vVK+FDJPDBmUcgbJPcaFFh2vXQoYQbxbZglZY5yN+Oo=", new DateTime(2020, 6, 6, 13, 4, 30, 377, DateTimeKind.Local).AddTicks(9626), "Modesto_Hagenes6" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 8,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 10, new DateTime(2020, 6, 6, 13, 4, 30, 384, DateTimeKind.Local).AddTicks(2818), "Garfield.Kilback@hotmail.com", "nk7DKaEkBQIrDi/JOB4bRmoumVORMq9FTXkf0o5A1jM=", "4wc5KIz58/Acvc+SSoxqaduyiSnGO4QVyLUb9ZRnx60=", new DateTime(2020, 6, 6, 13, 4, 30, 384, DateTimeKind.Local).AddTicks(2849), "Savanna38" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 9,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 3, new DateTime(2020, 6, 6, 13, 4, 30, 390, DateTimeKind.Local).AddTicks(5110), "Elmer_Morar@hotmail.com", "ILAcLyo6ANeqZQ61bmCVH1g7HzUlW4jOn6PzSI4Vxf0=", "MEy+m1LN+Dtd1mq5N+O+plKZz9a9nU2wdoEGXsZhjwM=", new DateTime(2020, 6, 6, 13, 4, 30, 390, DateTimeKind.Local).AddTicks(5147), "Lea.Fritsch79" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 10,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 4, new DateTime(2020, 6, 6, 13, 4, 30, 396, DateTimeKind.Local).AddTicks(6563), "Larue62@gmail.com", "c7xzvwf3pT4NnI7sO3YcHPFHQ1Jc42opJGhVyjHkpeQ=", "n6ysL6uRvI2GWkz8KihtOyBFji2uwNSNwHAajgLElZE=", new DateTime(2020, 6, 6, 13, 4, 30, 396, DateTimeKind.Local).AddTicks(6599), "Elmira_Haley54" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 11,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 3, new DateTime(2020, 6, 6, 13, 4, 30, 402, DateTimeKind.Local).AddTicks(6049), "Shane_Hauck@hotmail.com", "v5nX5dde8HMJFi3u2oi80bxLETGsJ9fsZS3bMdtRyzs=", "YnxfN73x9YuC3LJ7xq/sWfrNCs1l2xGnDqT/ty+z4E8=", new DateTime(2020, 6, 6, 13, 4, 30, 402, DateTimeKind.Local).AddTicks(6066), "Doyle_Barton" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 12,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 10, new DateTime(2020, 6, 6, 13, 4, 30, 408, DateTimeKind.Local).AddTicks(6000), "Judy.Lemke66@gmail.com", "oGHUgzNXZnyQnYmAK5UPhG0jlcNBM6XIuhs5RRTKDTw=", "eEwA9F7R0uJvShAGSAw+8SHc5AfWb2t8OQxRDiFj+C4=", new DateTime(2020, 6, 6, 13, 4, 30, 408, DateTimeKind.Local).AddTicks(6029), "Louie15" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 13,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 12, new DateTime(2020, 6, 6, 13, 4, 30, 414, DateTimeKind.Local).AddTicks(8163), "Johnny_Mills@hotmail.com", "nBZqRzHEbxD0FKd5q/gvzSbCufbQCG6mLs0pYYams94=", "w8wQwDJEbPmh6yt0oWLrigIEa2gnD8WrCOgTY+7Z1ts=", new DateTime(2020, 6, 6, 13, 4, 30, 414, DateTimeKind.Local).AddTicks(8216), "Viviane.Stokes" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 14,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 4, new DateTime(2020, 6, 6, 13, 4, 30, 420, DateTimeKind.Local).AddTicks(8757), "Cristian52@hotmail.com", "CeqVpPI2PHu+E/pf6cJKwCmLngzbwJ1LyuzjdOYLmmU=", "fSeDFPifxzlEAwmQjLsjA6JRPn4qjreD5QDBFHBoDsI=", new DateTime(2020, 6, 6, 13, 4, 30, 420, DateTimeKind.Local).AddTicks(8768), "Justice51" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 15,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 13, new DateTime(2020, 6, 6, 13, 4, 30, 427, DateTimeKind.Local).AddTicks(1925), "Vicky.Kautzer41@gmail.com", "uF/0ZvgNDXtPmeTziLaLIIbZAYyfGIkmNOhTbna8M7c=", "rPvNSyehdwQ8mUkFmBH+0iXSLUIN8zG2+9tb8pmeSOU=", new DateTime(2020, 6, 6, 13, 4, 30, 427, DateTimeKind.Local).AddTicks(1985), "Wilfred_Green96" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 16,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 17, new DateTime(2020, 6, 6, 13, 4, 30, 433, DateTimeKind.Local).AddTicks(5314), "Claud_Larkin@gmail.com", "eaMY7jSgMZo0sjT4TE6o0J1x+rOOqWoT13G0oqO7ZcQ=", "rlsbuGx9e1q+pNboeru17rntw3gbqH1R1fshrsJdJsY=", new DateTime(2020, 6, 6, 13, 4, 30, 433, DateTimeKind.Local).AddTicks(5352), "Penelope.Littel67" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 17,
                columns: new[] { "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { new DateTime(2020, 6, 6, 13, 4, 30, 444, DateTimeKind.Local).AddTicks(267), "Bertram.Lang20@hotmail.com", "YKxW+74m2SOIY/QgRVCzW1U4vwxqyGO4CUpWhzT4oFI=", "TL1NeE5Nkuszskp+kILjb1d+3b/rY4nsAzpgTgWRPow=", new DateTime(2020, 6, 6, 13, 4, 30, 444, DateTimeKind.Local).AddTicks(316), "Ciara.Raynor6" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 18,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 20, new DateTime(2020, 6, 6, 13, 4, 30, 454, DateTimeKind.Local).AddTicks(1925), "Alexanne36@gmail.com", "KghPj6CeEgJfQJMcqLz93Src7RxNFE1qIN+/2bZaMW0=", "p3NMiAwyzLWYVZsaAUzpr0tv803B7vFkxtg6K0Dk9Pc=", new DateTime(2020, 6, 6, 13, 4, 30, 454, DateTimeKind.Local).AddTicks(1959), "Glennie.Nicolas70" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 19,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 2, new DateTime(2020, 6, 6, 13, 4, 30, 460, DateTimeKind.Local).AddTicks(5603), "Luna_Marquardt@yahoo.com", "r71QOe/ZCvPGTllMw6JYAaLroiPsJVV1ko08xHqL/+s=", "EYD4uYa1xSskVIm4uXTGjziQn0gksOHuOEtkVqW9xU4=", new DateTime(2020, 6, 6, 13, 4, 30, 460, DateTimeKind.Local).AddTicks(5648), "Nona_Murphy6" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 20,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 16, new DateTime(2020, 6, 6, 13, 4, 30, 466, DateTimeKind.Local).AddTicks(6680), "Noel24@yahoo.com", "h1X1c1Av8Jiza9yC6MOggGd34U55K+UQ0ua27AavjuQ=", "BZ6sQDQBv2ObnG9rHs+mQMUc4xl5sIdZaMs36RVlE5Y=", new DateTime(2020, 6, 6, 13, 4, 30, 466, DateTimeKind.Local).AddTicks(6695), "Chet_Miller" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 21,
                columns: new[] { "CreatedAt", "Password", "Salt", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 6, 13, 4, 30, 472, DateTimeKind.Local).AddTicks(8049), "FMSuFW9UwBmcCWU6ov0reHvcTZWdV/w0fv3wNzWc/qA=", "I++YOzymuGdYB0vwhQSrqTR4AzhMbk9RXbktG5tIYSs=", new DateTime(2020, 6, 6, 13, 4, 30, 472, DateTimeKind.Local).AddTicks(8049) });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 5);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 6);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 7);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 8);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 9);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 10);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 11);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 12);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 13);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 14);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 15);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 16);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 17);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 18);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 19);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 20);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 5);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 6);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 7);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 8);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 9);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 10);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 11);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 12);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 13);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 14);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 15);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 16);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 17);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 18);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 19);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 20);

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "Body", "CreatedAt", "UpdatedAt" },
                values: new object[] { "Sed sint dolorum nulla fuga magni tempora inventore et.", new DateTime(2020, 6, 6, 13, 0, 8, 358, DateTimeKind.Local).AddTicks(8501), new DateTime(2020, 6, 6, 13, 0, 8, 358, DateTimeKind.Local).AddTicks(8992) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 9, "Natus velit alias adipisci distinctio amet.", new DateTime(2020, 6, 6, 13, 0, 8, 358, DateTimeKind.Local).AddTicks(9501), 9, new DateTime(2020, 6, 6, 13, 0, 8, 358, DateTimeKind.Local).AddTicks(9515) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 7, "Consequuntur corrupti eum et.", new DateTime(2020, 6, 6, 13, 0, 8, 358, DateTimeKind.Local).AddTicks(9573), 5, new DateTime(2020, 6, 6, 13, 0, 8, 358, DateTimeKind.Local).AddTicks(9578) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 21, "Dolorem natus asperiores.", new DateTime(2020, 6, 6, 13, 0, 8, 358, DateTimeKind.Local).AddTicks(9617), 13, new DateTime(2020, 6, 6, 13, 0, 8, 358, DateTimeKind.Local).AddTicks(9621) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 15, "Eligendi omnis facilis et suscipit ut occaecati voluptatibus praesentium et.", new DateTime(2020, 6, 6, 13, 0, 8, 358, DateTimeKind.Local).AddTicks(9694), 1, new DateTime(2020, 6, 6, 13, 0, 8, 358, DateTimeKind.Local).AddTicks(9699) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 6, "Blanditiis ut at eveniet dolore necessitatibus dolor distinctio asperiores et.", new DateTime(2020, 6, 6, 13, 0, 8, 358, DateTimeKind.Local).AddTicks(9760), 5, new DateTime(2020, 6, 6, 13, 0, 8, 358, DateTimeKind.Local).AddTicks(9765) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 7,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 19, "Et ipsum ut inventore id.", new DateTime(2020, 6, 6, 13, 0, 8, 358, DateTimeKind.Local).AddTicks(9871), 15, new DateTime(2020, 6, 6, 13, 0, 8, 358, DateTimeKind.Local).AddTicks(9876) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 8,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 5, "Quisquam et unde aut pariatur ratione eveniet.", new DateTime(2020, 6, 6, 13, 0, 8, 358, DateTimeKind.Local).AddTicks(9929), 14, new DateTime(2020, 6, 6, 13, 0, 8, 358, DateTimeKind.Local).AddTicks(9933) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 9,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 16, "Impedit animi eum est occaecati ut distinctio.", new DateTime(2020, 6, 6, 13, 0, 8, 358, DateTimeKind.Local).AddTicks(9981), 2, new DateTime(2020, 6, 6, 13, 0, 8, 358, DateTimeKind.Local).AddTicks(9985) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 10,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 9, "Minima optio voluptas voluptate quam consequatur ea ut placeat.", new DateTime(2020, 6, 6, 13, 0, 8, 359, DateTimeKind.Local).AddTicks(45), 4, new DateTime(2020, 6, 6, 13, 0, 8, 359, DateTimeKind.Local).AddTicks(49) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 11,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 10, "Ad exercitationem id exercitationem nemo necessitatibus qui maxime rerum.", new DateTime(2020, 6, 6, 13, 0, 8, 359, DateTimeKind.Local).AddTicks(103), 7, new DateTime(2020, 6, 6, 13, 0, 8, 359, DateTimeKind.Local).AddTicks(107) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 12,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 19, "Provident et hic dolores et ut in doloribus assumenda.", new DateTime(2020, 6, 6, 13, 0, 8, 359, DateTimeKind.Local).AddTicks(186), 7, new DateTime(2020, 6, 6, 13, 0, 8, 359, DateTimeKind.Local).AddTicks(191) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 13,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "UpdatedAt" },
                values: new object[] { 18, "Repudiandae qui officia.", new DateTime(2020, 6, 6, 13, 0, 8, 359, DateTimeKind.Local).AddTicks(230), new DateTime(2020, 6, 6, 13, 0, 8, 359, DateTimeKind.Local).AddTicks(234) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 14,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 5, "Dignissimos ut voluptate saepe voluptas dicta eaque.", new DateTime(2020, 6, 6, 13, 0, 8, 359, DateTimeKind.Local).AddTicks(281), 14, new DateTime(2020, 6, 6, 13, 0, 8, 359, DateTimeKind.Local).AddTicks(286) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 15,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 5, "Aliquid ut ut consequatur quis voluptatum debitis autem illo.", new DateTime(2020, 6, 6, 13, 0, 8, 359, DateTimeKind.Local).AddTicks(340), 14, new DateTime(2020, 6, 6, 13, 0, 8, 359, DateTimeKind.Local).AddTicks(344) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 16,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 8, "Alias sit quae reprehenderit a quo.", new DateTime(2020, 6, 6, 13, 0, 8, 359, DateTimeKind.Local).AddTicks(389), 4, new DateTime(2020, 6, 6, 13, 0, 8, 359, DateTimeKind.Local).AddTicks(393) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 17,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 10, "Unde sapiente suscipit accusantium veritatis.", new DateTime(2020, 6, 6, 13, 0, 8, 359, DateTimeKind.Local).AddTicks(436), 13, new DateTime(2020, 6, 6, 13, 0, 8, 359, DateTimeKind.Local).AddTicks(440) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 18,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 14, "Et explicabo molestiae id harum.", new DateTime(2020, 6, 6, 13, 0, 8, 359, DateTimeKind.Local).AddTicks(546), 9, new DateTime(2020, 6, 6, 13, 0, 8, 359, DateTimeKind.Local).AddTicks(551) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 19,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "UpdatedAt" },
                values: new object[] { 5, "Non est odio voluptatem autem temporibus iste at sapiente ut.", new DateTime(2020, 6, 6, 13, 0, 8, 359, DateTimeKind.Local).AddTicks(612), new DateTime(2020, 6, 6, 13, 0, 8, 359, DateTimeKind.Local).AddTicks(616) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 20,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 1, "Consequuntur quia nulla dolores laudantium aut iure harum repellendus nemo.", new DateTime(2020, 6, 6, 13, 0, 8, 359, DateTimeKind.Local).AddTicks(675), 5, new DateTime(2020, 6, 6, 13, 0, 8, 359, DateTimeKind.Local).AddTicks(679) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 6, 13, 0, 8, 192, DateTimeKind.Local).AddTicks(2789), "https://s3.amazonaws.com/uifaces/faces/twitter/otozk/128.jpg", new DateTime(2020, 6, 6, 13, 0, 8, 192, DateTimeKind.Local).AddTicks(6548) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 6, 13, 0, 8, 192, DateTimeKind.Local).AddTicks(7193), "https://s3.amazonaws.com/uifaces/faces/twitter/catadeleon/128.jpg", new DateTime(2020, 6, 6, 13, 0, 8, 192, DateTimeKind.Local).AddTicks(7213) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 6, 13, 0, 8, 192, DateTimeKind.Local).AddTicks(7243), "https://s3.amazonaws.com/uifaces/faces/twitter/leehambley/128.jpg", new DateTime(2020, 6, 6, 13, 0, 8, 192, DateTimeKind.Local).AddTicks(7247) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 6, 13, 0, 8, 192, DateTimeKind.Local).AddTicks(7266), "https://s3.amazonaws.com/uifaces/faces/twitter/drewbyreese/128.jpg", new DateTime(2020, 6, 6, 13, 0, 8, 192, DateTimeKind.Local).AddTicks(7270) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 6, 13, 0, 8, 192, DateTimeKind.Local).AddTicks(7288), "https://s3.amazonaws.com/uifaces/faces/twitter/vaughanmoffitt/128.jpg", new DateTime(2020, 6, 6, 13, 0, 8, 192, DateTimeKind.Local).AddTicks(7292) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 6, 13, 0, 8, 192, DateTimeKind.Local).AddTicks(7340), "https://s3.amazonaws.com/uifaces/faces/twitter/quailandquasar/128.jpg", new DateTime(2020, 6, 6, 13, 0, 8, 192, DateTimeKind.Local).AddTicks(7344) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 7,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 6, 13, 0, 8, 192, DateTimeKind.Local).AddTicks(7362), "https://s3.amazonaws.com/uifaces/faces/twitter/rahmeen/128.jpg", new DateTime(2020, 6, 6, 13, 0, 8, 192, DateTimeKind.Local).AddTicks(7366) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 8,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 6, 13, 0, 8, 192, DateTimeKind.Local).AddTicks(7383), "https://s3.amazonaws.com/uifaces/faces/twitter/jarsen/128.jpg", new DateTime(2020, 6, 6, 13, 0, 8, 192, DateTimeKind.Local).AddTicks(7387) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 9,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 6, 13, 0, 8, 192, DateTimeKind.Local).AddTicks(7403), "https://s3.amazonaws.com/uifaces/faces/twitter/lososina/128.jpg", new DateTime(2020, 6, 6, 13, 0, 8, 192, DateTimeKind.Local).AddTicks(7407) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 10,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 6, 13, 0, 8, 192, DateTimeKind.Local).AddTicks(7425), "https://s3.amazonaws.com/uifaces/faces/twitter/stan/128.jpg", new DateTime(2020, 6, 6, 13, 0, 8, 192, DateTimeKind.Local).AddTicks(7429) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 11,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 6, 13, 0, 8, 192, DateTimeKind.Local).AddTicks(7447), "https://s3.amazonaws.com/uifaces/faces/twitter/ruehldesign/128.jpg", new DateTime(2020, 6, 6, 13, 0, 8, 192, DateTimeKind.Local).AddTicks(7452) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 12,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 6, 13, 0, 8, 192, DateTimeKind.Local).AddTicks(7468), "https://s3.amazonaws.com/uifaces/faces/twitter/josecarlospsh/128.jpg", new DateTime(2020, 6, 6, 13, 0, 8, 192, DateTimeKind.Local).AddTicks(7472) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 13,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 6, 13, 0, 8, 192, DateTimeKind.Local).AddTicks(7488), "https://s3.amazonaws.com/uifaces/faces/twitter/supervova/128.jpg", new DateTime(2020, 6, 6, 13, 0, 8, 192, DateTimeKind.Local).AddTicks(7493) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 14,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 6, 13, 0, 8, 192, DateTimeKind.Local).AddTicks(7512), "https://s3.amazonaws.com/uifaces/faces/twitter/stushona/128.jpg", new DateTime(2020, 6, 6, 13, 0, 8, 192, DateTimeKind.Local).AddTicks(7516) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 15,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 6, 13, 0, 8, 192, DateTimeKind.Local).AddTicks(7533), "https://s3.amazonaws.com/uifaces/faces/twitter/leemunroe/128.jpg", new DateTime(2020, 6, 6, 13, 0, 8, 192, DateTimeKind.Local).AddTicks(7538) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 16,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 6, 13, 0, 8, 192, DateTimeKind.Local).AddTicks(7554), "https://s3.amazonaws.com/uifaces/faces/twitter/garand/128.jpg", new DateTime(2020, 6, 6, 13, 0, 8, 192, DateTimeKind.Local).AddTicks(7558) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 17,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 6, 13, 0, 8, 192, DateTimeKind.Local).AddTicks(7574), "https://s3.amazonaws.com/uifaces/faces/twitter/mutlu82/128.jpg", new DateTime(2020, 6, 6, 13, 0, 8, 192, DateTimeKind.Local).AddTicks(7578) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 18,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 6, 13, 0, 8, 192, DateTimeKind.Local).AddTicks(7594), "https://s3.amazonaws.com/uifaces/faces/twitter/moscoz/128.jpg", new DateTime(2020, 6, 6, 13, 0, 8, 192, DateTimeKind.Local).AddTicks(7598) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 19,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 6, 13, 0, 8, 192, DateTimeKind.Local).AddTicks(7613), "https://s3.amazonaws.com/uifaces/faces/twitter/mvdheuvel/128.jpg", new DateTime(2020, 6, 6, 13, 0, 8, 192, DateTimeKind.Local).AddTicks(7618) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 20,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 6, 13, 0, 8, 192, DateTimeKind.Local).AddTicks(7634), "https://s3.amazonaws.com/uifaces/faces/twitter/ninjad3m0/128.jpg", new DateTime(2020, 6, 6, 13, 0, 8, 192, DateTimeKind.Local).AddTicks(7638) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 21,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 6, 13, 0, 8, 197, DateTimeKind.Local).AddTicks(3062), "https://picsum.photos/640/480/?image=930", new DateTime(2020, 6, 6, 13, 0, 8, 197, DateTimeKind.Local).AddTicks(3642) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 22,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 6, 13, 0, 8, 197, DateTimeKind.Local).AddTicks(3855), "https://picsum.photos/640/480/?image=573", new DateTime(2020, 6, 6, 13, 0, 8, 197, DateTimeKind.Local).AddTicks(3883) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 23,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 6, 13, 0, 8, 197, DateTimeKind.Local).AddTicks(3905), "https://picsum.photos/640/480/?image=489", new DateTime(2020, 6, 6, 13, 0, 8, 197, DateTimeKind.Local).AddTicks(3910) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 24,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 6, 13, 0, 8, 197, DateTimeKind.Local).AddTicks(3961), "https://picsum.photos/640/480/?image=502", new DateTime(2020, 6, 6, 13, 0, 8, 197, DateTimeKind.Local).AddTicks(3966) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 25,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 6, 13, 0, 8, 197, DateTimeKind.Local).AddTicks(3983), "https://picsum.photos/640/480/?image=281", new DateTime(2020, 6, 6, 13, 0, 8, 197, DateTimeKind.Local).AddTicks(3987) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 26,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 6, 13, 0, 8, 197, DateTimeKind.Local).AddTicks(4003), "https://picsum.photos/640/480/?image=851", new DateTime(2020, 6, 6, 13, 0, 8, 197, DateTimeKind.Local).AddTicks(4007) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 27,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 6, 13, 0, 8, 197, DateTimeKind.Local).AddTicks(4024), "https://picsum.photos/640/480/?image=724", new DateTime(2020, 6, 6, 13, 0, 8, 197, DateTimeKind.Local).AddTicks(4027) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 28,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 6, 13, 0, 8, 197, DateTimeKind.Local).AddTicks(4044), "https://picsum.photos/640/480/?image=32", new DateTime(2020, 6, 6, 13, 0, 8, 197, DateTimeKind.Local).AddTicks(4048) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 29,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 6, 13, 0, 8, 197, DateTimeKind.Local).AddTicks(4064), "https://picsum.photos/640/480/?image=940", new DateTime(2020, 6, 6, 13, 0, 8, 197, DateTimeKind.Local).AddTicks(4068) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 30,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 6, 13, 0, 8, 197, DateTimeKind.Local).AddTicks(4084), "https://picsum.photos/640/480/?image=667", new DateTime(2020, 6, 6, 13, 0, 8, 197, DateTimeKind.Local).AddTicks(4088) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 31,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 6, 13, 0, 8, 197, DateTimeKind.Local).AddTicks(4104), "https://picsum.photos/640/480/?image=290", new DateTime(2020, 6, 6, 13, 0, 8, 197, DateTimeKind.Local).AddTicks(4108) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 32,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 6, 13, 0, 8, 197, DateTimeKind.Local).AddTicks(4124), "https://picsum.photos/640/480/?image=841", new DateTime(2020, 6, 6, 13, 0, 8, 197, DateTimeKind.Local).AddTicks(4128) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 33,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 6, 13, 0, 8, 197, DateTimeKind.Local).AddTicks(4143), "https://picsum.photos/640/480/?image=908", new DateTime(2020, 6, 6, 13, 0, 8, 197, DateTimeKind.Local).AddTicks(4147) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 34,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 6, 13, 0, 8, 197, DateTimeKind.Local).AddTicks(4163), "https://picsum.photos/640/480/?image=435", new DateTime(2020, 6, 6, 13, 0, 8, 197, DateTimeKind.Local).AddTicks(4167) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 35,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 6, 13, 0, 8, 197, DateTimeKind.Local).AddTicks(4182), "https://picsum.photos/640/480/?image=876", new DateTime(2020, 6, 6, 13, 0, 8, 197, DateTimeKind.Local).AddTicks(4186) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 36,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 6, 13, 0, 8, 197, DateTimeKind.Local).AddTicks(4202), "https://picsum.photos/640/480/?image=986", new DateTime(2020, 6, 6, 13, 0, 8, 197, DateTimeKind.Local).AddTicks(4206) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 37,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 6, 13, 0, 8, 197, DateTimeKind.Local).AddTicks(4250), "https://picsum.photos/640/480/?image=201", new DateTime(2020, 6, 6, 13, 0, 8, 197, DateTimeKind.Local).AddTicks(4254) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 38,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 6, 13, 0, 8, 197, DateTimeKind.Local).AddTicks(4272), "https://picsum.photos/640/480/?image=320", new DateTime(2020, 6, 6, 13, 0, 8, 197, DateTimeKind.Local).AddTicks(4276) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 39,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 6, 13, 0, 8, 197, DateTimeKind.Local).AddTicks(4291), "https://picsum.photos/640/480/?image=19", new DateTime(2020, 6, 6, 13, 0, 8, 197, DateTimeKind.Local).AddTicks(4295) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 40,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 6, 13, 0, 8, 197, DateTimeKind.Local).AddTicks(4311), "https://picsum.photos/640/480/?image=849", new DateTime(2020, 6, 6, 13, 0, 8, 197, DateTimeKind.Local).AddTicks(4314) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 16, "quam", new DateTime(2020, 6, 6, 13, 0, 8, 353, DateTimeKind.Local).AddTicks(1441), 28, new DateTime(2020, 6, 6, 13, 0, 8, 353, DateTimeKind.Local).AddTicks(1965) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 9, "In nulla est et reprehenderit ea non. Alias excepturi sit vitae. Dolorem quam rerum assumenda amet.", new DateTime(2020, 6, 6, 13, 0, 8, 353, DateTimeKind.Local).AddTicks(8068), 21, new DateTime(2020, 6, 6, 13, 0, 8, 353, DateTimeKind.Local).AddTicks(8094) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 18, "Perferendis tempore omnis pariatur quasi reiciendis et modi. Mollitia officia eum rem. Est doloremque incidunt praesentium veritatis quis perferendis.", new DateTime(2020, 6, 6, 13, 0, 8, 353, DateTimeKind.Local).AddTicks(8288), 34, new DateTime(2020, 6, 6, 13, 0, 8, 353, DateTimeKind.Local).AddTicks(8294) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 16, "Provident officia sed voluptates pariatur iste non explicabo. Dolore quo omnis vero ipsum autem nostrum amet. Tempore odio hic voluptatem repellat sequi. Non itaque illum minus commodi pariatur laborum vero sit. Eaque nobis ut placeat aperiam quia. Maiores cupiditate consectetur cum animi necessitatibus.", new DateTime(2020, 6, 6, 13, 0, 8, 353, DateTimeKind.Local).AddTicks(8564), 40, new DateTime(2020, 6, 6, 13, 0, 8, 353, DateTimeKind.Local).AddTicks(8570) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "UpdatedAt" },
                values: new object[] { 3, "Enim unde temporibus enim officia.", new DateTime(2020, 6, 6, 13, 0, 8, 353, DateTimeKind.Local).AddTicks(9197), new DateTime(2020, 6, 6, 13, 0, 8, 353, DateTimeKind.Local).AddTicks(9209) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 13, "Ea ad qui tempora optio cupiditate. Suscipit id tenetur id rerum ut similique ad. Delectus magni voluptas quae qui aut. Excepturi laboriosam nemo perspiciatis quia ducimus corporis magni eum esse. Sit quo consequuntur sapiente voluptatibus architecto dolorem distinctio quia.", new DateTime(2020, 6, 6, 13, 0, 8, 353, DateTimeKind.Local).AddTicks(9444), 31, new DateTime(2020, 6, 6, 13, 0, 8, 353, DateTimeKind.Local).AddTicks(9450) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 7,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 4, @"Asperiores fugit esse aut.
Similique similique aliquid autem rerum necessitatibus iure.
Unde rerum ullam qui voluptates modi atque unde error ut.", new DateTime(2020, 6, 6, 13, 0, 8, 354, DateTimeKind.Local).AddTicks(36), 21, new DateTime(2020, 6, 6, 13, 0, 8, 354, DateTimeKind.Local).AddTicks(47) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 8,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 5, "Neque necessitatibus iure voluptates impedit accusantium quia sit suscipit necessitatibus.", new DateTime(2020, 6, 6, 13, 0, 8, 354, DateTimeKind.Local).AddTicks(122), 24, new DateTime(2020, 6, 6, 13, 0, 8, 354, DateTimeKind.Local).AddTicks(127) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 9,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 8, "Suscipit et architecto mollitia explicabo et est qui. Est voluptate et libero molestiae assumenda velit eum. Est voluptate laudantium maiores aut.", new DateTime(2020, 6, 6, 13, 0, 8, 354, DateTimeKind.Local).AddTicks(259), 24, new DateTime(2020, 6, 6, 13, 0, 8, 354, DateTimeKind.Local).AddTicks(264) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 10,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 14, @"Velit nam tempore quis enim aut tempora.
Non omnis sequi.
Reprehenderit et cumque modi ullam.", new DateTime(2020, 6, 6, 13, 0, 8, 354, DateTimeKind.Local).AddTicks(351), 25, new DateTime(2020, 6, 6, 13, 0, 8, 354, DateTimeKind.Local).AddTicks(356) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 11,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 18, "Voluptas saepe neque consequatur quia aliquid. Saepe ut odio dolore eius. Voluptatem perferendis aperiam provident temporibus sed. Atque ad amet quam perferendis qui quis facere. Eos alias laborum maiores recusandae suscipit dolor non praesentium cum.", new DateTime(2020, 6, 6, 13, 0, 8, 354, DateTimeKind.Local).AddTicks(534), 38, new DateTime(2020, 6, 6, 13, 0, 8, 354, DateTimeKind.Local).AddTicks(539) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 12,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 1, "voluptatem", new DateTime(2020, 6, 6, 13, 0, 8, 354, DateTimeKind.Local).AddTicks(587), 35, new DateTime(2020, 6, 6, 13, 0, 8, 354, DateTimeKind.Local).AddTicks(592) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 13,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 5, "expedita", new DateTime(2020, 6, 6, 13, 0, 8, 354, DateTimeKind.Local).AddTicks(619), 34, new DateTime(2020, 6, 6, 13, 0, 8, 354, DateTimeKind.Local).AddTicks(624) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 14,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 2, "Accusamus omnis et asperiores illo impedit quod autem.", new DateTime(2020, 6, 6, 13, 0, 8, 354, DateTimeKind.Local).AddTicks(702), 37, new DateTime(2020, 6, 6, 13, 0, 8, 354, DateTimeKind.Local).AddTicks(708) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 15,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 10, @"Odio harum ut.
Nobis ut dolorem quasi fugit quia.", new DateTime(2020, 6, 6, 13, 0, 8, 354, DateTimeKind.Local).AddTicks(771), 37, new DateTime(2020, 6, 6, 13, 0, 8, 354, DateTimeKind.Local).AddTicks(776) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 16,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 14, "Nulla id id unde et dignissimos. Veritatis vitae earum veniam. Rem libero et. Quia sit modi minima eum itaque sit repellat ut.", new DateTime(2020, 6, 6, 13, 0, 8, 354, DateTimeKind.Local).AddTicks(883), 29, new DateTime(2020, 6, 6, 13, 0, 8, 354, DateTimeKind.Local).AddTicks(888) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 17,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 11, "Perspiciatis explicabo et. Hic architecto praesentium ut in. Nemo sunt voluptatem provident in neque aliquid in.", new DateTime(2020, 6, 6, 13, 0, 8, 354, DateTimeKind.Local).AddTicks(1021), 25, new DateTime(2020, 6, 6, 13, 0, 8, 354, DateTimeKind.Local).AddTicks(1025) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 18,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 2, @"Consectetur sit et omnis id in dolor qui.
Fugiat alias in consectetur eum magni.
Temporibus aliquid ex dolorum sunt aut vel repellendus.
Animi aut dicta modi molestias porro in fugit.", new DateTime(2020, 6, 6, 13, 0, 8, 354, DateTimeKind.Local).AddTicks(1179), 28, new DateTime(2020, 6, 6, 13, 0, 8, 354, DateTimeKind.Local).AddTicks(1185) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 19,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 18, "Cupiditate at qui.", new DateTime(2020, 6, 6, 13, 0, 8, 354, DateTimeKind.Local).AddTicks(1224), 25, new DateTime(2020, 6, 6, 13, 0, 8, 354, DateTimeKind.Local).AddTicks(1228) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 20,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 5, "Magni quia suscipit modi. Praesentium nesciunt ratione consequatur neque eius sed quia. Consequatur assumenda sunt animi accusamus sed nihil quis fugiat omnis. Soluta ut et sapiente in maxime ipsa beatae quidem. Reprehenderit rem et ut quae natus et.", new DateTime(2020, 6, 6, 13, 0, 8, 354, DateTimeKind.Local).AddTicks(1414), 27, new DateTime(2020, 6, 6, 13, 0, 8, 354, DateTimeKind.Local).AddTicks(1419) });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 14, new DateTime(2020, 6, 6, 13, 0, 8, 223, DateTimeKind.Local).AddTicks(9719), "Gloria.Fahey@hotmail.com", "p01IEmuRHHkKskT+U3nmCo/PAB39hESjzZpP9dy8ISU=", "ETXlCLHidic9WzcwteG3zuOlvoeEY8/D8qM9G1dvOGI=", new DateTime(2020, 6, 6, 13, 0, 8, 224, DateTimeKind.Local).AddTicks(279), "Harvey.Hodkiewicz" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 17, new DateTime(2020, 6, 6, 13, 0, 8, 230, DateTimeKind.Local).AddTicks(2878), "Stephania74@gmail.com", "7S+ErIgX8maQ2SGK68ytMXTbHVoMqM09xiKXIWzx8S8=", "7/ymxZsIlII39OYMXGgbUIE72ZB1WlPkQYyCVnVr3DE=", new DateTime(2020, 6, 6, 13, 0, 8, 230, DateTimeKind.Local).AddTicks(2901), "Noble12" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 10, new DateTime(2020, 6, 6, 13, 0, 8, 236, DateTimeKind.Local).AddTicks(3860), "Aileen_Berge36@yahoo.com", "KeVByYjQDr9z6MGFTuNIf4AiRUPq1mfzXO7ARWcwaWs=", "guwIpmEI8YpGU2LjysrfW6VQneIU1udtdkLuLb+FbH8=", new DateTime(2020, 6, 6, 13, 0, 8, 236, DateTimeKind.Local).AddTicks(3872), "Rusty_Braun" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 18, new DateTime(2020, 6, 6, 13, 0, 8, 242, DateTimeKind.Local).AddTicks(7653), "Eino3@gmail.com", "Onjoea8kerlXKquz2OnKFqj0pUdplybQXcSTNl2HwMI=", "hSXCx/Nr3Nv7YDW2GZ3OuauJu9aD0mH4Zppi97GVbgg=", new DateTime(2020, 6, 6, 13, 0, 8, 242, DateTimeKind.Local).AddTicks(7666), "Jarred.Ratke" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 1, new DateTime(2020, 6, 6, 13, 0, 8, 248, DateTimeKind.Local).AddTicks(7144), "Nikolas.Bednar@hotmail.com", "ThPgGgMH8oBXm0c88HuqYTqeEB/tF5v+zjYH/2Tl28Y=", "7d8Gpe8zp6Br8fcF4ffb5FutLPTC1qeQ6fqzgNPx828=", new DateTime(2020, 6, 6, 13, 0, 8, 248, DateTimeKind.Local).AddTicks(7155), "Alek75" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 15, new DateTime(2020, 6, 6, 13, 0, 8, 254, DateTimeKind.Local).AddTicks(6897), "Alaina.Kunde@gmail.com", "PofzQH1xFwL3zNOcfD1va6eT8MalEP3BcifrF8v1IAc=", "lzrnWyHNuFi7R23fl4SpIfCVypnPgPo6sDi02m1gvXE=", new DateTime(2020, 6, 6, 13, 0, 8, 254, DateTimeKind.Local).AddTicks(6908), "Kip_Price" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 7,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 8, new DateTime(2020, 6, 6, 13, 0, 8, 261, DateTimeKind.Local).AddTicks(4789), "Patricia.Ratke@yahoo.com", "2INUP/Uo9LY3RpHKF5QQEw4YwMOOEEhiHVdMbdI6nas=", "vpAd9nwM+IUoloGBWfTAQ3SX0AV1mjCxkOH5JrhqvA4=", new DateTime(2020, 6, 6, 13, 0, 8, 261, DateTimeKind.Local).AddTicks(4828), "Orpha_Bins" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 8,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 15, new DateTime(2020, 6, 6, 13, 0, 8, 267, DateTimeKind.Local).AddTicks(9757), "Marty82@gmail.com", "Evqt7iNfUW4s58xKakC2q55EcnXDV4uBwIjrgZUB93g=", "VQy6MZ6sITtPMi/aDOppOqM6AtaVelH7ydnAmGNJhs8=", new DateTime(2020, 6, 6, 13, 0, 8, 267, DateTimeKind.Local).AddTicks(9793), "Misty94" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 9,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 7, new DateTime(2020, 6, 6, 13, 0, 8, 274, DateTimeKind.Local).AddTicks(3401), "Kristy_Goodwin47@hotmail.com", "E1ITyGJMCKtwKKRU/KbVmp4OLfVa6++d/Pa89esaS0E=", "m3Gw4QKpOsrSKu3kFQRlJEWupTdhEr8egye1RNqpiEQ=", new DateTime(2020, 6, 6, 13, 0, 8, 274, DateTimeKind.Local).AddTicks(3426), "Terrell84" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 10,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 3, new DateTime(2020, 6, 6, 13, 0, 8, 280, DateTimeKind.Local).AddTicks(6154), "Elouise5@gmail.com", "WEXGs1wb08GyoZfoUTlQ4oJa4cu2ARWT5zHDPkJGzJ4=", "aV8LRLNAqHAEd3Sur4ff/UmwmftewHbqQ/McuDbtnRI=", new DateTime(2020, 6, 6, 13, 0, 8, 280, DateTimeKind.Local).AddTicks(6180), "Cecile.Hermiston" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 11,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 9, new DateTime(2020, 6, 6, 13, 0, 8, 286, DateTimeKind.Local).AddTicks(5626), "Anabel.Lehner@hotmail.com", "k1AE4gK5ew8n20lJI/oKPOUes3Q70hh66eygW0eEeXQ=", "9H3vpz5Gc7NWqCN73WUEYG1x9W2YqsE11aPc8S9B5+w=", new DateTime(2020, 6, 6, 13, 0, 8, 286, DateTimeKind.Local).AddTicks(5636), "Micah_Hamill63" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 12,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 11, new DateTime(2020, 6, 6, 13, 0, 8, 292, DateTimeKind.Local).AddTicks(7003), "Jesus52@hotmail.com", "+POyvtImnjNYMYGVi6abP82pa1J3WVyMKScQOaku8TM=", "gkyuJ/yavXnk5FI3KRwWB+ul+QBVl3398CnfVBWtCL4=", new DateTime(2020, 6, 6, 13, 0, 8, 292, DateTimeKind.Local).AddTicks(7013), "Brando66" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 13,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 10, new DateTime(2020, 6, 6, 13, 0, 8, 298, DateTimeKind.Local).AddTicks(6376), "Dianna.Hane4@hotmail.com", "2m8CeJI3FaQrzeStgoExjJh77p3tdlWvxSQg92YFjYY=", "AT/KlPpj2xeA0vIc1mc4ly7/kxIOBFUbL2cj3NbppSc=", new DateTime(2020, 6, 6, 13, 0, 8, 298, DateTimeKind.Local).AddTicks(6386), "Cassandra83" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 14,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 18, new DateTime(2020, 6, 6, 13, 0, 8, 304, DateTimeKind.Local).AddTicks(5954), "Curt.Auer7@yahoo.com", "j7t6/mRhaIEw732xYGYC+nJ4/4ifaGBzTkHJwQ7SjKQ=", "wRefIHIYH38TK20A943Ee7TkVYynHRrUjBzd8oCsdI8=", new DateTime(2020, 6, 6, 13, 0, 8, 304, DateTimeKind.Local).AddTicks(5964), "Leonor_Casper53" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 15,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 19, new DateTime(2020, 6, 6, 13, 0, 8, 310, DateTimeKind.Local).AddTicks(8788), "Jeromy.Stracke70@yahoo.com", "epqjjqmwMciDAPjlkY6KCCJxO7ZLpl8TUifDy6ab9E4=", "4CbL1LxkFOi7tkzvRXDMT3B/NDl5OKBhdFd4IC5wWUY=", new DateTime(2020, 6, 6, 13, 0, 8, 310, DateTimeKind.Local).AddTicks(8827), "Elisha26" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 16,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 12, new DateTime(2020, 6, 6, 13, 0, 8, 317, DateTimeKind.Local).AddTicks(403), "Cristopher7@gmail.com", "QNgeRuG45s+9/wNlHYyz3fAJOFKYQEHf1Wslcg6Wr6I=", "M70vsKKE4GVfkaN4Gh4EoNGXaMk2PQVrsSXr25xbokg=", new DateTime(2020, 6, 6, 13, 0, 8, 317, DateTimeKind.Local).AddTicks(414), "Wilfrid75" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 17,
                columns: new[] { "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { new DateTime(2020, 6, 6, 13, 0, 8, 323, DateTimeKind.Local).AddTicks(322), "Jordyn72@yahoo.com", "l346x32HkFphsGMt5GNNLdt6Jg7YfnxIdaS4ASUZyeE=", "d2PMIFyVEwDjF0y5kFwIrWCStQ/3TI7YMfHNRWwTouo=", new DateTime(2020, 6, 6, 13, 0, 8, 323, DateTimeKind.Local).AddTicks(333), "Thad65" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 18,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 8, new DateTime(2020, 6, 6, 13, 0, 8, 329, DateTimeKind.Local).AddTicks(1574), "Frederick97@yahoo.com", "KdLGvzQesGeA74y2nQp1RaOCdo4WaOC33dya4ZfVxiQ=", "Ka30dD+kklToND3ZUCLEvcr7mvykY9AspvWSKenx/Lw=", new DateTime(2020, 6, 6, 13, 0, 8, 329, DateTimeKind.Local).AddTicks(1584), "Vince40" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 19,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 16, new DateTime(2020, 6, 6, 13, 0, 8, 335, DateTimeKind.Local).AddTicks(759), "King_Senger@hotmail.com", "XFL2WyBXbttF+tig71O2vyI74jS2cX7e5NkM8leeq0s=", "SNkaqYqzn9+vDXa1OIFvfymUoZWEgtFB0PzH7JW6eH4=", new DateTime(2020, 6, 6, 13, 0, 8, 335, DateTimeKind.Local).AddTicks(770), "Yasmine_Jaskolski42" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 20,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 17, new DateTime(2020, 6, 6, 13, 0, 8, 341, DateTimeKind.Local).AddTicks(4437), "Nikko39@yahoo.com", "sGT5ejxc+LIb/ByibE2J023zXaGcuNklUyQLED8OswA=", "s6hcd6gZ/5Vmww4joHPbQSn749yymNMXqgdBXxjZRf0=", new DateTime(2020, 6, 6, 13, 0, 8, 341, DateTimeKind.Local).AddTicks(4446), "Cordie99" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 21,
                columns: new[] { "CreatedAt", "Password", "Salt", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 6, 13, 0, 8, 347, DateTimeKind.Local).AddTicks(6013), "gcDOwt4iZM3kvZOrWbIeZTt34STVb17hY4ya09GE4/U=", "uUxTwtEalV4t3jeQL5RH6eFDOo0p3QGVCA8kiigJ+mU=", new DateTime(2020, 6, 6, 13, 0, 8, 347, DateTimeKind.Local).AddTicks(6013) });
        }
    }
}
