﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Thread_.NET.DAL.Migrations
{
    public partial class Add_IsDeleted_Field_To_Comment : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_PostReactions_PostId",
                table: "PostReactions");

            migrationBuilder.DropIndex(
                name: "IX_CommentReactions_CommentId",
                table: "CommentReactions");

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 5);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 6);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 7);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 8);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 9);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 10);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 11);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 12);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 13);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 14);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 15);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 16);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 17);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 18);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 19);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 20);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 5);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 6);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 7);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 8);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 9);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 10);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 11);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 12);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 13);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 14);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 15);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 16);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 17);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 18);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 19);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 20);

            migrationBuilder.AddColumn<bool>(
                name: "IsDeleted",
                table: "Comments",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddUniqueConstraint(
                name: "AK_PostReactions_PostId_UserId",
                table: "PostReactions",
                columns: new[] { "PostId", "UserId" });

            migrationBuilder.AddUniqueConstraint(
                name: "AK_CommentReactions_CommentId_UserId",
                table: "CommentReactions",
                columns: new[] { "CommentId", "UserId" });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 16, "Sed sint dolorum nulla fuga magni tempora inventore et.", new DateTime(2020, 6, 6, 13, 0, 8, 358, DateTimeKind.Local).AddTicks(8501), 17, new DateTime(2020, 6, 6, 13, 0, 8, 358, DateTimeKind.Local).AddTicks(8992) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 9, "Natus velit alias adipisci distinctio amet.", new DateTime(2020, 6, 6, 13, 0, 8, 358, DateTimeKind.Local).AddTicks(9501), 9, new DateTime(2020, 6, 6, 13, 0, 8, 358, DateTimeKind.Local).AddTicks(9515) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 7, "Consequuntur corrupti eum et.", new DateTime(2020, 6, 6, 13, 0, 8, 358, DateTimeKind.Local).AddTicks(9573), 5, new DateTime(2020, 6, 6, 13, 0, 8, 358, DateTimeKind.Local).AddTicks(9578) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 21, "Dolorem natus asperiores.", new DateTime(2020, 6, 6, 13, 0, 8, 358, DateTimeKind.Local).AddTicks(9617), 13, new DateTime(2020, 6, 6, 13, 0, 8, 358, DateTimeKind.Local).AddTicks(9621) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 15, "Eligendi omnis facilis et suscipit ut occaecati voluptatibus praesentium et.", new DateTime(2020, 6, 6, 13, 0, 8, 358, DateTimeKind.Local).AddTicks(9694), 1, new DateTime(2020, 6, 6, 13, 0, 8, 358, DateTimeKind.Local).AddTicks(9699) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { "Blanditiis ut at eveniet dolore necessitatibus dolor distinctio asperiores et.", new DateTime(2020, 6, 6, 13, 0, 8, 358, DateTimeKind.Local).AddTicks(9760), 5, new DateTime(2020, 6, 6, 13, 0, 8, 358, DateTimeKind.Local).AddTicks(9765) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 7,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 19, "Et ipsum ut inventore id.", new DateTime(2020, 6, 6, 13, 0, 8, 358, DateTimeKind.Local).AddTicks(9871), 15, new DateTime(2020, 6, 6, 13, 0, 8, 358, DateTimeKind.Local).AddTicks(9876) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 8,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 5, "Quisquam et unde aut pariatur ratione eveniet.", new DateTime(2020, 6, 6, 13, 0, 8, 358, DateTimeKind.Local).AddTicks(9929), 14, new DateTime(2020, 6, 6, 13, 0, 8, 358, DateTimeKind.Local).AddTicks(9933) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 9,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 16, "Impedit animi eum est occaecati ut distinctio.", new DateTime(2020, 6, 6, 13, 0, 8, 358, DateTimeKind.Local).AddTicks(9981), 2, new DateTime(2020, 6, 6, 13, 0, 8, 358, DateTimeKind.Local).AddTicks(9985) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 10,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 9, "Minima optio voluptas voluptate quam consequatur ea ut placeat.", new DateTime(2020, 6, 6, 13, 0, 8, 359, DateTimeKind.Local).AddTicks(45), 4, new DateTime(2020, 6, 6, 13, 0, 8, 359, DateTimeKind.Local).AddTicks(49) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 11,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 10, "Ad exercitationem id exercitationem nemo necessitatibus qui maxime rerum.", new DateTime(2020, 6, 6, 13, 0, 8, 359, DateTimeKind.Local).AddTicks(103), 7, new DateTime(2020, 6, 6, 13, 0, 8, 359, DateTimeKind.Local).AddTicks(107) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 12,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 19, "Provident et hic dolores et ut in doloribus assumenda.", new DateTime(2020, 6, 6, 13, 0, 8, 359, DateTimeKind.Local).AddTicks(186), 7, new DateTime(2020, 6, 6, 13, 0, 8, 359, DateTimeKind.Local).AddTicks(191) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 13,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 18, "Repudiandae qui officia.", new DateTime(2020, 6, 6, 13, 0, 8, 359, DateTimeKind.Local).AddTicks(230), 16, new DateTime(2020, 6, 6, 13, 0, 8, 359, DateTimeKind.Local).AddTicks(234) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 14,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 5, "Dignissimos ut voluptate saepe voluptas dicta eaque.", new DateTime(2020, 6, 6, 13, 0, 8, 359, DateTimeKind.Local).AddTicks(281), 14, new DateTime(2020, 6, 6, 13, 0, 8, 359, DateTimeKind.Local).AddTicks(286) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 15,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 5, "Aliquid ut ut consequatur quis voluptatum debitis autem illo.", new DateTime(2020, 6, 6, 13, 0, 8, 359, DateTimeKind.Local).AddTicks(340), 14, new DateTime(2020, 6, 6, 13, 0, 8, 359, DateTimeKind.Local).AddTicks(344) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 16,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 8, "Alias sit quae reprehenderit a quo.", new DateTime(2020, 6, 6, 13, 0, 8, 359, DateTimeKind.Local).AddTicks(389), 4, new DateTime(2020, 6, 6, 13, 0, 8, 359, DateTimeKind.Local).AddTicks(393) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 17,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 10, "Unde sapiente suscipit accusantium veritatis.", new DateTime(2020, 6, 6, 13, 0, 8, 359, DateTimeKind.Local).AddTicks(436), 13, new DateTime(2020, 6, 6, 13, 0, 8, 359, DateTimeKind.Local).AddTicks(440) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 18,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 14, "Et explicabo molestiae id harum.", new DateTime(2020, 6, 6, 13, 0, 8, 359, DateTimeKind.Local).AddTicks(546), 9, new DateTime(2020, 6, 6, 13, 0, 8, 359, DateTimeKind.Local).AddTicks(551) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 19,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 5, "Non est odio voluptatem autem temporibus iste at sapiente ut.", new DateTime(2020, 6, 6, 13, 0, 8, 359, DateTimeKind.Local).AddTicks(612), 18, new DateTime(2020, 6, 6, 13, 0, 8, 359, DateTimeKind.Local).AddTicks(616) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 20,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 1, "Consequuntur quia nulla dolores laudantium aut iure harum repellendus nemo.", new DateTime(2020, 6, 6, 13, 0, 8, 359, DateTimeKind.Local).AddTicks(675), 5, new DateTime(2020, 6, 6, 13, 0, 8, 359, DateTimeKind.Local).AddTicks(679) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 6, 13, 0, 8, 192, DateTimeKind.Local).AddTicks(2789), "https://s3.amazonaws.com/uifaces/faces/twitter/otozk/128.jpg", new DateTime(2020, 6, 6, 13, 0, 8, 192, DateTimeKind.Local).AddTicks(6548) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 6, 13, 0, 8, 192, DateTimeKind.Local).AddTicks(7193), "https://s3.amazonaws.com/uifaces/faces/twitter/catadeleon/128.jpg", new DateTime(2020, 6, 6, 13, 0, 8, 192, DateTimeKind.Local).AddTicks(7213) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 6, 13, 0, 8, 192, DateTimeKind.Local).AddTicks(7243), "https://s3.amazonaws.com/uifaces/faces/twitter/leehambley/128.jpg", new DateTime(2020, 6, 6, 13, 0, 8, 192, DateTimeKind.Local).AddTicks(7247) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 6, 13, 0, 8, 192, DateTimeKind.Local).AddTicks(7266), "https://s3.amazonaws.com/uifaces/faces/twitter/drewbyreese/128.jpg", new DateTime(2020, 6, 6, 13, 0, 8, 192, DateTimeKind.Local).AddTicks(7270) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 6, 13, 0, 8, 192, DateTimeKind.Local).AddTicks(7288), "https://s3.amazonaws.com/uifaces/faces/twitter/vaughanmoffitt/128.jpg", new DateTime(2020, 6, 6, 13, 0, 8, 192, DateTimeKind.Local).AddTicks(7292) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 6, 13, 0, 8, 192, DateTimeKind.Local).AddTicks(7340), "https://s3.amazonaws.com/uifaces/faces/twitter/quailandquasar/128.jpg", new DateTime(2020, 6, 6, 13, 0, 8, 192, DateTimeKind.Local).AddTicks(7344) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 7,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 6, 13, 0, 8, 192, DateTimeKind.Local).AddTicks(7362), "https://s3.amazonaws.com/uifaces/faces/twitter/rahmeen/128.jpg", new DateTime(2020, 6, 6, 13, 0, 8, 192, DateTimeKind.Local).AddTicks(7366) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 8,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 6, 13, 0, 8, 192, DateTimeKind.Local).AddTicks(7383), "https://s3.amazonaws.com/uifaces/faces/twitter/jarsen/128.jpg", new DateTime(2020, 6, 6, 13, 0, 8, 192, DateTimeKind.Local).AddTicks(7387) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 9,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 6, 13, 0, 8, 192, DateTimeKind.Local).AddTicks(7403), "https://s3.amazonaws.com/uifaces/faces/twitter/lososina/128.jpg", new DateTime(2020, 6, 6, 13, 0, 8, 192, DateTimeKind.Local).AddTicks(7407) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 10,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 6, 13, 0, 8, 192, DateTimeKind.Local).AddTicks(7425), "https://s3.amazonaws.com/uifaces/faces/twitter/stan/128.jpg", new DateTime(2020, 6, 6, 13, 0, 8, 192, DateTimeKind.Local).AddTicks(7429) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 11,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 6, 13, 0, 8, 192, DateTimeKind.Local).AddTicks(7447), "https://s3.amazonaws.com/uifaces/faces/twitter/ruehldesign/128.jpg", new DateTime(2020, 6, 6, 13, 0, 8, 192, DateTimeKind.Local).AddTicks(7452) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 12,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 6, 13, 0, 8, 192, DateTimeKind.Local).AddTicks(7468), "https://s3.amazonaws.com/uifaces/faces/twitter/josecarlospsh/128.jpg", new DateTime(2020, 6, 6, 13, 0, 8, 192, DateTimeKind.Local).AddTicks(7472) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 13,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 6, 13, 0, 8, 192, DateTimeKind.Local).AddTicks(7488), "https://s3.amazonaws.com/uifaces/faces/twitter/supervova/128.jpg", new DateTime(2020, 6, 6, 13, 0, 8, 192, DateTimeKind.Local).AddTicks(7493) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 14,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 6, 13, 0, 8, 192, DateTimeKind.Local).AddTicks(7512), "https://s3.amazonaws.com/uifaces/faces/twitter/stushona/128.jpg", new DateTime(2020, 6, 6, 13, 0, 8, 192, DateTimeKind.Local).AddTicks(7516) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 15,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 6, 13, 0, 8, 192, DateTimeKind.Local).AddTicks(7533), "https://s3.amazonaws.com/uifaces/faces/twitter/leemunroe/128.jpg", new DateTime(2020, 6, 6, 13, 0, 8, 192, DateTimeKind.Local).AddTicks(7538) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 16,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 6, 13, 0, 8, 192, DateTimeKind.Local).AddTicks(7554), "https://s3.amazonaws.com/uifaces/faces/twitter/garand/128.jpg", new DateTime(2020, 6, 6, 13, 0, 8, 192, DateTimeKind.Local).AddTicks(7558) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 17,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 6, 13, 0, 8, 192, DateTimeKind.Local).AddTicks(7574), "https://s3.amazonaws.com/uifaces/faces/twitter/mutlu82/128.jpg", new DateTime(2020, 6, 6, 13, 0, 8, 192, DateTimeKind.Local).AddTicks(7578) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 18,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 6, 13, 0, 8, 192, DateTimeKind.Local).AddTicks(7594), "https://s3.amazonaws.com/uifaces/faces/twitter/moscoz/128.jpg", new DateTime(2020, 6, 6, 13, 0, 8, 192, DateTimeKind.Local).AddTicks(7598) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 19,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 6, 13, 0, 8, 192, DateTimeKind.Local).AddTicks(7613), "https://s3.amazonaws.com/uifaces/faces/twitter/mvdheuvel/128.jpg", new DateTime(2020, 6, 6, 13, 0, 8, 192, DateTimeKind.Local).AddTicks(7618) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 20,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 6, 13, 0, 8, 192, DateTimeKind.Local).AddTicks(7634), "https://s3.amazonaws.com/uifaces/faces/twitter/ninjad3m0/128.jpg", new DateTime(2020, 6, 6, 13, 0, 8, 192, DateTimeKind.Local).AddTicks(7638) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 21,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 6, 13, 0, 8, 197, DateTimeKind.Local).AddTicks(3062), "https://picsum.photos/640/480/?image=930", new DateTime(2020, 6, 6, 13, 0, 8, 197, DateTimeKind.Local).AddTicks(3642) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 22,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 6, 13, 0, 8, 197, DateTimeKind.Local).AddTicks(3855), "https://picsum.photos/640/480/?image=573", new DateTime(2020, 6, 6, 13, 0, 8, 197, DateTimeKind.Local).AddTicks(3883) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 23,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 6, 13, 0, 8, 197, DateTimeKind.Local).AddTicks(3905), "https://picsum.photos/640/480/?image=489", new DateTime(2020, 6, 6, 13, 0, 8, 197, DateTimeKind.Local).AddTicks(3910) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 24,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 6, 13, 0, 8, 197, DateTimeKind.Local).AddTicks(3961), "https://picsum.photos/640/480/?image=502", new DateTime(2020, 6, 6, 13, 0, 8, 197, DateTimeKind.Local).AddTicks(3966) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 25,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 6, 13, 0, 8, 197, DateTimeKind.Local).AddTicks(3983), "https://picsum.photos/640/480/?image=281", new DateTime(2020, 6, 6, 13, 0, 8, 197, DateTimeKind.Local).AddTicks(3987) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 26,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 6, 13, 0, 8, 197, DateTimeKind.Local).AddTicks(4003), "https://picsum.photos/640/480/?image=851", new DateTime(2020, 6, 6, 13, 0, 8, 197, DateTimeKind.Local).AddTicks(4007) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 27,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 6, 13, 0, 8, 197, DateTimeKind.Local).AddTicks(4024), "https://picsum.photos/640/480/?image=724", new DateTime(2020, 6, 6, 13, 0, 8, 197, DateTimeKind.Local).AddTicks(4027) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 28,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 6, 13, 0, 8, 197, DateTimeKind.Local).AddTicks(4044), "https://picsum.photos/640/480/?image=32", new DateTime(2020, 6, 6, 13, 0, 8, 197, DateTimeKind.Local).AddTicks(4048) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 29,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 6, 13, 0, 8, 197, DateTimeKind.Local).AddTicks(4064), "https://picsum.photos/640/480/?image=940", new DateTime(2020, 6, 6, 13, 0, 8, 197, DateTimeKind.Local).AddTicks(4068) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 30,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 6, 13, 0, 8, 197, DateTimeKind.Local).AddTicks(4084), "https://picsum.photos/640/480/?image=667", new DateTime(2020, 6, 6, 13, 0, 8, 197, DateTimeKind.Local).AddTicks(4088) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 31,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 6, 13, 0, 8, 197, DateTimeKind.Local).AddTicks(4104), "https://picsum.photos/640/480/?image=290", new DateTime(2020, 6, 6, 13, 0, 8, 197, DateTimeKind.Local).AddTicks(4108) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 32,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 6, 13, 0, 8, 197, DateTimeKind.Local).AddTicks(4124), "https://picsum.photos/640/480/?image=841", new DateTime(2020, 6, 6, 13, 0, 8, 197, DateTimeKind.Local).AddTicks(4128) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 33,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 6, 13, 0, 8, 197, DateTimeKind.Local).AddTicks(4143), "https://picsum.photos/640/480/?image=908", new DateTime(2020, 6, 6, 13, 0, 8, 197, DateTimeKind.Local).AddTicks(4147) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 34,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 6, 13, 0, 8, 197, DateTimeKind.Local).AddTicks(4163), "https://picsum.photos/640/480/?image=435", new DateTime(2020, 6, 6, 13, 0, 8, 197, DateTimeKind.Local).AddTicks(4167) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 35,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 6, 13, 0, 8, 197, DateTimeKind.Local).AddTicks(4182), "https://picsum.photos/640/480/?image=876", new DateTime(2020, 6, 6, 13, 0, 8, 197, DateTimeKind.Local).AddTicks(4186) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 36,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 6, 13, 0, 8, 197, DateTimeKind.Local).AddTicks(4202), "https://picsum.photos/640/480/?image=986", new DateTime(2020, 6, 6, 13, 0, 8, 197, DateTimeKind.Local).AddTicks(4206) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 37,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 6, 13, 0, 8, 197, DateTimeKind.Local).AddTicks(4250), "https://picsum.photos/640/480/?image=201", new DateTime(2020, 6, 6, 13, 0, 8, 197, DateTimeKind.Local).AddTicks(4254) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 38,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 6, 13, 0, 8, 197, DateTimeKind.Local).AddTicks(4272), "https://picsum.photos/640/480/?image=320", new DateTime(2020, 6, 6, 13, 0, 8, 197, DateTimeKind.Local).AddTicks(4276) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 39,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 6, 13, 0, 8, 197, DateTimeKind.Local).AddTicks(4291), "https://picsum.photos/640/480/?image=19", new DateTime(2020, 6, 6, 13, 0, 8, 197, DateTimeKind.Local).AddTicks(4295) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 40,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 6, 13, 0, 8, 197, DateTimeKind.Local).AddTicks(4311), "https://picsum.photos/640/480/?image=849", new DateTime(2020, 6, 6, 13, 0, 8, 197, DateTimeKind.Local).AddTicks(4314) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 16, "quam", new DateTime(2020, 6, 6, 13, 0, 8, 353, DateTimeKind.Local).AddTicks(1441), 28, new DateTime(2020, 6, 6, 13, 0, 8, 353, DateTimeKind.Local).AddTicks(1965) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { "In nulla est et reprehenderit ea non. Alias excepturi sit vitae. Dolorem quam rerum assumenda amet.", new DateTime(2020, 6, 6, 13, 0, 8, 353, DateTimeKind.Local).AddTicks(8068), 21, new DateTime(2020, 6, 6, 13, 0, 8, 353, DateTimeKind.Local).AddTicks(8094) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 18, "Perferendis tempore omnis pariatur quasi reiciendis et modi. Mollitia officia eum rem. Est doloremque incidunt praesentium veritatis quis perferendis.", new DateTime(2020, 6, 6, 13, 0, 8, 353, DateTimeKind.Local).AddTicks(8288), 34, new DateTime(2020, 6, 6, 13, 0, 8, 353, DateTimeKind.Local).AddTicks(8294) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 16, "Provident officia sed voluptates pariatur iste non explicabo. Dolore quo omnis vero ipsum autem nostrum amet. Tempore odio hic voluptatem repellat sequi. Non itaque illum minus commodi pariatur laborum vero sit. Eaque nobis ut placeat aperiam quia. Maiores cupiditate consectetur cum animi necessitatibus.", new DateTime(2020, 6, 6, 13, 0, 8, 353, DateTimeKind.Local).AddTicks(8564), 40, new DateTime(2020, 6, 6, 13, 0, 8, 353, DateTimeKind.Local).AddTicks(8570) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 3, "Enim unde temporibus enim officia.", new DateTime(2020, 6, 6, 13, 0, 8, 353, DateTimeKind.Local).AddTicks(9197), 28, new DateTime(2020, 6, 6, 13, 0, 8, 353, DateTimeKind.Local).AddTicks(9209) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 13, "Ea ad qui tempora optio cupiditate. Suscipit id tenetur id rerum ut similique ad. Delectus magni voluptas quae qui aut. Excepturi laboriosam nemo perspiciatis quia ducimus corporis magni eum esse. Sit quo consequuntur sapiente voluptatibus architecto dolorem distinctio quia.", new DateTime(2020, 6, 6, 13, 0, 8, 353, DateTimeKind.Local).AddTicks(9444), 31, new DateTime(2020, 6, 6, 13, 0, 8, 353, DateTimeKind.Local).AddTicks(9450) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 7,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 4, @"Asperiores fugit esse aut.
Similique similique aliquid autem rerum necessitatibus iure.
Unde rerum ullam qui voluptates modi atque unde error ut.", new DateTime(2020, 6, 6, 13, 0, 8, 354, DateTimeKind.Local).AddTicks(36), 21, new DateTime(2020, 6, 6, 13, 0, 8, 354, DateTimeKind.Local).AddTicks(47) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 8,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 5, "Neque necessitatibus iure voluptates impedit accusantium quia sit suscipit necessitatibus.", new DateTime(2020, 6, 6, 13, 0, 8, 354, DateTimeKind.Local).AddTicks(122), 24, new DateTime(2020, 6, 6, 13, 0, 8, 354, DateTimeKind.Local).AddTicks(127) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 9,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 8, "Suscipit et architecto mollitia explicabo et est qui. Est voluptate et libero molestiae assumenda velit eum. Est voluptate laudantium maiores aut.", new DateTime(2020, 6, 6, 13, 0, 8, 354, DateTimeKind.Local).AddTicks(259), 24, new DateTime(2020, 6, 6, 13, 0, 8, 354, DateTimeKind.Local).AddTicks(264) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 10,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 14, @"Velit nam tempore quis enim aut tempora.
Non omnis sequi.
Reprehenderit et cumque modi ullam.", new DateTime(2020, 6, 6, 13, 0, 8, 354, DateTimeKind.Local).AddTicks(351), 25, new DateTime(2020, 6, 6, 13, 0, 8, 354, DateTimeKind.Local).AddTicks(356) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 11,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 18, "Voluptas saepe neque consequatur quia aliquid. Saepe ut odio dolore eius. Voluptatem perferendis aperiam provident temporibus sed. Atque ad amet quam perferendis qui quis facere. Eos alias laborum maiores recusandae suscipit dolor non praesentium cum.", new DateTime(2020, 6, 6, 13, 0, 8, 354, DateTimeKind.Local).AddTicks(534), 38, new DateTime(2020, 6, 6, 13, 0, 8, 354, DateTimeKind.Local).AddTicks(539) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 12,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 1, "voluptatem", new DateTime(2020, 6, 6, 13, 0, 8, 354, DateTimeKind.Local).AddTicks(587), 35, new DateTime(2020, 6, 6, 13, 0, 8, 354, DateTimeKind.Local).AddTicks(592) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 13,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 5, "expedita", new DateTime(2020, 6, 6, 13, 0, 8, 354, DateTimeKind.Local).AddTicks(619), 34, new DateTime(2020, 6, 6, 13, 0, 8, 354, DateTimeKind.Local).AddTicks(624) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 14,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 2, "Accusamus omnis et asperiores illo impedit quod autem.", new DateTime(2020, 6, 6, 13, 0, 8, 354, DateTimeKind.Local).AddTicks(702), 37, new DateTime(2020, 6, 6, 13, 0, 8, 354, DateTimeKind.Local).AddTicks(708) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 15,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 10, @"Odio harum ut.
Nobis ut dolorem quasi fugit quia.", new DateTime(2020, 6, 6, 13, 0, 8, 354, DateTimeKind.Local).AddTicks(771), 37, new DateTime(2020, 6, 6, 13, 0, 8, 354, DateTimeKind.Local).AddTicks(776) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 16,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "UpdatedAt" },
                values: new object[] { 14, "Nulla id id unde et dignissimos. Veritatis vitae earum veniam. Rem libero et. Quia sit modi minima eum itaque sit repellat ut.", new DateTime(2020, 6, 6, 13, 0, 8, 354, DateTimeKind.Local).AddTicks(883), new DateTime(2020, 6, 6, 13, 0, 8, 354, DateTimeKind.Local).AddTicks(888) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 17,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 11, "Perspiciatis explicabo et. Hic architecto praesentium ut in. Nemo sunt voluptatem provident in neque aliquid in.", new DateTime(2020, 6, 6, 13, 0, 8, 354, DateTimeKind.Local).AddTicks(1021), 25, new DateTime(2020, 6, 6, 13, 0, 8, 354, DateTimeKind.Local).AddTicks(1025) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 18,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 2, @"Consectetur sit et omnis id in dolor qui.
Fugiat alias in consectetur eum magni.
Temporibus aliquid ex dolorum sunt aut vel repellendus.
Animi aut dicta modi molestias porro in fugit.", new DateTime(2020, 6, 6, 13, 0, 8, 354, DateTimeKind.Local).AddTicks(1179), 28, new DateTime(2020, 6, 6, 13, 0, 8, 354, DateTimeKind.Local).AddTicks(1185) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 19,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 18, "Cupiditate at qui.", new DateTime(2020, 6, 6, 13, 0, 8, 354, DateTimeKind.Local).AddTicks(1224), 25, new DateTime(2020, 6, 6, 13, 0, 8, 354, DateTimeKind.Local).AddTicks(1228) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 20,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 5, "Magni quia suscipit modi. Praesentium nesciunt ratione consequatur neque eius sed quia. Consequatur assumenda sunt animi accusamus sed nihil quis fugiat omnis. Soluta ut et sapiente in maxime ipsa beatae quidem. Reprehenderit rem et ut quae natus et.", new DateTime(2020, 6, 6, 13, 0, 8, 354, DateTimeKind.Local).AddTicks(1414), 27, new DateTime(2020, 6, 6, 13, 0, 8, 354, DateTimeKind.Local).AddTicks(1419) });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 14, new DateTime(2020, 6, 6, 13, 0, 8, 223, DateTimeKind.Local).AddTicks(9719), "Gloria.Fahey@hotmail.com", "p01IEmuRHHkKskT+U3nmCo/PAB39hESjzZpP9dy8ISU=", "ETXlCLHidic9WzcwteG3zuOlvoeEY8/D8qM9G1dvOGI=", new DateTime(2020, 6, 6, 13, 0, 8, 224, DateTimeKind.Local).AddTicks(279), "Harvey.Hodkiewicz" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 17, new DateTime(2020, 6, 6, 13, 0, 8, 230, DateTimeKind.Local).AddTicks(2878), "Stephania74@gmail.com", "7S+ErIgX8maQ2SGK68ytMXTbHVoMqM09xiKXIWzx8S8=", "7/ymxZsIlII39OYMXGgbUIE72ZB1WlPkQYyCVnVr3DE=", new DateTime(2020, 6, 6, 13, 0, 8, 230, DateTimeKind.Local).AddTicks(2901), "Noble12" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 10, new DateTime(2020, 6, 6, 13, 0, 8, 236, DateTimeKind.Local).AddTicks(3860), "Aileen_Berge36@yahoo.com", "KeVByYjQDr9z6MGFTuNIf4AiRUPq1mfzXO7ARWcwaWs=", "guwIpmEI8YpGU2LjysrfW6VQneIU1udtdkLuLb+FbH8=", new DateTime(2020, 6, 6, 13, 0, 8, 236, DateTimeKind.Local).AddTicks(3872), "Rusty_Braun" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 18, new DateTime(2020, 6, 6, 13, 0, 8, 242, DateTimeKind.Local).AddTicks(7653), "Eino3@gmail.com", "Onjoea8kerlXKquz2OnKFqj0pUdplybQXcSTNl2HwMI=", "hSXCx/Nr3Nv7YDW2GZ3OuauJu9aD0mH4Zppi97GVbgg=", new DateTime(2020, 6, 6, 13, 0, 8, 242, DateTimeKind.Local).AddTicks(7666), "Jarred.Ratke" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 1, new DateTime(2020, 6, 6, 13, 0, 8, 248, DateTimeKind.Local).AddTicks(7144), "Nikolas.Bednar@hotmail.com", "ThPgGgMH8oBXm0c88HuqYTqeEB/tF5v+zjYH/2Tl28Y=", "7d8Gpe8zp6Br8fcF4ffb5FutLPTC1qeQ6fqzgNPx828=", new DateTime(2020, 6, 6, 13, 0, 8, 248, DateTimeKind.Local).AddTicks(7155), "Alek75" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 15, new DateTime(2020, 6, 6, 13, 0, 8, 254, DateTimeKind.Local).AddTicks(6897), "Alaina.Kunde@gmail.com", "PofzQH1xFwL3zNOcfD1va6eT8MalEP3BcifrF8v1IAc=", "lzrnWyHNuFi7R23fl4SpIfCVypnPgPo6sDi02m1gvXE=", new DateTime(2020, 6, 6, 13, 0, 8, 254, DateTimeKind.Local).AddTicks(6908), "Kip_Price" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 7,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 8, new DateTime(2020, 6, 6, 13, 0, 8, 261, DateTimeKind.Local).AddTicks(4789), "Patricia.Ratke@yahoo.com", "2INUP/Uo9LY3RpHKF5QQEw4YwMOOEEhiHVdMbdI6nas=", "vpAd9nwM+IUoloGBWfTAQ3SX0AV1mjCxkOH5JrhqvA4=", new DateTime(2020, 6, 6, 13, 0, 8, 261, DateTimeKind.Local).AddTicks(4828), "Orpha_Bins" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 8,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 15, new DateTime(2020, 6, 6, 13, 0, 8, 267, DateTimeKind.Local).AddTicks(9757), "Marty82@gmail.com", "Evqt7iNfUW4s58xKakC2q55EcnXDV4uBwIjrgZUB93g=", "VQy6MZ6sITtPMi/aDOppOqM6AtaVelH7ydnAmGNJhs8=", new DateTime(2020, 6, 6, 13, 0, 8, 267, DateTimeKind.Local).AddTicks(9793), "Misty94" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 9,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 7, new DateTime(2020, 6, 6, 13, 0, 8, 274, DateTimeKind.Local).AddTicks(3401), "Kristy_Goodwin47@hotmail.com", "E1ITyGJMCKtwKKRU/KbVmp4OLfVa6++d/Pa89esaS0E=", "m3Gw4QKpOsrSKu3kFQRlJEWupTdhEr8egye1RNqpiEQ=", new DateTime(2020, 6, 6, 13, 0, 8, 274, DateTimeKind.Local).AddTicks(3426), "Terrell84" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 10,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 3, new DateTime(2020, 6, 6, 13, 0, 8, 280, DateTimeKind.Local).AddTicks(6154), "Elouise5@gmail.com", "WEXGs1wb08GyoZfoUTlQ4oJa4cu2ARWT5zHDPkJGzJ4=", "aV8LRLNAqHAEd3Sur4ff/UmwmftewHbqQ/McuDbtnRI=", new DateTime(2020, 6, 6, 13, 0, 8, 280, DateTimeKind.Local).AddTicks(6180), "Cecile.Hermiston" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 11,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 9, new DateTime(2020, 6, 6, 13, 0, 8, 286, DateTimeKind.Local).AddTicks(5626), "Anabel.Lehner@hotmail.com", "k1AE4gK5ew8n20lJI/oKPOUes3Q70hh66eygW0eEeXQ=", "9H3vpz5Gc7NWqCN73WUEYG1x9W2YqsE11aPc8S9B5+w=", new DateTime(2020, 6, 6, 13, 0, 8, 286, DateTimeKind.Local).AddTicks(5636), "Micah_Hamill63" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 12,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 11, new DateTime(2020, 6, 6, 13, 0, 8, 292, DateTimeKind.Local).AddTicks(7003), "Jesus52@hotmail.com", "+POyvtImnjNYMYGVi6abP82pa1J3WVyMKScQOaku8TM=", "gkyuJ/yavXnk5FI3KRwWB+ul+QBVl3398CnfVBWtCL4=", new DateTime(2020, 6, 6, 13, 0, 8, 292, DateTimeKind.Local).AddTicks(7013), "Brando66" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 13,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 10, new DateTime(2020, 6, 6, 13, 0, 8, 298, DateTimeKind.Local).AddTicks(6376), "Dianna.Hane4@hotmail.com", "2m8CeJI3FaQrzeStgoExjJh77p3tdlWvxSQg92YFjYY=", "AT/KlPpj2xeA0vIc1mc4ly7/kxIOBFUbL2cj3NbppSc=", new DateTime(2020, 6, 6, 13, 0, 8, 298, DateTimeKind.Local).AddTicks(6386), "Cassandra83" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 14,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 18, new DateTime(2020, 6, 6, 13, 0, 8, 304, DateTimeKind.Local).AddTicks(5954), "Curt.Auer7@yahoo.com", "j7t6/mRhaIEw732xYGYC+nJ4/4ifaGBzTkHJwQ7SjKQ=", "wRefIHIYH38TK20A943Ee7TkVYynHRrUjBzd8oCsdI8=", new DateTime(2020, 6, 6, 13, 0, 8, 304, DateTimeKind.Local).AddTicks(5964), "Leonor_Casper53" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 15,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 19, new DateTime(2020, 6, 6, 13, 0, 8, 310, DateTimeKind.Local).AddTicks(8788), "Jeromy.Stracke70@yahoo.com", "epqjjqmwMciDAPjlkY6KCCJxO7ZLpl8TUifDy6ab9E4=", "4CbL1LxkFOi7tkzvRXDMT3B/NDl5OKBhdFd4IC5wWUY=", new DateTime(2020, 6, 6, 13, 0, 8, 310, DateTimeKind.Local).AddTicks(8827), "Elisha26" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 16,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 12, new DateTime(2020, 6, 6, 13, 0, 8, 317, DateTimeKind.Local).AddTicks(403), "Cristopher7@gmail.com", "QNgeRuG45s+9/wNlHYyz3fAJOFKYQEHf1Wslcg6Wr6I=", "M70vsKKE4GVfkaN4Gh4EoNGXaMk2PQVrsSXr25xbokg=", new DateTime(2020, 6, 6, 13, 0, 8, 317, DateTimeKind.Local).AddTicks(414), "Wilfrid75" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 17,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 7, new DateTime(2020, 6, 6, 13, 0, 8, 323, DateTimeKind.Local).AddTicks(322), "Jordyn72@yahoo.com", "l346x32HkFphsGMt5GNNLdt6Jg7YfnxIdaS4ASUZyeE=", "d2PMIFyVEwDjF0y5kFwIrWCStQ/3TI7YMfHNRWwTouo=", new DateTime(2020, 6, 6, 13, 0, 8, 323, DateTimeKind.Local).AddTicks(333), "Thad65" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 18,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 8, new DateTime(2020, 6, 6, 13, 0, 8, 329, DateTimeKind.Local).AddTicks(1574), "Frederick97@yahoo.com", "KdLGvzQesGeA74y2nQp1RaOCdo4WaOC33dya4ZfVxiQ=", "Ka30dD+kklToND3ZUCLEvcr7mvykY9AspvWSKenx/Lw=", new DateTime(2020, 6, 6, 13, 0, 8, 329, DateTimeKind.Local).AddTicks(1584), "Vince40" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 19,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 16, new DateTime(2020, 6, 6, 13, 0, 8, 335, DateTimeKind.Local).AddTicks(759), "King_Senger@hotmail.com", "XFL2WyBXbttF+tig71O2vyI74jS2cX7e5NkM8leeq0s=", "SNkaqYqzn9+vDXa1OIFvfymUoZWEgtFB0PzH7JW6eH4=", new DateTime(2020, 6, 6, 13, 0, 8, 335, DateTimeKind.Local).AddTicks(770), "Yasmine_Jaskolski42" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 20,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 17, new DateTime(2020, 6, 6, 13, 0, 8, 341, DateTimeKind.Local).AddTicks(4437), "Nikko39@yahoo.com", "sGT5ejxc+LIb/ByibE2J023zXaGcuNklUyQLED8OswA=", "s6hcd6gZ/5Vmww4joHPbQSn749yymNMXqgdBXxjZRf0=", new DateTime(2020, 6, 6, 13, 0, 8, 341, DateTimeKind.Local).AddTicks(4446), "Cordie99" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 21,
                columns: new[] { "CreatedAt", "Password", "Salt", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 6, 13, 0, 8, 347, DateTimeKind.Local).AddTicks(6013), "gcDOwt4iZM3kvZOrWbIeZTt34STVb17hY4ya09GE4/U=", "uUxTwtEalV4t3jeQL5RH6eFDOo0p3QGVCA8kiigJ+mU=", new DateTime(2020, 6, 6, 13, 0, 8, 347, DateTimeKind.Local).AddTicks(6013) });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropUniqueConstraint(
                name: "AK_PostReactions_PostId_UserId",
                table: "PostReactions");

            migrationBuilder.DropUniqueConstraint(
                name: "AK_CommentReactions_CommentId_UserId",
                table: "CommentReactions");

            migrationBuilder.DropColumn(
                name: "IsDeleted",
                table: "Comments");

            migrationBuilder.InsertData(
                table: "CommentReactions",
                columns: new[] { "Id", "CommentId", "CreatedAt", "IsLike", "UpdatedAt", "UserId" },
                values: new object[,]
                {
                    { 1, 8, new DateTime(2019, 4, 10, 13, 42, 27, 539, DateTimeKind.Local).AddTicks(9429), true, new DateTime(2019, 4, 10, 13, 42, 27, 540, DateTimeKind.Local).AddTicks(424), 8 },
                    { 19, 4, new DateTime(2019, 4, 10, 13, 42, 27, 540, DateTimeKind.Local).AddTicks(1791), true, new DateTime(2019, 4, 10, 13, 42, 27, 540, DateTimeKind.Local).AddTicks(1796), 15 },
                    { 18, 4, new DateTime(2019, 4, 10, 13, 42, 27, 540, DateTimeKind.Local).AddTicks(1763), false, new DateTime(2019, 4, 10, 13, 42, 27, 540, DateTimeKind.Local).AddTicks(1768), 9 },
                    { 17, 1, new DateTime(2019, 4, 10, 13, 42, 27, 540, DateTimeKind.Local).AddTicks(1735), false, new DateTime(2019, 4, 10, 13, 42, 27, 540, DateTimeKind.Local).AddTicks(1740), 18 },
                    { 16, 7, new DateTime(2019, 4, 10, 13, 42, 27, 540, DateTimeKind.Local).AddTicks(1707), true, new DateTime(2019, 4, 10, 13, 42, 27, 540, DateTimeKind.Local).AddTicks(1711), 18 },
                    { 15, 2, new DateTime(2019, 4, 10, 13, 42, 27, 540, DateTimeKind.Local).AddTicks(1678), false, new DateTime(2019, 4, 10, 13, 42, 27, 540, DateTimeKind.Local).AddTicks(1682), 12 },
                    { 14, 20, new DateTime(2019, 4, 10, 13, 42, 27, 540, DateTimeKind.Local).AddTicks(1646), false, new DateTime(2019, 4, 10, 13, 42, 27, 540, DateTimeKind.Local).AddTicks(1651), 9 },
                    { 13, 1, new DateTime(2019, 4, 10, 13, 42, 27, 540, DateTimeKind.Local).AddTicks(1614), false, new DateTime(2019, 4, 10, 13, 42, 27, 540, DateTimeKind.Local).AddTicks(1620), 10 },
                    { 12, 18, new DateTime(2019, 4, 10, 13, 42, 27, 540, DateTimeKind.Local).AddTicks(1563), false, new DateTime(2019, 4, 10, 13, 42, 27, 540, DateTimeKind.Local).AddTicks(1567), 9 },
                    { 11, 16, new DateTime(2019, 4, 10, 13, 42, 27, 540, DateTimeKind.Local).AddTicks(1534), true, new DateTime(2019, 4, 10, 13, 42, 27, 540, DateTimeKind.Local).AddTicks(1538), 1 },
                    { 20, 2, new DateTime(2019, 4, 10, 13, 42, 27, 540, DateTimeKind.Local).AddTicks(1825), false, new DateTime(2019, 4, 10, 13, 42, 27, 540, DateTimeKind.Local).AddTicks(1830), 17 },
                    { 9, 5, new DateTime(2019, 4, 10, 13, 42, 27, 540, DateTimeKind.Local).AddTicks(1476), true, new DateTime(2019, 4, 10, 13, 42, 27, 540, DateTimeKind.Local).AddTicks(1481), 20 },
                    { 8, 12, new DateTime(2019, 4, 10, 13, 42, 27, 540, DateTimeKind.Local).AddTicks(1443), false, new DateTime(2019, 4, 10, 13, 42, 27, 540, DateTimeKind.Local).AddTicks(1448), 4 },
                    { 7, 5, new DateTime(2019, 4, 10, 13, 42, 27, 540, DateTimeKind.Local).AddTicks(1414), false, new DateTime(2019, 4, 10, 13, 42, 27, 540, DateTimeKind.Local).AddTicks(1418), 16 },
                    { 6, 16, new DateTime(2019, 4, 10, 13, 42, 27, 540, DateTimeKind.Local).AddTicks(1385), false, new DateTime(2019, 4, 10, 13, 42, 27, 540, DateTimeKind.Local).AddTicks(1390), 13 },
                    { 5, 7, new DateTime(2019, 4, 10, 13, 42, 27, 540, DateTimeKind.Local).AddTicks(1356), true, new DateTime(2019, 4, 10, 13, 42, 27, 540, DateTimeKind.Local).AddTicks(1361), 13 },
                    { 4, 17, new DateTime(2019, 4, 10, 13, 42, 27, 540, DateTimeKind.Local).AddTicks(1326), true, new DateTime(2019, 4, 10, 13, 42, 27, 540, DateTimeKind.Local).AddTicks(1331), 16 },
                    { 3, 20, new DateTime(2019, 4, 10, 13, 42, 27, 540, DateTimeKind.Local).AddTicks(1291), true, new DateTime(2019, 4, 10, 13, 42, 27, 540, DateTimeKind.Local).AddTicks(1296), 11 },
                    { 2, 12, new DateTime(2019, 4, 10, 13, 42, 27, 540, DateTimeKind.Local).AddTicks(1237), false, new DateTime(2019, 4, 10, 13, 42, 27, 540, DateTimeKind.Local).AddTicks(1250), 1 },
                    { 10, 7, new DateTime(2019, 4, 10, 13, 42, 27, 540, DateTimeKind.Local).AddTicks(1505), true, new DateTime(2019, 4, 10, 13, 42, 27, 540, DateTimeKind.Local).AddTicks(1510), 15 }
                });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 10, "Odio earum expedita eos magnam voluptas quia aut ipsum.", new DateTime(2019, 4, 10, 13, 42, 27, 524, DateTimeKind.Local).AddTicks(1617), 3, new DateTime(2019, 4, 10, 13, 42, 27, 524, DateTimeKind.Local).AddTicks(2611) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 17, "Illum et neque facilis.", new DateTime(2019, 4, 10, 13, 42, 27, 524, DateTimeKind.Local).AddTicks(3483), 14, new DateTime(2019, 4, 10, 13, 42, 27, 524, DateTimeKind.Local).AddTicks(3498) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 8, "Ullam magnam quis nemo eum dolor quaerat eum.", new DateTime(2019, 4, 10, 13, 42, 27, 524, DateTimeKind.Local).AddTicks(3603), 10, new DateTime(2019, 4, 10, 13, 42, 27, 524, DateTimeKind.Local).AddTicks(3610) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 16, "Fugiat delectus aspernatur soluta sit sed provident nostrum et.", new DateTime(2019, 4, 10, 13, 42, 27, 524, DateTimeKind.Local).AddTicks(3691), 19, new DateTime(2019, 4, 10, 13, 42, 27, 524, DateTimeKind.Local).AddTicks(3698) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 6, "Modi in officiis itaque et et quas.", new DateTime(2019, 4, 10, 13, 42, 27, 524, DateTimeKind.Local).AddTicks(3759), 8, new DateTime(2019, 4, 10, 13, 42, 27, 524, DateTimeKind.Local).AddTicks(3766) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { "Reiciendis quia perspiciatis et quidem.", new DateTime(2019, 4, 10, 13, 42, 27, 524, DateTimeKind.Local).AddTicks(3830), 8, new DateTime(2019, 4, 10, 13, 42, 27, 524, DateTimeKind.Local).AddTicks(3836) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 7,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 3, "Et accusamus ea voluptatem harum dolorem eaque eos ducimus fuga.", new DateTime(2019, 4, 10, 13, 42, 27, 524, DateTimeKind.Local).AddTicks(3906), 10, new DateTime(2019, 4, 10, 13, 42, 27, 524, DateTimeKind.Local).AddTicks(3913) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 8,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 16, "Rerum iure libero voluptatem.", new DateTime(2019, 4, 10, 13, 42, 27, 524, DateTimeKind.Local).AddTicks(3983), 11, new DateTime(2019, 4, 10, 13, 42, 27, 524, DateTimeKind.Local).AddTicks(3990) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 9,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 3, "Possimus dolor fugiat modi voluptatum rem doloribus necessitatibus dolores.", new DateTime(2019, 4, 10, 13, 42, 27, 524, DateTimeKind.Local).AddTicks(4056), 19, new DateTime(2019, 4, 10, 13, 42, 27, 524, DateTimeKind.Local).AddTicks(4063) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 10,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 19, "Recusandae molestias eius ipsa sit.", new DateTime(2019, 4, 10, 13, 42, 27, 524, DateTimeKind.Local).AddTicks(4120), 19, new DateTime(2019, 4, 10, 13, 42, 27, 524, DateTimeKind.Local).AddTicks(4126) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 11,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 18, "Ut commodi sed aut ratione saepe sed doloremque.", new DateTime(2019, 4, 10, 13, 42, 27, 524, DateTimeKind.Local).AddTicks(4188), 18, new DateTime(2019, 4, 10, 13, 42, 27, 524, DateTimeKind.Local).AddTicks(4194) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 12,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 4, "Et assumenda sint labore voluptatem eaque dolor nulla neque quia.", new DateTime(2019, 4, 10, 13, 42, 27, 524, DateTimeKind.Local).AddTicks(4281), 1, new DateTime(2019, 4, 10, 13, 42, 27, 524, DateTimeKind.Local).AddTicks(4289) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 13,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 8, "Quisquam vero unde et qui.", new DateTime(2019, 4, 10, 13, 42, 27, 524, DateTimeKind.Local).AddTicks(4350), 10, new DateTime(2019, 4, 10, 13, 42, 27, 524, DateTimeKind.Local).AddTicks(4356) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 14,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 21, "Repellendus quibusdam earum qui.", new DateTime(2019, 4, 10, 13, 42, 27, 524, DateTimeKind.Local).AddTicks(4420), 20, new DateTime(2019, 4, 10, 13, 42, 27, 524, DateTimeKind.Local).AddTicks(4427) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 15,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 2, "Culpa enim a.", new DateTime(2019, 4, 10, 13, 42, 27, 524, DateTimeKind.Local).AddTicks(4479), 6, new DateTime(2019, 4, 10, 13, 42, 27, 524, DateTimeKind.Local).AddTicks(4486) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 16,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 19, "Asperiores corrupti quam enim.", new DateTime(2019, 4, 10, 13, 42, 27, 524, DateTimeKind.Local).AddTicks(4538), 1, new DateTime(2019, 4, 10, 13, 42, 27, 524, DateTimeKind.Local).AddTicks(4544) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 17,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 16, "Inventore dolor provident.", new DateTime(2019, 4, 10, 13, 42, 27, 524, DateTimeKind.Local).AddTicks(4593), 8, new DateTime(2019, 4, 10, 13, 42, 27, 524, DateTimeKind.Local).AddTicks(4599) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 18,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 13, "Nesciunt nostrum ut unde corporis rem reprehenderit libero quia ut.", new DateTime(2019, 4, 10, 13, 42, 27, 524, DateTimeKind.Local).AddTicks(4665), 17, new DateTime(2019, 4, 10, 13, 42, 27, 524, DateTimeKind.Local).AddTicks(4672) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 19,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 2, "Sunt odit voluptatem saepe eius.", new DateTime(2019, 4, 10, 13, 42, 27, 524, DateTimeKind.Local).AddTicks(4725), 15, new DateTime(2019, 4, 10, 13, 42, 27, 524, DateTimeKind.Local).AddTicks(4732) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 20,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 18, "Est consequatur consequuntur eum.", new DateTime(2019, 4, 10, 13, 42, 27, 524, DateTimeKind.Local).AddTicks(4880), 7, new DateTime(2019, 4, 10, 13, 42, 27, 524, DateTimeKind.Local).AddTicks(4888) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 4, 10, 13, 42, 27, 288, DateTimeKind.Local).AddTicks(1098), "https://s3.amazonaws.com/uifaces/faces/twitter/belyaev_rs/128.jpg", new DateTime(2019, 4, 10, 13, 42, 27, 288, DateTimeKind.Local).AddTicks(9343) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 4, 10, 13, 42, 27, 289, DateTimeKind.Local).AddTicks(576), "https://s3.amazonaws.com/uifaces/faces/twitter/bpartridge/128.jpg", new DateTime(2019, 4, 10, 13, 42, 27, 289, DateTimeKind.Local).AddTicks(590) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 4, 10, 13, 42, 27, 289, DateTimeKind.Local).AddTicks(630), "https://s3.amazonaws.com/uifaces/faces/twitter/martip07/128.jpg", new DateTime(2019, 4, 10, 13, 42, 27, 289, DateTimeKind.Local).AddTicks(636) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 4, 10, 13, 42, 27, 289, DateTimeKind.Local).AddTicks(668), "https://s3.amazonaws.com/uifaces/faces/twitter/ricburton/128.jpg", new DateTime(2019, 4, 10, 13, 42, 27, 289, DateTimeKind.Local).AddTicks(674) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 4, 10, 13, 42, 27, 289, DateTimeKind.Local).AddTicks(697), "https://s3.amazonaws.com/uifaces/faces/twitter/sreejithexp/128.jpg", new DateTime(2019, 4, 10, 13, 42, 27, 289, DateTimeKind.Local).AddTicks(702) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 4, 10, 13, 42, 27, 289, DateTimeKind.Local).AddTicks(725), "https://s3.amazonaws.com/uifaces/faces/twitter/longlivemyword/128.jpg", new DateTime(2019, 4, 10, 13, 42, 27, 289, DateTimeKind.Local).AddTicks(730) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 7,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 4, 10, 13, 42, 27, 289, DateTimeKind.Local).AddTicks(754), "https://s3.amazonaws.com/uifaces/faces/twitter/ajaxy_ru/128.jpg", new DateTime(2019, 4, 10, 13, 42, 27, 289, DateTimeKind.Local).AddTicks(759) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 8,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 4, 10, 13, 42, 27, 289, DateTimeKind.Local).AddTicks(782), "https://s3.amazonaws.com/uifaces/faces/twitter/kalmerrautam/128.jpg", new DateTime(2019, 4, 10, 13, 42, 27, 289, DateTimeKind.Local).AddTicks(787) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 9,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 4, 10, 13, 42, 27, 289, DateTimeKind.Local).AddTicks(817), "https://s3.amazonaws.com/uifaces/faces/twitter/gonzalorobaina/128.jpg", new DateTime(2019, 4, 10, 13, 42, 27, 289, DateTimeKind.Local).AddTicks(822) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 10,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 4, 10, 13, 42, 27, 289, DateTimeKind.Local).AddTicks(844), "https://s3.amazonaws.com/uifaces/faces/twitter/vitor376/128.jpg", new DateTime(2019, 4, 10, 13, 42, 27, 289, DateTimeKind.Local).AddTicks(850) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 11,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 4, 10, 13, 42, 27, 289, DateTimeKind.Local).AddTicks(872), "https://s3.amazonaws.com/uifaces/faces/twitter/baumannzone/128.jpg", new DateTime(2019, 4, 10, 13, 42, 27, 289, DateTimeKind.Local).AddTicks(877) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 12,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 4, 10, 13, 42, 27, 289, DateTimeKind.Local).AddTicks(900), "https://s3.amazonaws.com/uifaces/faces/twitter/andreas_pr/128.jpg", new DateTime(2019, 4, 10, 13, 42, 27, 289, DateTimeKind.Local).AddTicks(905) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 13,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 4, 10, 13, 42, 27, 289, DateTimeKind.Local).AddTicks(928), "https://s3.amazonaws.com/uifaces/faces/twitter/rtgibbons/128.jpg", new DateTime(2019, 4, 10, 13, 42, 27, 289, DateTimeKind.Local).AddTicks(933) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 14,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 4, 10, 13, 42, 27, 289, DateTimeKind.Local).AddTicks(963), "https://s3.amazonaws.com/uifaces/faces/twitter/timothycd/128.jpg", new DateTime(2019, 4, 10, 13, 42, 27, 289, DateTimeKind.Local).AddTicks(968) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 15,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 4, 10, 13, 42, 27, 289, DateTimeKind.Local).AddTicks(991), "https://s3.amazonaws.com/uifaces/faces/twitter/adobi/128.jpg", new DateTime(2019, 4, 10, 13, 42, 27, 289, DateTimeKind.Local).AddTicks(996) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 16,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 4, 10, 13, 42, 27, 289, DateTimeKind.Local).AddTicks(1231), "https://s3.amazonaws.com/uifaces/faces/twitter/moynihan/128.jpg", new DateTime(2019, 4, 10, 13, 42, 27, 289, DateTimeKind.Local).AddTicks(1240) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 17,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 4, 10, 13, 42, 27, 289, DateTimeKind.Local).AddTicks(1282), "https://s3.amazonaws.com/uifaces/faces/twitter/weavermedia/128.jpg", new DateTime(2019, 4, 10, 13, 42, 27, 289, DateTimeKind.Local).AddTicks(1288) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 18,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 4, 10, 13, 42, 27, 289, DateTimeKind.Local).AddTicks(1312), "https://s3.amazonaws.com/uifaces/faces/twitter/nvkznemo/128.jpg", new DateTime(2019, 4, 10, 13, 42, 27, 289, DateTimeKind.Local).AddTicks(1317) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 19,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 4, 10, 13, 42, 27, 289, DateTimeKind.Local).AddTicks(1340), "https://s3.amazonaws.com/uifaces/faces/twitter/exentrich/128.jpg", new DateTime(2019, 4, 10, 13, 42, 27, 289, DateTimeKind.Local).AddTicks(1345) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 20,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 4, 10, 13, 42, 27, 289, DateTimeKind.Local).AddTicks(1369), "https://s3.amazonaws.com/uifaces/faces/twitter/cbracco/128.jpg", new DateTime(2019, 4, 10, 13, 42, 27, 289, DateTimeKind.Local).AddTicks(1374) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 21,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 4, 10, 13, 42, 27, 295, DateTimeKind.Local).AddTicks(906), "https://picsum.photos/640/480/?image=392", new DateTime(2019, 4, 10, 13, 42, 27, 295, DateTimeKind.Local).AddTicks(1980) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 22,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 4, 10, 13, 42, 27, 295, DateTimeKind.Local).AddTicks(2289), "https://picsum.photos/640/480/?image=425", new DateTime(2019, 4, 10, 13, 42, 27, 295, DateTimeKind.Local).AddTicks(2301) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 23,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 4, 10, 13, 42, 27, 295, DateTimeKind.Local).AddTicks(2335), "https://picsum.photos/640/480/?image=784", new DateTime(2019, 4, 10, 13, 42, 27, 295, DateTimeKind.Local).AddTicks(2340) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 24,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 4, 10, 13, 42, 27, 295, DateTimeKind.Local).AddTicks(2365), "https://picsum.photos/640/480/?image=758", new DateTime(2019, 4, 10, 13, 42, 27, 295, DateTimeKind.Local).AddTicks(2370) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 25,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 4, 10, 13, 42, 27, 295, DateTimeKind.Local).AddTicks(2392), "https://picsum.photos/640/480/?image=726", new DateTime(2019, 4, 10, 13, 42, 27, 295, DateTimeKind.Local).AddTicks(2397) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 26,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 4, 10, 13, 42, 27, 295, DateTimeKind.Local).AddTicks(2419), "https://picsum.photos/640/480/?image=866", new DateTime(2019, 4, 10, 13, 42, 27, 295, DateTimeKind.Local).AddTicks(2423) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 27,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 4, 10, 13, 42, 27, 295, DateTimeKind.Local).AddTicks(2445), "https://picsum.photos/640/480/?image=557", new DateTime(2019, 4, 10, 13, 42, 27, 295, DateTimeKind.Local).AddTicks(2450) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 28,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 4, 10, 13, 42, 27, 295, DateTimeKind.Local).AddTicks(2471), "https://picsum.photos/640/480/?image=263", new DateTime(2019, 4, 10, 13, 42, 27, 295, DateTimeKind.Local).AddTicks(2475) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 29,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 4, 10, 13, 42, 27, 295, DateTimeKind.Local).AddTicks(2498), "https://picsum.photos/640/480/?image=60", new DateTime(2019, 4, 10, 13, 42, 27, 295, DateTimeKind.Local).AddTicks(2503) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 30,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 4, 10, 13, 42, 27, 295, DateTimeKind.Local).AddTicks(2593), "https://picsum.photos/640/480/?image=222", new DateTime(2019, 4, 10, 13, 42, 27, 295, DateTimeKind.Local).AddTicks(2599) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 31,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 4, 10, 13, 42, 27, 295, DateTimeKind.Local).AddTicks(2625), "https://picsum.photos/640/480/?image=201", new DateTime(2019, 4, 10, 13, 42, 27, 295, DateTimeKind.Local).AddTicks(2630) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 32,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 4, 10, 13, 42, 27, 295, DateTimeKind.Local).AddTicks(2651), "https://picsum.photos/640/480/?image=217", new DateTime(2019, 4, 10, 13, 42, 27, 295, DateTimeKind.Local).AddTicks(2656) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 33,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 4, 10, 13, 42, 27, 295, DateTimeKind.Local).AddTicks(2678), "https://picsum.photos/640/480/?image=730", new DateTime(2019, 4, 10, 13, 42, 27, 295, DateTimeKind.Local).AddTicks(2682) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 34,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 4, 10, 13, 42, 27, 295, DateTimeKind.Local).AddTicks(2705), "https://picsum.photos/640/480/?image=1061", new DateTime(2019, 4, 10, 13, 42, 27, 295, DateTimeKind.Local).AddTicks(2709) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 35,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 4, 10, 13, 42, 27, 295, DateTimeKind.Local).AddTicks(2731), "https://picsum.photos/640/480/?image=52", new DateTime(2019, 4, 10, 13, 42, 27, 295, DateTimeKind.Local).AddTicks(2736) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 36,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 4, 10, 13, 42, 27, 295, DateTimeKind.Local).AddTicks(2758), "https://picsum.photos/640/480/?image=467", new DateTime(2019, 4, 10, 13, 42, 27, 295, DateTimeKind.Local).AddTicks(2762) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 37,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 4, 10, 13, 42, 27, 295, DateTimeKind.Local).AddTicks(2784), "https://picsum.photos/640/480/?image=385", new DateTime(2019, 4, 10, 13, 42, 27, 295, DateTimeKind.Local).AddTicks(2788) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 38,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 4, 10, 13, 42, 27, 295, DateTimeKind.Local).AddTicks(2810), "https://picsum.photos/640/480/?image=19", new DateTime(2019, 4, 10, 13, 42, 27, 295, DateTimeKind.Local).AddTicks(2814) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 39,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 4, 10, 13, 42, 27, 295, DateTimeKind.Local).AddTicks(2836), "https://picsum.photos/640/480/?image=341", new DateTime(2019, 4, 10, 13, 42, 27, 295, DateTimeKind.Local).AddTicks(2840) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 40,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 4, 10, 13, 42, 27, 295, DateTimeKind.Local).AddTicks(2862), "https://picsum.photos/640/480/?image=685", new DateTime(2019, 4, 10, 13, 42, 27, 295, DateTimeKind.Local).AddTicks(2866) });

            migrationBuilder.InsertData(
                table: "PostReactions",
                columns: new[] { "Id", "CreatedAt", "IsLike", "PostId", "UpdatedAt", "UserId" },
                values: new object[,]
                {
                    { 11, new DateTime(2019, 4, 10, 13, 42, 27, 533, DateTimeKind.Local).AddTicks(4773), true, 10, new DateTime(2019, 4, 10, 13, 42, 27, 533, DateTimeKind.Local).AddTicks(4778), 4 },
                    { 10, new DateTime(2019, 4, 10, 13, 42, 27, 533, DateTimeKind.Local).AddTicks(4744), false, 19, new DateTime(2019, 4, 10, 13, 42, 27, 533, DateTimeKind.Local).AddTicks(4749), 10 },
                    { 9, new DateTime(2019, 4, 10, 13, 42, 27, 533, DateTimeKind.Local).AddTicks(4715), true, 13, new DateTime(2019, 4, 10, 13, 42, 27, 533, DateTimeKind.Local).AddTicks(4720), 15 },
                    { 8, new DateTime(2019, 4, 10, 13, 42, 27, 533, DateTimeKind.Local).AddTicks(4685), false, 18, new DateTime(2019, 4, 10, 13, 42, 27, 533, DateTimeKind.Local).AddTicks(4690), 17 },
                    { 6, new DateTime(2019, 4, 10, 13, 42, 27, 533, DateTimeKind.Local).AddTicks(4624), false, 17, new DateTime(2019, 4, 10, 13, 42, 27, 533, DateTimeKind.Local).AddTicks(4628), 11 },
                    { 3, new DateTime(2019, 4, 10, 13, 42, 27, 533, DateTimeKind.Local).AddTicks(4526), true, 16, new DateTime(2019, 4, 10, 13, 42, 27, 533, DateTimeKind.Local).AddTicks(4531), 17 },
                    { 5, new DateTime(2019, 4, 10, 13, 42, 27, 533, DateTimeKind.Local).AddTicks(4592), false, 11, new DateTime(2019, 4, 10, 13, 42, 27, 533, DateTimeKind.Local).AddTicks(4597), 19 },
                    { 4, new DateTime(2019, 4, 10, 13, 42, 27, 533, DateTimeKind.Local).AddTicks(4560), true, 14, new DateTime(2019, 4, 10, 13, 42, 27, 533, DateTimeKind.Local).AddTicks(4565), 16 },
                    { 12, new DateTime(2019, 4, 10, 13, 42, 27, 533, DateTimeKind.Local).AddTicks(4812), true, 10, new DateTime(2019, 4, 10, 13, 42, 27, 533, DateTimeKind.Local).AddTicks(4818), 20 },
                    { 1, new DateTime(2019, 4, 10, 13, 42, 27, 533, DateTimeKind.Local).AddTicks(2442), true, 1, new DateTime(2019, 4, 10, 13, 42, 27, 533, DateTimeKind.Local).AddTicks(3481), 4 },
                    { 2, new DateTime(2019, 4, 10, 13, 42, 27, 533, DateTimeKind.Local).AddTicks(4462), true, 1, new DateTime(2019, 4, 10, 13, 42, 27, 533, DateTimeKind.Local).AddTicks(4478), 4 },
                    { 7, new DateTime(2019, 4, 10, 13, 42, 27, 533, DateTimeKind.Local).AddTicks(4655), true, 19, new DateTime(2019, 4, 10, 13, 42, 27, 533, DateTimeKind.Local).AddTicks(4660), 12 },
                    { 13, new DateTime(2019, 4, 10, 13, 42, 27, 533, DateTimeKind.Local).AddTicks(4843), true, 8, new DateTime(2019, 4, 10, 13, 42, 27, 533, DateTimeKind.Local).AddTicks(4848), 15 },
                    { 18, new DateTime(2019, 4, 10, 13, 42, 27, 533, DateTimeKind.Local).AddTicks(4991), true, 6, new DateTime(2019, 4, 10, 13, 42, 27, 533, DateTimeKind.Local).AddTicks(4996), 21 },
                    { 15, new DateTime(2019, 4, 10, 13, 42, 27, 533, DateTimeKind.Local).AddTicks(4904), true, 4, new DateTime(2019, 4, 10, 13, 42, 27, 533, DateTimeKind.Local).AddTicks(4909), 1 },
                    { 16, new DateTime(2019, 4, 10, 13, 42, 27, 533, DateTimeKind.Local).AddTicks(4933), false, 18, new DateTime(2019, 4, 10, 13, 42, 27, 533, DateTimeKind.Local).AddTicks(4938), 10 },
                    { 20, new DateTime(2019, 4, 10, 13, 42, 27, 533, DateTimeKind.Local).AddTicks(5049), true, 11, new DateTime(2019, 4, 10, 13, 42, 27, 533, DateTimeKind.Local).AddTicks(5054), 20 },
                    { 17, new DateTime(2019, 4, 10, 13, 42, 27, 533, DateTimeKind.Local).AddTicks(4962), false, 17, new DateTime(2019, 4, 10, 13, 42, 27, 533, DateTimeKind.Local).AddTicks(4967), 18 },
                    { 19, new DateTime(2019, 4, 10, 13, 42, 27, 533, DateTimeKind.Local).AddTicks(5021), true, 8, new DateTime(2019, 4, 10, 13, 42, 27, 533, DateTimeKind.Local).AddTicks(5026), 13 },
                    { 14, new DateTime(2019, 4, 10, 13, 42, 27, 533, DateTimeKind.Local).AddTicks(4873), false, 6, new DateTime(2019, 4, 10, 13, 42, 27, 533, DateTimeKind.Local).AddTicks(4878), 6 }
                });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 14, "molestiae", new DateTime(2019, 4, 10, 13, 42, 27, 514, DateTimeKind.Local).AddTicks(4538), 29, new DateTime(2019, 4, 10, 13, 42, 27, 514, DateTimeKind.Local).AddTicks(5593) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { @"Nisi quia sunt sapiente sapiente quia maiores iure.
Non rerum pariatur rem id autem.
Accusantium iure odio et possimus.
Rerum illum ut unde sed qui aperiam corrupti.
Et enim repellendus dolor inventore inventore unde cupiditate tempore.
Aut possimus temporibus pariatur at voluptas dolor minima.", new DateTime(2019, 4, 10, 13, 42, 27, 516, DateTimeKind.Local).AddTicks(5149), 32, new DateTime(2019, 4, 10, 13, 42, 27, 516, DateTimeKind.Local).AddTicks(5171) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 9, "Sapiente itaque fuga sed ad.", new DateTime(2019, 4, 10, 13, 42, 27, 516, DateTimeKind.Local).AddTicks(7346), 40, new DateTime(2019, 4, 10, 13, 42, 27, 516, DateTimeKind.Local).AddTicks(7361) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 20, "sed", new DateTime(2019, 4, 10, 13, 42, 27, 516, DateTimeKind.Local).AddTicks(7457), 28, new DateTime(2019, 4, 10, 13, 42, 27, 516, DateTimeKind.Local).AddTicks(7464) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 17, @"Est nihil id corporis voluptatum eos voluptas.
Tenetur delectus pariatur ut.", new DateTime(2019, 4, 10, 13, 42, 27, 516, DateTimeKind.Local).AddTicks(7654), 23, new DateTime(2019, 4, 10, 13, 42, 27, 516, DateTimeKind.Local).AddTicks(7663) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 18, "Rem aspernatur excepturi et.", new DateTime(2019, 4, 10, 13, 42, 27, 516, DateTimeKind.Local).AddTicks(7754), 27, new DateTime(2019, 4, 10, 13, 42, 27, 516, DateTimeKind.Local).AddTicks(7761) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 7,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 19, @"Iste aut et qui qui consequatur ipsa aut minima.
Ea perspiciatis aliquam.
Voluptate occaecati id voluptatibus expedita nobis nemo officiis harum quisquam.
Aut pariatur ut molestiae ut.", new DateTime(2019, 4, 10, 13, 42, 27, 516, DateTimeKind.Local).AddTicks(7926), 37, new DateTime(2019, 4, 10, 13, 42, 27, 516, DateTimeKind.Local).AddTicks(7933) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 8,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 16, "sint", new DateTime(2019, 4, 10, 13, 42, 27, 516, DateTimeKind.Local).AddTicks(7985), 39, new DateTime(2019, 4, 10, 13, 42, 27, 516, DateTimeKind.Local).AddTicks(7992) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 9,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 4, "vitae", new DateTime(2019, 4, 10, 13, 42, 27, 516, DateTimeKind.Local).AddTicks(8036), 31, new DateTime(2019, 4, 10, 13, 42, 27, 516, DateTimeKind.Local).AddTicks(8042) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 10,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 21, "Cumque reiciendis et numquam sit aut explicabo sunt et. Quo deleniti nobis voluptatum eveniet dolor sunt a aut vel. Labore quos voluptas.", new DateTime(2019, 4, 10, 13, 42, 27, 517, DateTimeKind.Local).AddTicks(1677), 35, new DateTime(2019, 4, 10, 13, 42, 27, 517, DateTimeKind.Local).AddTicks(1693) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 11,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 1, @"Voluptatum dolores officiis qui aperiam recusandae quod minus.
Libero dolores sit eligendi reiciendis delectus vel eaque.
Ad aliquam corporis recusandae.", new DateTime(2019, 4, 10, 13, 42, 27, 517, DateTimeKind.Local).AddTicks(1890), 25, new DateTime(2019, 4, 10, 13, 42, 27, 517, DateTimeKind.Local).AddTicks(1899) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 12,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 8, "Corrupti corporis voluptatum sit dignissimos est. Iure ea sapiente laboriosam et perspiciatis nesciunt quae et quis. Quae voluptatem voluptatem non hic cumque voluptas vel. Ut perspiciatis exercitationem. Ex quia totam magnam et amet corrupti est est. Officia natus quam optio culpa minus aspernatur quaerat accusantium.", new DateTime(2019, 4, 10, 13, 42, 27, 517, DateTimeKind.Local).AddTicks(2146), 39, new DateTime(2019, 4, 10, 13, 42, 27, 517, DateTimeKind.Local).AddTicks(2154) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 13,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 21, @"Sapiente ex omnis quam.
Id sunt voluptas.
Explicabo voluptatibus dignissimos.
Et laborum quisquam.", new DateTime(2019, 4, 10, 13, 42, 27, 517, DateTimeKind.Local).AddTicks(2261), 37, new DateTime(2019, 4, 10, 13, 42, 27, 517, DateTimeKind.Local).AddTicks(2268) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 14,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 11, "Tempore delectus non consequatur sit est voluptatem.", new DateTime(2019, 4, 10, 13, 42, 27, 517, DateTimeKind.Local).AddTicks(2455), 22, new DateTime(2019, 4, 10, 13, 42, 27, 517, DateTimeKind.Local).AddTicks(2464) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 15,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 1, "Dolorum optio ut nostrum beatae velit et.", new DateTime(2019, 4, 10, 13, 42, 27, 517, DateTimeKind.Local).AddTicks(2541), 33, new DateTime(2019, 4, 10, 13, 42, 27, 517, DateTimeKind.Local).AddTicks(2547) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 16,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "UpdatedAt" },
                values: new object[] { 7, "maxime", new DateTime(2019, 4, 10, 13, 42, 27, 517, DateTimeKind.Local).AddTicks(2591), new DateTime(2019, 4, 10, 13, 42, 27, 517, DateTimeKind.Local).AddTicks(2597) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 17,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 17, @"Consectetur est nemo fugit ea exercitationem a non.
Dolorem assumenda ut quam qui corrupti voluptas qui error commodi.", new DateTime(2019, 4, 10, 13, 42, 27, 517, DateTimeKind.Local).AddTicks(2709), 35, new DateTime(2019, 4, 10, 13, 42, 27, 517, DateTimeKind.Local).AddTicks(2716) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 18,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 16, @"Minus ex odio aliquid molestiae.
Sed nostrum quae doloribus eum odio quia.
Ipsum quia quod id exercitationem.
Rerum fugiat quia esse impedit.", new DateTime(2019, 4, 10, 13, 42, 27, 517, DateTimeKind.Local).AddTicks(2846), 29, new DateTime(2019, 4, 10, 13, 42, 27, 517, DateTimeKind.Local).AddTicks(2853) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 19,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 19, "Pariatur in aut alias eum alias. Est id voluptatum fuga deleniti amet et facere quae. Totam reprehenderit ipsa. Quasi voluptatem veniam.", new DateTime(2019, 4, 10, 13, 42, 27, 517, DateTimeKind.Local).AddTicks(2984), 34, new DateTime(2019, 4, 10, 13, 42, 27, 517, DateTimeKind.Local).AddTicks(2991) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 20,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 11, "voluptatem", new DateTime(2019, 4, 10, 13, 42, 27, 517, DateTimeKind.Local).AddTicks(3034), 33, new DateTime(2019, 4, 10, 13, 42, 27, 517, DateTimeKind.Local).AddTicks(3041) });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 4, new DateTime(2019, 4, 10, 13, 42, 27, 354, DateTimeKind.Local).AddTicks(9279), "Eleanore78@hotmail.com", "JwhgJRWNi9KZ6HvsVC4o3ATXPKrIl6NpFs14Ywn6XcQ=", "1ZKXAVI9IEeT90K/cc4u5/9FM9h5NSvHrOT3p8VVgXk=", new DateTime(2019, 4, 10, 13, 42, 27, 355, DateTimeKind.Local).AddTicks(557), "Reanna.Berge" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 15, new DateTime(2019, 4, 10, 13, 42, 27, 362, DateTimeKind.Local).AddTicks(6332), "Noel37@gmail.com", "Ov/w2Ir6JjrZM+lrZWDJZQdRTyrjeX3kkEV1sNq5DLM=", "81E5KuSfD6Wy6OFVRVrzJi2uxOmPDeOHy31ZANgLikw=", new DateTime(2019, 4, 10, 13, 42, 27, 362, DateTimeKind.Local).AddTicks(6356), "Trevion.Konopelski" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 18, new DateTime(2019, 4, 10, 13, 42, 27, 370, DateTimeKind.Local).AddTicks(1374), "Tyree.Hettinger5@hotmail.com", "IFV9xQIA/JkXs2hSSZbxmkQc94MfDE6An6s8rISbMYo=", "lt+NT9Cw+2mitIPEmnHcp4xH3eoNtuwBukz7l2yl2Rg=", new DateTime(2019, 4, 10, 13, 42, 27, 370, DateTimeKind.Local).AddTicks(1394), "Raoul_Armstrong" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 9, new DateTime(2019, 4, 10, 13, 42, 27, 377, DateTimeKind.Local).AddTicks(6273), "Landen82@gmail.com", "pel2LSd08C+t2mEhX1uBO7rVCWTaKiAjkqj43O1ytxo=", "W/OnJ8Ygem16yrXzV3ijvpYdye+/hgN2MeIiz/j9spM=", new DateTime(2019, 4, 10, 13, 42, 27, 377, DateTimeKind.Local).AddTicks(6291), "Tamara_Heaney59" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 13, new DateTime(2019, 4, 10, 13, 42, 27, 385, DateTimeKind.Local).AddTicks(1240), "Ashlee_Walker@gmail.com", "Ty1WmblkkeeehqZtK993rkptymgJ4HHmP3FtlQwggTQ=", "xmkPKualMGfdZNrMieig0XspCqfUdXIsiM9n+hH0BNM=", new DateTime(2019, 4, 10, 13, 42, 27, 385, DateTimeKind.Local).AddTicks(1258), "April.Baumbach63" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 5, new DateTime(2019, 4, 10, 13, 42, 27, 392, DateTimeKind.Local).AddTicks(4481), "Vern_Yost17@hotmail.com", "UrtG6PTWOuZN01Y0wJW2RXzWQztXetNZKLMXY2kPapU=", "nI8AsOHQ9rdpquComXIuzdUI83EJA5Ebtk53tHp8okM=", new DateTime(2019, 4, 10, 13, 42, 27, 392, DateTimeKind.Local).AddTicks(4494), "Jessy_Trantow70" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 7,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 11, new DateTime(2019, 4, 10, 13, 42, 27, 400, DateTimeKind.Local).AddTicks(1588), "Octavia.Breitenberg15@gmail.com", "JEPtNmc+xcRh6dRMz0XpW3YjPfo70dx6ghqpRbR7qZ8=", "QkAvLRdlO782y7r9/FQ5BeOVYPCV2OixVUgbJ0x0kZQ=", new DateTime(2019, 4, 10, 13, 42, 27, 400, DateTimeKind.Local).AddTicks(1618), "Brionna_Huel" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 8,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 20, new DateTime(2019, 4, 10, 13, 42, 27, 407, DateTimeKind.Local).AddTicks(5516), "Jaida.Robel@yahoo.com", "2sM5eXjzXM4rit4n0XznRgPXWrMUrkM2NEYIt8wWdM0=", "uZISArgxWHVs5sNqC3KY/zSN2GC6+cQ4PbSZXd2tY/w=", new DateTime(2019, 4, 10, 13, 42, 27, 407, DateTimeKind.Local).AddTicks(5528), "Idella67" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 9,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 4, new DateTime(2019, 4, 10, 13, 42, 27, 415, DateTimeKind.Local).AddTicks(286), "Merle3@hotmail.com", "vPqcCQvAy3m0ApPP9wTVPS+S6M+kffrzhI0KKY5byUE=", "73Y6XFrs5GNfbX3gEPnUriXG+09YbIzyLCyKeTM4d/4=", new DateTime(2019, 4, 10, 13, 42, 27, 415, DateTimeKind.Local).AddTicks(305), "Francesca_McClure" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 10,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 8, new DateTime(2019, 4, 10, 13, 42, 27, 422, DateTimeKind.Local).AddTicks(5128), "Destin87@gmail.com", "3OzL76X0wfbkPwt7JQPH+/ebvc9xfBO3bcFQunQ0v4c=", "E4foLxFiXVPkgaoQ+Tr6Ge0g6OSnwWDa3eNX3Y0ExT4=", new DateTime(2019, 4, 10, 13, 42, 27, 422, DateTimeKind.Local).AddTicks(5147), "Ben.Legros80" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 11,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 14, new DateTime(2019, 4, 10, 13, 42, 27, 430, DateTimeKind.Local).AddTicks(2390), "Finn84@gmail.com", "izsFaOQN3vHcHSvx8GGNmww7cWdIFOMBLISdizTYcw8=", "QwJmN2g2pIq3vU+epDXifJpLcfBOM84mE8O97MdJkzg=", new DateTime(2019, 4, 10, 13, 42, 27, 430, DateTimeKind.Local).AddTicks(2408), "Claudie_Von90" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 12,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 6, new DateTime(2019, 4, 10, 13, 42, 27, 437, DateTimeKind.Local).AddTicks(7089), "Brady24@yahoo.com", "fvm+iVG/i1teAwJu6gjd99cM3VzEf33Q4lZmpYa3gtA=", "qwNd4zHDb4xaQdyEy9lNs/c4y5rsfGTbauvyVwf8COc=", new DateTime(2019, 4, 10, 13, 42, 27, 437, DateTimeKind.Local).AddTicks(7102), "Bertha_Kuhic" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 13,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 13, new DateTime(2019, 4, 10, 13, 42, 27, 445, DateTimeKind.Local).AddTicks(471), "Amira84@yahoo.com", "OO/o8dRJNQHE/Cy5SsMuUoV59W4CfSqsYA1JYbnBbKA=", "BcFPRW2kgTsEIJrwMiZMtEJGYYttXjV8hE2B6jCKgSQ=", new DateTime(2019, 4, 10, 13, 42, 27, 445, DateTimeKind.Local).AddTicks(482), "Roxane.Bechtelar" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 14,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 17, new DateTime(2019, 4, 10, 13, 42, 27, 452, DateTimeKind.Local).AddTicks(5168), "Reagan_Gottlieb96@gmail.com", "8/CAtMOKHMOIgZYZyjfPyFgxlGUPh2TRexcrWcVuKR0=", "N4HTAz9evQh9HSBh4Ui10gobECk9Re8EzmJd04m9PKc=", new DateTime(2019, 4, 10, 13, 42, 27, 452, DateTimeKind.Local).AddTicks(5186), "Willow84" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 15,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 10, new DateTime(2019, 4, 10, 13, 42, 27, 460, DateTimeKind.Local).AddTicks(40), "Dane_Toy25@hotmail.com", "5ksghg+QRIoR4HCwib/D71e6GWEgIzgvHUy+/jIU8u4=", "YHCWI8ogFKXeVnbDjONhQnBcwOv9KTCv65i5aPHgxy4=", new DateTime(2019, 4, 10, 13, 42, 27, 460, DateTimeKind.Local).AddTicks(52), "Lyric_Powlowski" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 16,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 9, new DateTime(2019, 4, 10, 13, 42, 27, 467, DateTimeKind.Local).AddTicks(7399), "Clyde.Kunze@gmail.com", "jN562rlj3Y4M9JiXhpaq22V9r5A0MCACnb8p5a03L24=", "PDjMLphnPnJG54zhHoLXWCW2Jr4kTqmaumBZuqRp0GM=", new DateTime(2019, 4, 10, 13, 42, 27, 467, DateTimeKind.Local).AddTicks(7433), "Kevon45" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 17,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 15, new DateTime(2019, 4, 10, 13, 42, 27, 475, DateTimeKind.Local).AddTicks(6227), "Chanel.Champlin68@hotmail.com", "AH5M8V4Q25IYweo/q/iVqQQ4h/xed8dxtZE0aeS5cWM=", "73owabAASkyG0Dvg2TnKlvWRX0FzS0ur49Ur3J5H3qM=", new DateTime(2019, 4, 10, 13, 42, 27, 475, DateTimeKind.Local).AddTicks(6272), "Jewell_Schmitt" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 18,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 4, new DateTime(2019, 4, 10, 13, 42, 27, 483, DateTimeKind.Local).AddTicks(1289), "Watson.Bahringer@yahoo.com", "Q62jzI0XYrLxW7Vfi/yI+uS4YjSlRcUbQZxMQGfc8C0=", "vSbXm9StFleAMjE1o2oRvdW3lJHYt7hkHrsLCa7Dg4U=", new DateTime(2019, 4, 10, 13, 42, 27, 483, DateTimeKind.Local).AddTicks(1302), "Jazmin98" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 19,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 13, new DateTime(2019, 4, 10, 13, 42, 27, 490, DateTimeKind.Local).AddTicks(5812), "Aditya.Kerluke@yahoo.com", "7D2UgGVDo4CTiK0CFT/Cd2Kryi7caZwemc5a7f9Xus8=", "zUsIM0NUWcyfmuUZWfbZD/ip+Wl+JZe/hLUSru5/rSM=", new DateTime(2019, 4, 10, 13, 42, 27, 490, DateTimeKind.Local).AddTicks(5824), "Josefa_Brakus" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 20,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 8, new DateTime(2019, 4, 10, 13, 42, 27, 498, DateTimeKind.Local).AddTicks(265), "Stewart.Kiehn@hotmail.com", "huLmL4GntVXBmnUFICkmowedAskr5IoAk5wW5424zqQ=", "AmgoIHeRfU91KKABVopqN9E6rEyTLpgLW9RqszTP2zc=", new DateTime(2019, 4, 10, 13, 42, 27, 498, DateTimeKind.Local).AddTicks(276), "Margaretta_Schaden" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 21,
                columns: new[] { "CreatedAt", "Password", "Salt", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 4, 10, 13, 42, 27, 505, DateTimeKind.Local).AddTicks(3866), "C+XvvutUOnUo6U86oKynPexTPQkkhKfd4pYAf+LzYPE=", "wBQhcRU+Nh0iNrhTwxtzwOXjMhOxlLTx5BkVJ3h6sB4=", new DateTime(2019, 4, 10, 13, 42, 27, 505, DateTimeKind.Local).AddTicks(3866) });

            migrationBuilder.CreateIndex(
                name: "IX_PostReactions_PostId",
                table: "PostReactions",
                column: "PostId");

            migrationBuilder.CreateIndex(
                name: "IX_CommentReactions_CommentId",
                table: "CommentReactions",
                column: "CommentId");
        }
    }
}
