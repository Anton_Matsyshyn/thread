﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Thread_.NET.DAL.Migrations
{
    public partial class Add_IsDeleted_Field_To_Post : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 5);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 6);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 7);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 8);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 9);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 10);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 11);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 12);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 13);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 14);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 15);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 16);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 17);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 18);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 19);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 20);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 5);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 6);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 7);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 8);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 9);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 10);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 11);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 12);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 13);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 14);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 15);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 16);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 17);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 18);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 19);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 20);

            migrationBuilder.AddColumn<bool>(
                name: "IsDeleted",
                table: "Posts",
                nullable: false,
                defaultValue: false);

            migrationBuilder.InsertData(
                table: "CommentReactions",
                columns: new[] { "Id", "CommentId", "CreatedAt", "IsLike", "UpdatedAt", "UserId" },
                values: new object[,]
                {
                    { 20, 15, new DateTime(2020, 6, 14, 15, 53, 45, 310, DateTimeKind.Local).AddTicks(9756), false, new DateTime(2020, 6, 14, 15, 53, 45, 310, DateTimeKind.Local).AddTicks(9762), 1 },
                    { 2, 10, new DateTime(2020, 6, 14, 15, 53, 45, 310, DateTimeKind.Local).AddTicks(9014), true, new DateTime(2020, 6, 14, 15, 53, 45, 310, DateTimeKind.Local).AddTicks(9037), 6 },
                    { 3, 4, new DateTime(2020, 6, 14, 15, 53, 45, 310, DateTimeKind.Local).AddTicks(9087), true, new DateTime(2020, 6, 14, 15, 53, 45, 310, DateTimeKind.Local).AddTicks(9095), 5 },
                    { 4, 5, new DateTime(2020, 6, 14, 15, 53, 45, 310, DateTimeKind.Local).AddTicks(9128), true, new DateTime(2020, 6, 14, 15, 53, 45, 310, DateTimeKind.Local).AddTicks(9135), 7 },
                    { 5, 14, new DateTime(2020, 6, 14, 15, 53, 45, 310, DateTimeKind.Local).AddTicks(9166), true, new DateTime(2020, 6, 14, 15, 53, 45, 310, DateTimeKind.Local).AddTicks(9172), 11 },
                    { 6, 18, new DateTime(2020, 6, 14, 15, 53, 45, 310, DateTimeKind.Local).AddTicks(9203), false, new DateTime(2020, 6, 14, 15, 53, 45, 310, DateTimeKind.Local).AddTicks(9210), 20 },
                    { 7, 20, new DateTime(2020, 6, 14, 15, 53, 45, 310, DateTimeKind.Local).AddTicks(9291), false, new DateTime(2020, 6, 14, 15, 53, 45, 310, DateTimeKind.Local).AddTicks(9298), 8 },
                    { 8, 16, new DateTime(2020, 6, 14, 15, 53, 45, 310, DateTimeKind.Local).AddTicks(9328), true, new DateTime(2020, 6, 14, 15, 53, 45, 310, DateTimeKind.Local).AddTicks(9335), 5 },
                    { 9, 17, new DateTime(2020, 6, 14, 15, 53, 45, 310, DateTimeKind.Local).AddTicks(9365), true, new DateTime(2020, 6, 14, 15, 53, 45, 310, DateTimeKind.Local).AddTicks(9372), 12 },
                    { 10, 3, new DateTime(2020, 6, 14, 15, 53, 45, 310, DateTimeKind.Local).AddTicks(9402), false, new DateTime(2020, 6, 14, 15, 53, 45, 310, DateTimeKind.Local).AddTicks(9408), 3 },
                    { 1, 15, new DateTime(2020, 6, 14, 15, 53, 45, 310, DateTimeKind.Local).AddTicks(7587), true, new DateTime(2020, 6, 14, 15, 53, 45, 310, DateTimeKind.Local).AddTicks(8421), 8 },
                    { 12, 12, new DateTime(2020, 6, 14, 15, 53, 45, 310, DateTimeKind.Local).AddTicks(9473), true, new DateTime(2020, 6, 14, 15, 53, 45, 310, DateTimeKind.Local).AddTicks(9479), 17 },
                    { 13, 2, new DateTime(2020, 6, 14, 15, 53, 45, 310, DateTimeKind.Local).AddTicks(9508), false, new DateTime(2020, 6, 14, 15, 53, 45, 310, DateTimeKind.Local).AddTicks(9515), 15 },
                    { 14, 6, new DateTime(2020, 6, 14, 15, 53, 45, 310, DateTimeKind.Local).AddTicks(9543), false, new DateTime(2020, 6, 14, 15, 53, 45, 310, DateTimeKind.Local).AddTicks(9550), 17 },
                    { 15, 18, new DateTime(2020, 6, 14, 15, 53, 45, 310, DateTimeKind.Local).AddTicks(9581), true, new DateTime(2020, 6, 14, 15, 53, 45, 310, DateTimeKind.Local).AddTicks(9587), 1 },
                    { 16, 12, new DateTime(2020, 6, 14, 15, 53, 45, 310, DateTimeKind.Local).AddTicks(9615), false, new DateTime(2020, 6, 14, 15, 53, 45, 310, DateTimeKind.Local).AddTicks(9622), 12 },
                    { 17, 3, new DateTime(2020, 6, 14, 15, 53, 45, 310, DateTimeKind.Local).AddTicks(9651), false, new DateTime(2020, 6, 14, 15, 53, 45, 310, DateTimeKind.Local).AddTicks(9657), 19 },
                    { 18, 5, new DateTime(2020, 6, 14, 15, 53, 45, 310, DateTimeKind.Local).AddTicks(9685), true, new DateTime(2020, 6, 14, 15, 53, 45, 310, DateTimeKind.Local).AddTicks(9691), 2 },
                    { 19, 3, new DateTime(2020, 6, 14, 15, 53, 45, 310, DateTimeKind.Local).AddTicks(9720), false, new DateTime(2020, 6, 14, 15, 53, 45, 310, DateTimeKind.Local).AddTicks(9726), 9 },
                    { 11, 2, new DateTime(2020, 6, 14, 15, 53, 45, 310, DateTimeKind.Local).AddTicks(9437), true, new DateTime(2020, 6, 14, 15, 53, 45, 310, DateTimeKind.Local).AddTicks(9444), 2 }
                });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 1, "Hic dolor et aut doloribus eum eum.", new DateTime(2020, 6, 14, 15, 53, 45, 295, DateTimeKind.Local).AddTicks(5625), 14, new DateTime(2020, 6, 14, 15, 53, 45, 295, DateTimeKind.Local).AddTicks(6486) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 19, "Non dolorem ipsa temporibus.", new DateTime(2020, 6, 14, 15, 53, 45, 295, DateTimeKind.Local).AddTicks(7266), 10, new DateTime(2020, 6, 14, 15, 53, 45, 295, DateTimeKind.Local).AddTicks(7289) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 12, "Eos labore voluptas distinctio qui consequuntur nesciunt.", new DateTime(2020, 6, 14, 15, 53, 45, 295, DateTimeKind.Local).AddTicks(7415), 1, new DateTime(2020, 6, 14, 15, 53, 45, 295, DateTimeKind.Local).AddTicks(7423) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 20, "Autem omnis voluptatibus reiciendis tempora perspiciatis laborum et.", new DateTime(2020, 6, 14, 15, 53, 45, 295, DateTimeKind.Local).AddTicks(7512), 13, new DateTime(2020, 6, 14, 15, 53, 45, 295, DateTimeKind.Local).AddTicks(7520) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 16, "Et enim cupiditate ut.", new DateTime(2020, 6, 14, 15, 53, 45, 295, DateTimeKind.Local).AddTicks(7644), 6, new DateTime(2020, 6, 14, 15, 53, 45, 295, DateTimeKind.Local).AddTicks(7653) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 17, "Delectus odit aliquid est ipsum omnis.", new DateTime(2020, 6, 14, 15, 53, 45, 295, DateTimeKind.Local).AddTicks(7726), 14, new DateTime(2020, 6, 14, 15, 53, 45, 295, DateTimeKind.Local).AddTicks(7733) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 7,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 1, "Eveniet dolores expedita enim ducimus hic sed.", new DateTime(2020, 6, 14, 15, 53, 45, 295, DateTimeKind.Local).AddTicks(7813), 5, new DateTime(2020, 6, 14, 15, 53, 45, 295, DateTimeKind.Local).AddTicks(7820) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 8,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 20, "Sed voluptatibus minima molestiae ea tenetur quo reprehenderit.", new DateTime(2020, 6, 14, 15, 53, 45, 295, DateTimeKind.Local).AddTicks(7900), 19, new DateTime(2020, 6, 14, 15, 53, 45, 295, DateTimeKind.Local).AddTicks(7908) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 9,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 10, "Omnis id et omnis animi itaque ea et id.", new DateTime(2020, 6, 14, 15, 53, 45, 295, DateTimeKind.Local).AddTicks(7990), 5, new DateTime(2020, 6, 14, 15, 53, 45, 295, DateTimeKind.Local).AddTicks(7998) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 10,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 5, "Aut quo recusandae minima consequatur repellendus aut.", new DateTime(2020, 6, 14, 15, 53, 45, 295, DateTimeKind.Local).AddTicks(8072), 10, new DateTime(2020, 6, 14, 15, 53, 45, 295, DateTimeKind.Local).AddTicks(8079) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 11,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 4, "Voluptatem illo quam nobis cum doloribus est sit.", new DateTime(2020, 6, 14, 15, 53, 45, 295, DateTimeKind.Local).AddTicks(8200), 17, new DateTime(2020, 6, 14, 15, 53, 45, 295, DateTimeKind.Local).AddTicks(8208) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 12,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 14, "Qui eos omnis temporibus excepturi nulla aut possimus ut.", new DateTime(2020, 6, 14, 15, 53, 45, 295, DateTimeKind.Local).AddTicks(8291), 6, new DateTime(2020, 6, 14, 15, 53, 45, 295, DateTimeKind.Local).AddTicks(8298) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 13,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 9, "Iusto enim porro.", new DateTime(2020, 6, 14, 15, 53, 45, 295, DateTimeKind.Local).AddTicks(8354), 14, new DateTime(2020, 6, 14, 15, 53, 45, 295, DateTimeKind.Local).AddTicks(8361) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 14,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 18, "Id delectus minus ipsam sunt quo.", new DateTime(2020, 6, 14, 15, 53, 45, 295, DateTimeKind.Local).AddTicks(8430), 7, new DateTime(2020, 6, 14, 15, 53, 45, 295, DateTimeKind.Local).AddTicks(8438) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 15,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 11, "Quaerat quis nostrum enim enim commodi autem minima sunt ipsum.", new DateTime(2020, 6, 14, 15, 53, 45, 295, DateTimeKind.Local).AddTicks(8525), 18, new DateTime(2020, 6, 14, 15, 53, 45, 295, DateTimeKind.Local).AddTicks(8533) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 16,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 3, "Sapiente odio occaecati ipsa omnis ab maiores esse sapiente error.", new DateTime(2020, 6, 14, 15, 53, 45, 295, DateTimeKind.Local).AddTicks(8660), 18, new DateTime(2020, 6, 14, 15, 53, 45, 295, DateTimeKind.Local).AddTicks(8668) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 17,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 20, "Officiis voluptatem esse ab ab reiciendis ipsum.", new DateTime(2020, 6, 14, 15, 53, 45, 295, DateTimeKind.Local).AddTicks(8744), 16, new DateTime(2020, 6, 14, 15, 53, 45, 295, DateTimeKind.Local).AddTicks(8752) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 18,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 4, "Cum ex doloribus rerum rerum doloribus.", new DateTime(2020, 6, 14, 15, 53, 45, 295, DateTimeKind.Local).AddTicks(8818), 6, new DateTime(2020, 6, 14, 15, 53, 45, 295, DateTimeKind.Local).AddTicks(8826) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 19,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 17, "Et nam alias exercitationem.", new DateTime(2020, 6, 14, 15, 53, 45, 295, DateTimeKind.Local).AddTicks(8891), 1, new DateTime(2020, 6, 14, 15, 53, 45, 295, DateTimeKind.Local).AddTicks(8899) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 20,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 19, "Omnis ex et ullam aut consectetur velit quo quos assumenda.", new DateTime(2020, 6, 14, 15, 53, 45, 295, DateTimeKind.Local).AddTicks(8986), 11, new DateTime(2020, 6, 14, 15, 53, 45, 295, DateTimeKind.Local).AddTicks(8993) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 14, 15, 53, 45, 41, DateTimeKind.Local).AddTicks(1520), "https://s3.amazonaws.com/uifaces/faces/twitter/jjsiii/128.jpg", new DateTime(2020, 6, 14, 15, 53, 45, 41, DateTimeKind.Local).AddTicks(7282) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 14, 15, 53, 45, 41, DateTimeKind.Local).AddTicks(8135), "https://s3.amazonaws.com/uifaces/faces/twitter/andrewcohen/128.jpg", new DateTime(2020, 6, 14, 15, 53, 45, 41, DateTimeKind.Local).AddTicks(8167) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 14, 15, 53, 45, 41, DateTimeKind.Local).AddTicks(8255), "https://s3.amazonaws.com/uifaces/faces/twitter/lebinoclard/128.jpg", new DateTime(2020, 6, 14, 15, 53, 45, 41, DateTimeKind.Local).AddTicks(8263) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 14, 15, 53, 45, 41, DateTimeKind.Local).AddTicks(8295), "https://s3.amazonaws.com/uifaces/faces/twitter/panchajanyag/128.jpg", new DateTime(2020, 6, 14, 15, 53, 45, 41, DateTimeKind.Local).AddTicks(8301) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 14, 15, 53, 45, 41, DateTimeKind.Local).AddTicks(8327), "https://s3.amazonaws.com/uifaces/faces/twitter/maiklam/128.jpg", new DateTime(2020, 6, 14, 15, 53, 45, 41, DateTimeKind.Local).AddTicks(8333) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 14, 15, 53, 45, 41, DateTimeKind.Local).AddTicks(8359), "https://s3.amazonaws.com/uifaces/faces/twitter/kalmerrautam/128.jpg", new DateTime(2020, 6, 14, 15, 53, 45, 41, DateTimeKind.Local).AddTicks(8365) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 7,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 14, 15, 53, 45, 41, DateTimeKind.Local).AddTicks(8391), "https://s3.amazonaws.com/uifaces/faces/twitter/anatolinicolae/128.jpg", new DateTime(2020, 6, 14, 15, 53, 45, 41, DateTimeKind.Local).AddTicks(8398) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 8,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 14, 15, 53, 45, 41, DateTimeKind.Local).AddTicks(8423), "https://s3.amazonaws.com/uifaces/faces/twitter/ninjad3m0/128.jpg", new DateTime(2020, 6, 14, 15, 53, 45, 41, DateTimeKind.Local).AddTicks(8430) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 9,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 14, 15, 53, 45, 41, DateTimeKind.Local).AddTicks(8455), "https://s3.amazonaws.com/uifaces/faces/twitter/arashmanteghi/128.jpg", new DateTime(2020, 6, 14, 15, 53, 45, 41, DateTimeKind.Local).AddTicks(8461) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 10,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 14, 15, 53, 45, 41, DateTimeKind.Local).AddTicks(8486), "https://s3.amazonaws.com/uifaces/faces/twitter/carlosm/128.jpg", new DateTime(2020, 6, 14, 15, 53, 45, 41, DateTimeKind.Local).AddTicks(8493) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 11,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 14, 15, 53, 45, 41, DateTimeKind.Local).AddTicks(8519), "https://s3.amazonaws.com/uifaces/faces/twitter/madshensel/128.jpg", new DateTime(2020, 6, 14, 15, 53, 45, 41, DateTimeKind.Local).AddTicks(8525) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 12,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 14, 15, 53, 45, 41, DateTimeKind.Local).AddTicks(8549), "https://s3.amazonaws.com/uifaces/faces/twitter/tgerken/128.jpg", new DateTime(2020, 6, 14, 15, 53, 45, 41, DateTimeKind.Local).AddTicks(8556) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 13,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 14, 15, 53, 45, 41, DateTimeKind.Local).AddTicks(8581), "https://s3.amazonaws.com/uifaces/faces/twitter/andresenfredrik/128.jpg", new DateTime(2020, 6, 14, 15, 53, 45, 41, DateTimeKind.Local).AddTicks(8587) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 14,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 14, 15, 53, 45, 41, DateTimeKind.Local).AddTicks(8613), "https://s3.amazonaws.com/uifaces/faces/twitter/elliotlewis/128.jpg", new DateTime(2020, 6, 14, 15, 53, 45, 41, DateTimeKind.Local).AddTicks(8620) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 15,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 14, 15, 53, 45, 41, DateTimeKind.Local).AddTicks(8643), "https://s3.amazonaws.com/uifaces/faces/twitter/nandini_m/128.jpg", new DateTime(2020, 6, 14, 15, 53, 45, 41, DateTimeKind.Local).AddTicks(8650) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 16,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 14, 15, 53, 45, 41, DateTimeKind.Local).AddTicks(8674), "https://s3.amazonaws.com/uifaces/faces/twitter/harry_sistalam/128.jpg", new DateTime(2020, 6, 14, 15, 53, 45, 41, DateTimeKind.Local).AddTicks(8681) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 17,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 14, 15, 53, 45, 41, DateTimeKind.Local).AddTicks(8705), "https://s3.amazonaws.com/uifaces/faces/twitter/wake_gs/128.jpg", new DateTime(2020, 6, 14, 15, 53, 45, 41, DateTimeKind.Local).AddTicks(8712) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 18,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 14, 15, 53, 45, 41, DateTimeKind.Local).AddTicks(8735), "https://s3.amazonaws.com/uifaces/faces/twitter/mirfanqureshi/128.jpg", new DateTime(2020, 6, 14, 15, 53, 45, 41, DateTimeKind.Local).AddTicks(8742) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 19,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 14, 15, 53, 45, 41, DateTimeKind.Local).AddTicks(8766), "https://s3.amazonaws.com/uifaces/faces/twitter/abdots/128.jpg", new DateTime(2020, 6, 14, 15, 53, 45, 41, DateTimeKind.Local).AddTicks(8772) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 20,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 14, 15, 53, 45, 41, DateTimeKind.Local).AddTicks(8798), "https://s3.amazonaws.com/uifaces/faces/twitter/mactopus/128.jpg", new DateTime(2020, 6, 14, 15, 53, 45, 41, DateTimeKind.Local).AddTicks(8804) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 21,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 14, 15, 53, 45, 47, DateTimeKind.Local).AddTicks(4973), "https://picsum.photos/640/480/?image=116", new DateTime(2020, 6, 14, 15, 53, 45, 47, DateTimeKind.Local).AddTicks(5880) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 22,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 14, 15, 53, 45, 47, DateTimeKind.Local).AddTicks(6266), "https://picsum.photos/640/480/?image=351", new DateTime(2020, 6, 14, 15, 53, 45, 47, DateTimeKind.Local).AddTicks(6301) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 23,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 14, 15, 53, 45, 47, DateTimeKind.Local).AddTicks(6340), "https://picsum.photos/640/480/?image=487", new DateTime(2020, 6, 14, 15, 53, 45, 47, DateTimeKind.Local).AddTicks(6347) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 24,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 14, 15, 53, 45, 47, DateTimeKind.Local).AddTicks(6374), "https://picsum.photos/640/480/?image=733", new DateTime(2020, 6, 14, 15, 53, 45, 47, DateTimeKind.Local).AddTicks(6380) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 25,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 14, 15, 53, 45, 47, DateTimeKind.Local).AddTicks(6406), "https://picsum.photos/640/480/?image=104", new DateTime(2020, 6, 14, 15, 53, 45, 47, DateTimeKind.Local).AddTicks(6413) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 26,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 14, 15, 53, 45, 47, DateTimeKind.Local).AddTicks(6438), "https://picsum.photos/640/480/?image=186", new DateTime(2020, 6, 14, 15, 53, 45, 47, DateTimeKind.Local).AddTicks(6444) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 27,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 14, 15, 53, 45, 47, DateTimeKind.Local).AddTicks(6470), "https://picsum.photos/640/480/?image=702", new DateTime(2020, 6, 14, 15, 53, 45, 47, DateTimeKind.Local).AddTicks(6476) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 28,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 14, 15, 53, 45, 47, DateTimeKind.Local).AddTicks(6500), "https://picsum.photos/640/480/?image=295", new DateTime(2020, 6, 14, 15, 53, 45, 47, DateTimeKind.Local).AddTicks(6506) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 29,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 14, 15, 53, 45, 47, DateTimeKind.Local).AddTicks(6531), "https://picsum.photos/640/480/?image=264", new DateTime(2020, 6, 14, 15, 53, 45, 47, DateTimeKind.Local).AddTicks(6538) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 30,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 14, 15, 53, 45, 47, DateTimeKind.Local).AddTicks(6563), "https://picsum.photos/640/480/?image=209", new DateTime(2020, 6, 14, 15, 53, 45, 47, DateTimeKind.Local).AddTicks(6569) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 31,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 14, 15, 53, 45, 47, DateTimeKind.Local).AddTicks(6593), "https://picsum.photos/640/480/?image=566", new DateTime(2020, 6, 14, 15, 53, 45, 47, DateTimeKind.Local).AddTicks(6600) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 32,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 14, 15, 53, 45, 47, DateTimeKind.Local).AddTicks(6624), "https://picsum.photos/640/480/?image=454", new DateTime(2020, 6, 14, 15, 53, 45, 47, DateTimeKind.Local).AddTicks(6630) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 33,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 14, 15, 53, 45, 47, DateTimeKind.Local).AddTicks(6654), "https://picsum.photos/640/480/?image=685", new DateTime(2020, 6, 14, 15, 53, 45, 47, DateTimeKind.Local).AddTicks(6660) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 34,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 14, 15, 53, 45, 47, DateTimeKind.Local).AddTicks(6685), "https://picsum.photos/640/480/?image=148", new DateTime(2020, 6, 14, 15, 53, 45, 47, DateTimeKind.Local).AddTicks(6691) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 35,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 14, 15, 53, 45, 47, DateTimeKind.Local).AddTicks(6775), "https://picsum.photos/640/480/?image=222", new DateTime(2020, 6, 14, 15, 53, 45, 47, DateTimeKind.Local).AddTicks(6782) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 36,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 14, 15, 53, 45, 47, DateTimeKind.Local).AddTicks(6810), "https://picsum.photos/640/480/?image=83", new DateTime(2020, 6, 14, 15, 53, 45, 47, DateTimeKind.Local).AddTicks(6816) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 37,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 14, 15, 53, 45, 47, DateTimeKind.Local).AddTicks(6840), "https://picsum.photos/640/480/?image=301", new DateTime(2020, 6, 14, 15, 53, 45, 47, DateTimeKind.Local).AddTicks(6847) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 38,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 14, 15, 53, 45, 47, DateTimeKind.Local).AddTicks(6871), "https://picsum.photos/640/480/?image=458", new DateTime(2020, 6, 14, 15, 53, 45, 47, DateTimeKind.Local).AddTicks(6877) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 39,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 14, 15, 53, 45, 47, DateTimeKind.Local).AddTicks(6902), "https://picsum.photos/640/480/?image=663", new DateTime(2020, 6, 14, 15, 53, 45, 47, DateTimeKind.Local).AddTicks(6908) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 40,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 14, 15, 53, 45, 47, DateTimeKind.Local).AddTicks(6933), "https://picsum.photos/640/480/?image=446", new DateTime(2020, 6, 14, 15, 53, 45, 47, DateTimeKind.Local).AddTicks(6939) });

            migrationBuilder.InsertData(
                table: "PostReactions",
                columns: new[] { "Id", "CreatedAt", "IsLike", "PostId", "UpdatedAt", "UserId" },
                values: new object[,]
                {
                    { 10, new DateTime(2020, 6, 14, 15, 53, 45, 303, DateTimeKind.Local).AddTicks(8597), true, 6, new DateTime(2020, 6, 14, 15, 53, 45, 303, DateTimeKind.Local).AddTicks(8603), 20 },
                    { 11, new DateTime(2020, 6, 14, 15, 53, 45, 303, DateTimeKind.Local).AddTicks(8633), true, 3, new DateTime(2020, 6, 14, 15, 53, 45, 303, DateTimeKind.Local).AddTicks(8639), 19 },
                    { 12, new DateTime(2020, 6, 14, 15, 53, 45, 303, DateTimeKind.Local).AddTicks(8669), false, 14, new DateTime(2020, 6, 14, 15, 53, 45, 303, DateTimeKind.Local).AddTicks(8675), 3 },
                    { 13, new DateTime(2020, 6, 14, 15, 53, 45, 303, DateTimeKind.Local).AddTicks(8703), false, 16, new DateTime(2020, 6, 14, 15, 53, 45, 303, DateTimeKind.Local).AddTicks(8710), 14 },
                    { 15, new DateTime(2020, 6, 14, 15, 53, 45, 303, DateTimeKind.Local).AddTicks(8774), true, 1, new DateTime(2020, 6, 14, 15, 53, 45, 303, DateTimeKind.Local).AddTicks(8780), 8 },
                    { 18, new DateTime(2020, 6, 14, 15, 53, 45, 303, DateTimeKind.Local).AddTicks(8881), false, 5, new DateTime(2020, 6, 14, 15, 53, 45, 303, DateTimeKind.Local).AddTicks(8887), 1 },
                    { 16, new DateTime(2020, 6, 14, 15, 53, 45, 303, DateTimeKind.Local).AddTicks(8809), true, 10, new DateTime(2020, 6, 14, 15, 53, 45, 303, DateTimeKind.Local).AddTicks(8816), 11 },
                    { 17, new DateTime(2020, 6, 14, 15, 53, 45, 303, DateTimeKind.Local).AddTicks(8845), true, 17, new DateTime(2020, 6, 14, 15, 53, 45, 303, DateTimeKind.Local).AddTicks(8851), 4 },
                    { 9, new DateTime(2020, 6, 14, 15, 53, 45, 303, DateTimeKind.Local).AddTicks(8560), true, 12, new DateTime(2020, 6, 14, 15, 53, 45, 303, DateTimeKind.Local).AddTicks(8566), 10 },
                    { 20, new DateTime(2020, 6, 14, 15, 53, 45, 303, DateTimeKind.Local).AddTicks(8952), false, 16, new DateTime(2020, 6, 14, 15, 53, 45, 303, DateTimeKind.Local).AddTicks(8958), 13 },
                    { 19, new DateTime(2020, 6, 14, 15, 53, 45, 303, DateTimeKind.Local).AddTicks(8916), false, 6, new DateTime(2020, 6, 14, 15, 53, 45, 303, DateTimeKind.Local).AddTicks(8923), 1 },
                    { 14, new DateTime(2020, 6, 14, 15, 53, 45, 303, DateTimeKind.Local).AddTicks(8739), true, 8, new DateTime(2020, 6, 14, 15, 53, 45, 303, DateTimeKind.Local).AddTicks(8745), 21 },
                    { 8, new DateTime(2020, 6, 14, 15, 53, 45, 303, DateTimeKind.Local).AddTicks(8522), true, 17, new DateTime(2020, 6, 14, 15, 53, 45, 303, DateTimeKind.Local).AddTicks(8528), 5 },
                    { 3, new DateTime(2020, 6, 14, 15, 53, 45, 303, DateTimeKind.Local).AddTicks(8334), false, 16, new DateTime(2020, 6, 14, 15, 53, 45, 303, DateTimeKind.Local).AddTicks(8341), 15 },
                    { 6, new DateTime(2020, 6, 14, 15, 53, 45, 303, DateTimeKind.Local).AddTicks(8445), true, 18, new DateTime(2020, 6, 14, 15, 53, 45, 303, DateTimeKind.Local).AddTicks(8452), 19 },
                    { 5, new DateTime(2020, 6, 14, 15, 53, 45, 303, DateTimeKind.Local).AddTicks(8409), false, 19, new DateTime(2020, 6, 14, 15, 53, 45, 303, DateTimeKind.Local).AddTicks(8415), 7 },
                    { 1, new DateTime(2020, 6, 14, 15, 53, 45, 303, DateTimeKind.Local).AddTicks(6679), true, 14, new DateTime(2020, 6, 14, 15, 53, 45, 303, DateTimeKind.Local).AddTicks(7519), 11 },
                    { 4, new DateTime(2020, 6, 14, 15, 53, 45, 303, DateTimeKind.Local).AddTicks(8373), true, 9, new DateTime(2020, 6, 14, 15, 53, 45, 303, DateTimeKind.Local).AddTicks(8379), 8 },
                    { 2, new DateTime(2020, 6, 14, 15, 53, 45, 303, DateTimeKind.Local).AddTicks(8270), true, 2, new DateTime(2020, 6, 14, 15, 53, 45, 303, DateTimeKind.Local).AddTicks(8287), 5 },
                    { 7, new DateTime(2020, 6, 14, 15, 53, 45, 303, DateTimeKind.Local).AddTicks(8485), false, 1, new DateTime(2020, 6, 14, 15, 53, 45, 303, DateTimeKind.Local).AddTicks(8492), 13 }
                });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 7, @"Ipsam fugiat et non et quia.
Est fugit libero voluptas eum.
Corrupti incidunt enim autem nulla magni omnis.
Illum temporibus cupiditate omnis sed.
Doloremque quam sunt.", new DateTime(2020, 6, 14, 15, 53, 45, 287, DateTimeKind.Local).AddTicks(7321), 27, new DateTime(2020, 6, 14, 15, 53, 45, 287, DateTimeKind.Local).AddTicks(8256) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 17, @"Aut est mollitia enim non.
Occaecati voluptatum amet iste voluptatem.
Magni nostrum dolor reprehenderit amet atque et.
Et ipsa voluptates autem.
Sequi magnam nemo voluptatem quia ducimus.
Dolorum quia aliquid.", new DateTime(2020, 6, 14, 15, 53, 45, 287, DateTimeKind.Local).AddTicks(9452), 33, new DateTime(2020, 6, 14, 15, 53, 45, 287, DateTimeKind.Local).AddTicks(9478) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 4, @"Omnis nisi id delectus non occaecati.
Id rerum optio nesciunt fugiat vel fugiat dolor quo dolor.
Non quis aspernatur numquam.
Eos aut non aut et earum.
Et et aut dolores.", new DateTime(2020, 6, 14, 15, 53, 45, 287, DateTimeKind.Local).AddTicks(9815), 28, new DateTime(2020, 6, 14, 15, 53, 45, 287, DateTimeKind.Local).AddTicks(9826) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 11, "Pariatur eos ut excepturi libero voluptas dolorem laborum veniam temporibus.", new DateTime(2020, 6, 14, 15, 53, 45, 288, DateTimeKind.Local).AddTicks(993), 29, new DateTime(2020, 6, 14, 15, 53, 45, 288, DateTimeKind.Local).AddTicks(1017) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 15, "Incidunt voluptas unde necessitatibus quod vero animi. Repellat quibusdam atque vitae quas tenetur ipsam. Fuga in quo quis quasi eos. Perspiciatis consequuntur eligendi dolores facilis sit et. Ipsam inventore ab et dolorem vel.", new DateTime(2020, 6, 14, 15, 53, 45, 288, DateTimeKind.Local).AddTicks(2852), 26, new DateTime(2020, 6, 14, 15, 53, 45, 288, DateTimeKind.Local).AddTicks(2875) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 5, "magnam", new DateTime(2020, 6, 14, 15, 53, 45, 288, DateTimeKind.Local).AddTicks(3468), 27, new DateTime(2020, 6, 14, 15, 53, 45, 288, DateTimeKind.Local).AddTicks(3487) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 7,
                columns: new[] { "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { @"Nam distinctio delectus voluptatem.
Aut alias amet sed sunt maxime consequatur voluptas ab.", new DateTime(2020, 6, 14, 15, 53, 45, 288, DateTimeKind.Local).AddTicks(3661), 28, new DateTime(2020, 6, 14, 15, 53, 45, 288, DateTimeKind.Local).AddTicks(3671) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 8,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 8, "Consequatur dolorem accusantium blanditiis corporis voluptatem.", new DateTime(2020, 6, 14, 15, 53, 45, 288, DateTimeKind.Local).AddTicks(3838), 34, new DateTime(2020, 6, 14, 15, 53, 45, 288, DateTimeKind.Local).AddTicks(3848) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 9,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 17, @"Possimus et accusamus.
Aut doloremque quo soluta et.
Voluptatem enim accusamus sint natus nihil consequatur quas.
Est ullam hic id aliquid voluptatem eius non.
Illum nam odio ea magnam.", new DateTime(2020, 6, 14, 15, 53, 45, 288, DateTimeKind.Local).AddTicks(4080), 22, new DateTime(2020, 6, 14, 15, 53, 45, 288, DateTimeKind.Local).AddTicks(4089) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 10,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 19, "debitis", new DateTime(2020, 6, 14, 15, 53, 45, 288, DateTimeKind.Local).AddTicks(4171), 33, new DateTime(2020, 6, 14, 15, 53, 45, 288, DateTimeKind.Local).AddTicks(4179) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 11,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 11, "cupiditate", new DateTime(2020, 6, 14, 15, 53, 45, 288, DateTimeKind.Local).AddTicks(4220), 26, new DateTime(2020, 6, 14, 15, 53, 45, 288, DateTimeKind.Local).AddTicks(4227) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 12,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 20, "Sequi itaque quod est nulla vero veritatis doloremque dolorem. Dolorem sint suscipit nam qui deleniti eos velit voluptatem. Veritatis aut id autem unde neque dolorum fuga velit accusantium.", new DateTime(2020, 6, 14, 15, 53, 45, 288, DateTimeKind.Local).AddTicks(4428), 29, new DateTime(2020, 6, 14, 15, 53, 45, 288, DateTimeKind.Local).AddTicks(4436) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 13,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 13, "Veritatis enim laborum sunt.", new DateTime(2020, 6, 14, 15, 53, 45, 288, DateTimeKind.Local).AddTicks(4546), 37, new DateTime(2020, 6, 14, 15, 53, 45, 288, DateTimeKind.Local).AddTicks(4554) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 14,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 5, "Labore ut sit ea.", new DateTime(2020, 6, 14, 15, 53, 45, 288, DateTimeKind.Local).AddTicks(4626), 27, new DateTime(2020, 6, 14, 15, 53, 45, 288, DateTimeKind.Local).AddTicks(4633) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 15,
                columns: new[] { "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { "ipsa", new DateTime(2020, 6, 14, 15, 53, 45, 288, DateTimeKind.Local).AddTicks(4674), 29, new DateTime(2020, 6, 14, 15, 53, 45, 288, DateTimeKind.Local).AddTicks(4681) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 16,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 17, @"Ut velit consequatur dolor soluta ut iusto eum quibusdam.
Tempora non ipsum id unde.", new DateTime(2020, 6, 14, 15, 53, 45, 288, DateTimeKind.Local).AddTicks(4804), 38, new DateTime(2020, 6, 14, 15, 53, 45, 288, DateTimeKind.Local).AddTicks(4813) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 17,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 4, "Sunt voluptas similique assumenda ex amet at voluptate.", new DateTime(2020, 6, 14, 15, 53, 45, 288, DateTimeKind.Local).AddTicks(4900), 29, new DateTime(2020, 6, 14, 15, 53, 45, 288, DateTimeKind.Local).AddTicks(4908) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 18,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 17, @"Ipsum vel porro quasi et repudiandae.
Aliquid voluptates reprehenderit odio.", new DateTime(2020, 6, 14, 15, 53, 45, 288, DateTimeKind.Local).AddTicks(5054), 33, new DateTime(2020, 6, 14, 15, 53, 45, 288, DateTimeKind.Local).AddTicks(5062) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 19,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 14, "qui", new DateTime(2020, 6, 14, 15, 53, 45, 288, DateTimeKind.Local).AddTicks(5106), 33, new DateTime(2020, 6, 14, 15, 53, 45, 288, DateTimeKind.Local).AddTicks(5114) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 20,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 19, "quidem", new DateTime(2020, 6, 14, 15, 53, 45, 288, DateTimeKind.Local).AddTicks(5154), 39, new DateTime(2020, 6, 14, 15, 53, 45, 288, DateTimeKind.Local).AddTicks(5161) });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 10, new DateTime(2020, 6, 14, 15, 53, 45, 86, DateTimeKind.Local).AddTicks(1974), "Lizeth.Schiller19@hotmail.com", "E1/9NquoSMd7mQJUBTMFwNt+tJdEeBzeHPclRVh/Sgg=", "+NgrviURBsp2GZdnqozlEm0WCdTdzsjIUo5zhCcKJtM=", new DateTime(2020, 6, 14, 15, 53, 45, 86, DateTimeKind.Local).AddTicks(2948), "Retta24" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 6, new DateTime(2020, 6, 14, 15, 53, 45, 96, DateTimeKind.Local).AddTicks(2152), "Garfield_Torphy27@yahoo.com", "Yu7E4IaW2PlttRU+RDDYTwfU9MZ9KTUeUvkpuM7DEkk=", "+DcAq7A7g9OzQFLIHN5Zk/fGCU5PmZ6mZks+XeE87uM=", new DateTime(2020, 6, 14, 15, 53, 45, 96, DateTimeKind.Local).AddTicks(2195), "Lois55" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 3, new DateTime(2020, 6, 14, 15, 53, 45, 105, DateTimeKind.Local).AddTicks(9126), "Tamia.Sporer@gmail.com", "sqnsqMCn4nH7aKu+f8Ov23XOsncOWL9pUA+A9+hJ9uU=", "Cvynr4Tj7nRe/8V1sCEy4pE3qrrOsvwkt93+bgCxK1s=", new DateTime(2020, 6, 14, 15, 53, 45, 105, DateTimeKind.Local).AddTicks(9144), "Katharina.Flatley" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 17, new DateTime(2020, 6, 14, 15, 53, 45, 115, DateTimeKind.Local).AddTicks(5928), "Celine32@yahoo.com", "qDKM4qqDYcYeDgm09yMS3241BaQp2iSuBruslrRjvAk=", "GMnzq80vVS9U3qh2y3b6EC9u/5mxycHSoUW+sr70zOo=", new DateTime(2020, 6, 14, 15, 53, 45, 115, DateTimeKind.Local).AddTicks(5944), "Dayana.Langworth" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 14, new DateTime(2020, 6, 14, 15, 53, 45, 125, DateTimeKind.Local).AddTicks(3168), "Jay_Luettgen11@hotmail.com", "r9T1ud3dAUN2jVdiOyj+vbi6gJz9PE4D8GCxZBbSgIU=", "NvgMPbsXs2PNy5irJ1tcN1pgHzAlvFmo0YFQ+30u7rw=", new DateTime(2020, 6, 14, 15, 53, 45, 125, DateTimeKind.Local).AddTicks(3186), "Ruby_Jacobs" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 6, new DateTime(2020, 6, 14, 15, 53, 45, 134, DateTimeKind.Local).AddTicks(9886), "Allen_DuBuque@hotmail.com", "mwa7M8IyXU4QJ1MMWSd5DbP54xVbGo6V3v8Mbe8yvaw=", "erV87zzxCiYgn9B3vqDfvBCLcWR7primbPcoRg14cww=", new DateTime(2020, 6, 14, 15, 53, 45, 134, DateTimeKind.Local).AddTicks(9902), "Madisen.Ferry47" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 7,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 15, new DateTime(2020, 6, 14, 15, 53, 45, 144, DateTimeKind.Local).AddTicks(6610), "Uriah.Harber87@hotmail.com", "jzKdsAng7LozfmLLEsfGDD4c7pbfOow3woFdf/ZKVfA=", "YoFUEvaLKkaYA5Tes1604vqWsriOJwvxcR3oWEmuLN0=", new DateTime(2020, 6, 14, 15, 53, 45, 144, DateTimeKind.Local).AddTicks(6630), "Marietta88" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 8,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 11, new DateTime(2020, 6, 14, 15, 53, 45, 155, DateTimeKind.Local).AddTicks(7911), "Kaycee4@hotmail.com", "L6An6FYI5K8a0+KOD/cLhjkD+Kz/FwN1R4DkTiccmW0=", "IboNSsVXIDPwusZcGY5uHzN0NrZbzFKQHUcpB61Qu5k=", new DateTime(2020, 6, 14, 15, 53, 45, 155, DateTimeKind.Local).AddTicks(7992), "Ulises73" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 9,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 16, new DateTime(2020, 6, 14, 15, 53, 45, 165, DateTimeKind.Local).AddTicks(5691), "Cameron_Abbott@yahoo.com", "n7K8fh9lG6piFRajfRaPk/2wtciVpdGpTSMGXQIPA2I=", "NuBwkHdvdlXClXFT8O1WcmfHbQukv0qTGB4tQm4WC9E=", new DateTime(2020, 6, 14, 15, 53, 45, 165, DateTimeKind.Local).AddTicks(5740), "Antonetta_Block41" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 10,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 17, new DateTime(2020, 6, 14, 15, 53, 45, 174, DateTimeKind.Local).AddTicks(9812), "Eloisa.Beahan@hotmail.com", "KE2U+nO6hUnU4/sxn2Hyetpp9+lMyZE/0af/McsRRxQ=", "9VnP3RMuiSQgMORUcGh6Htzo66NEvrGTvIgKEX36lbE=", new DateTime(2020, 6, 14, 15, 53, 45, 174, DateTimeKind.Local).AddTicks(9828), "Eliseo.Armstrong46" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 11,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 8, new DateTime(2020, 6, 14, 15, 53, 45, 184, DateTimeKind.Local).AddTicks(3195), "Christophe10@yahoo.com", "oh9ygqLGBd749dvE/mhMbWMz4aS3vuo7LCtrXlX0VR4=", "gBLgy5QQtrM1iMv+PfZjmDPXVBgD+ArrNUkFjHG9Mmc=", new DateTime(2020, 6, 14, 15, 53, 45, 184, DateTimeKind.Local).AddTicks(3217), "Leslie32" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 12,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 14, new DateTime(2020, 6, 14, 15, 53, 45, 193, DateTimeKind.Local).AddTicks(6169), "Marie98@yahoo.com", "EL0MItOMPF+Dqazds0lK/2ez39XVTfBjyv/s374bvjM=", "bDckvCKmWii7EO14jWE2ESHbqn1ys4r/3X0N3UUX+Ic=", new DateTime(2020, 6, 14, 15, 53, 45, 193, DateTimeKind.Local).AddTicks(6186), "Tina28" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 13,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 20, new DateTime(2020, 6, 14, 15, 53, 45, 202, DateTimeKind.Local).AddTicks(9864), "Dulce25@gmail.com", "LF3KxfwvExXAjdwGqVs7tDkfjt+Ics/Ng0RYTBJwOBM=", "jlZwNaJIfSSnK3aRqhEiXcOeW69FZS7GWAKLp25DHp4=", new DateTime(2020, 6, 14, 15, 53, 45, 202, DateTimeKind.Local).AddTicks(9883), "Bart_Schultz" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 14,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 15, new DateTime(2020, 6, 14, 15, 53, 45, 212, DateTimeKind.Local).AddTicks(3125), "Abigayle_Rice99@hotmail.com", "kDMUXIbljNCFGdYDfuTeOGJT94G6sLh+EABA8JjxIAY=", "tTaOnRnvv5FOJt+9KmmxnNPrR3c0FLhcmU9WF+/xDDw=", new DateTime(2020, 6, 14, 15, 53, 45, 212, DateTimeKind.Local).AddTicks(3148), "Kirsten.Cole63" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 15,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 10, new DateTime(2020, 6, 14, 15, 53, 45, 221, DateTimeKind.Local).AddTicks(6023), "Micaela.Kuhic@gmail.com", "ER7mmwpjUHJaC4CKTC+dLZTk1U1pmJ0mqcX2I13v6vE=", "iqH+2lLZq8Nn9t4C75KhXYCsocZRTwqQ3dDztdiso3w=", new DateTime(2020, 6, 14, 15, 53, 45, 221, DateTimeKind.Local).AddTicks(6039), "Missouri28" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 16,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 1, new DateTime(2020, 6, 14, 15, 53, 45, 230, DateTimeKind.Local).AddTicks(8986), "Dorthy54@yahoo.com", "Z2rz2NXj31beKNWYKJSAQfZmMjL0TDVFy6xRZSu+wdo=", "BCGhmUExvlhqWYE9P1EufFymG7bV5bS9Xy91LdQTYJs=", new DateTime(2020, 6, 14, 15, 53, 45, 230, DateTimeKind.Local).AddTicks(9002), "Lindsay.Huels36" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 17,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 8, new DateTime(2020, 6, 14, 15, 53, 45, 240, DateTimeKind.Local).AddTicks(2119), "Donato11@hotmail.com", "awlqBNAXRgXCUudfcJqAYqzhCHXbdfRnKJRAUNOiFfU=", "5joH4W+SOOOf2ibRit44oiFiEMctSbFG/qfu+XBUiAk=", new DateTime(2020, 6, 14, 15, 53, 45, 240, DateTimeKind.Local).AddTicks(2134), "Buster24" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 18,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 14, new DateTime(2020, 6, 14, 15, 53, 45, 249, DateTimeKind.Local).AddTicks(5487), "Katharina19@yahoo.com", "LsIbvBBQ5DBeBqk7bcz2IlA4IVWPywg0ocw8S75d/II=", "gWuGmSCD/sDnB3SJ4YXx7nGaqt5DqLv+xtQXMh2LeME=", new DateTime(2020, 6, 14, 15, 53, 45, 249, DateTimeKind.Local).AddTicks(5504), "Alanis.Barton" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 19,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 6, new DateTime(2020, 6, 14, 15, 53, 45, 259, DateTimeKind.Local).AddTicks(402), "Esperanza_Schiller23@gmail.com", "uQlQiWdjrY6GT8B6SFLu4Nk1n2Ev0LdpyhHDXk1dnLU=", "CRzTFNEpatdBSeW2d+UoWMt95u5jSsRHdQe+K3onG2U=", new DateTime(2020, 6, 14, 15, 53, 45, 259, DateTimeKind.Local).AddTicks(793), "Jay_Buckridge47" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 20,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 1, new DateTime(2020, 6, 14, 15, 53, 45, 268, DateTimeKind.Local).AddTicks(6482), "Trace_Kerluke@yahoo.com", "2G/YCa5kAcnNtYUplNZTPbWYtEfXsGtmQBaExVsfrjc=", "Nj8VGsH1dw+xcHlPpl72HjWD3hGcWgWy/uKtcJO0yFQ=", new DateTime(2020, 6, 14, 15, 53, 45, 268, DateTimeKind.Local).AddTicks(6861), "Marcelina_Luettgen" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 21,
                columns: new[] { "CreatedAt", "Password", "Salt", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 14, 15, 53, 45, 277, DateTimeKind.Local).AddTicks(3565), "h/XDDaAQaa2nxXHdgWAgT3EUzc/rnzJFrHK/K+6/zMQ=", "8Ppe1YeHb/8/lPqSnFtsK6xAwYWhmbq5g8DPDzqmUyo=", new DateTime(2020, 6, 14, 15, 53, 45, 277, DateTimeKind.Local).AddTicks(3565) });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 5);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 6);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 7);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 8);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 9);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 10);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 11);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 12);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 13);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 14);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 15);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 16);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 17);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 18);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 19);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 20);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 5);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 6);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 7);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 8);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 9);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 10);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 11);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 12);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 13);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 14);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 15);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 16);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 17);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 18);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 19);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 20);

            migrationBuilder.DropColumn(
                name: "IsDeleted",
                table: "Posts");

            migrationBuilder.InsertData(
                table: "CommentReactions",
                columns: new[] { "Id", "CommentId", "CreatedAt", "IsLike", "UpdatedAt", "UserId" },
                values: new object[,]
                {
                    { 20, 13, new DateTime(2020, 6, 6, 13, 4, 30, 496, DateTimeKind.Local).AddTicks(3948), false, new DateTime(2020, 6, 6, 13, 4, 30, 496, DateTimeKind.Local).AddTicks(3952), 8 },
                    { 2, 7, new DateTime(2020, 6, 6, 13, 4, 30, 496, DateTimeKind.Local).AddTicks(3456), true, new DateTime(2020, 6, 6, 13, 4, 30, 496, DateTimeKind.Local).AddTicks(3470), 2 },
                    { 3, 18, new DateTime(2020, 6, 6, 13, 4, 30, 496, DateTimeKind.Local).AddTicks(3500), true, new DateTime(2020, 6, 6, 13, 4, 30, 496, DateTimeKind.Local).AddTicks(3505), 10 },
                    { 4, 7, new DateTime(2020, 6, 6, 13, 4, 30, 496, DateTimeKind.Local).AddTicks(3526), true, new DateTime(2020, 6, 6, 13, 4, 30, 496, DateTimeKind.Local).AddTicks(3530), 10 },
                    { 5, 5, new DateTime(2020, 6, 6, 13, 4, 30, 496, DateTimeKind.Local).AddTicks(3551), true, new DateTime(2020, 6, 6, 13, 4, 30, 496, DateTimeKind.Local).AddTicks(3555), 17 },
                    { 6, 16, new DateTime(2020, 6, 6, 13, 4, 30, 496, DateTimeKind.Local).AddTicks(3576), false, new DateTime(2020, 6, 6, 13, 4, 30, 496, DateTimeKind.Local).AddTicks(3580), 11 },
                    { 7, 4, new DateTime(2020, 6, 6, 13, 4, 30, 496, DateTimeKind.Local).AddTicks(3601), false, new DateTime(2020, 6, 6, 13, 4, 30, 496, DateTimeKind.Local).AddTicks(3605), 16 },
                    { 8, 2, new DateTime(2020, 6, 6, 13, 4, 30, 496, DateTimeKind.Local).AddTicks(3626), true, new DateTime(2020, 6, 6, 13, 4, 30, 496, DateTimeKind.Local).AddTicks(3630), 7 },
                    { 9, 2, new DateTime(2020, 6, 6, 13, 4, 30, 496, DateTimeKind.Local).AddTicks(3649), true, new DateTime(2020, 6, 6, 13, 4, 30, 496, DateTimeKind.Local).AddTicks(3653), 10 },
                    { 10, 4, new DateTime(2020, 6, 6, 13, 4, 30, 496, DateTimeKind.Local).AddTicks(3673), true, new DateTime(2020, 6, 6, 13, 4, 30, 496, DateTimeKind.Local).AddTicks(3677), 21 },
                    { 1, 13, new DateTime(2020, 6, 6, 13, 4, 30, 496, DateTimeKind.Local).AddTicks(2487), false, new DateTime(2020, 6, 6, 13, 4, 30, 496, DateTimeKind.Local).AddTicks(3055), 3 },
                    { 12, 6, new DateTime(2020, 6, 6, 13, 4, 30, 496, DateTimeKind.Local).AddTicks(3721), false, new DateTime(2020, 6, 6, 13, 4, 30, 496, DateTimeKind.Local).AddTicks(3726), 4 },
                    { 13, 7, new DateTime(2020, 6, 6, 13, 4, 30, 496, DateTimeKind.Local).AddTicks(3745), true, new DateTime(2020, 6, 6, 13, 4, 30, 496, DateTimeKind.Local).AddTicks(3749), 3 },
                    { 14, 6, new DateTime(2020, 6, 6, 13, 4, 30, 496, DateTimeKind.Local).AddTicks(3769), true, new DateTime(2020, 6, 6, 13, 4, 30, 496, DateTimeKind.Local).AddTicks(3773), 21 },
                    { 15, 18, new DateTime(2020, 6, 6, 13, 4, 30, 496, DateTimeKind.Local).AddTicks(3793), true, new DateTime(2020, 6, 6, 13, 4, 30, 496, DateTimeKind.Local).AddTicks(3797), 11 },
                    { 16, 17, new DateTime(2020, 6, 6, 13, 4, 30, 496, DateTimeKind.Local).AddTicks(3818), false, new DateTime(2020, 6, 6, 13, 4, 30, 496, DateTimeKind.Local).AddTicks(3822), 9 },
                    { 17, 8, new DateTime(2020, 6, 6, 13, 4, 30, 496, DateTimeKind.Local).AddTicks(3843), false, new DateTime(2020, 6, 6, 13, 4, 30, 496, DateTimeKind.Local).AddTicks(3847), 15 },
                    { 18, 3, new DateTime(2020, 6, 6, 13, 4, 30, 496, DateTimeKind.Local).AddTicks(3866), true, new DateTime(2020, 6, 6, 13, 4, 30, 496, DateTimeKind.Local).AddTicks(3870), 14 },
                    { 19, 16, new DateTime(2020, 6, 6, 13, 4, 30, 496, DateTimeKind.Local).AddTicks(3923), false, new DateTime(2020, 6, 6, 13, 4, 30, 496, DateTimeKind.Local).AddTicks(3927), 16 },
                    { 11, 16, new DateTime(2020, 6, 6, 13, 4, 30, 496, DateTimeKind.Local).AddTicks(3696), true, new DateTime(2020, 6, 6, 13, 4, 30, 496, DateTimeKind.Local).AddTicks(3700), 10 }
                });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 16, "Doloremque nostrum voluptates velit ut enim voluptatem dignissimos ipsum.", new DateTime(2020, 6, 6, 13, 4, 30, 485, DateTimeKind.Local).AddTicks(735), 17, new DateTime(2020, 6, 6, 13, 4, 30, 485, DateTimeKind.Local).AddTicks(1357) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 4, "Libero facilis dolorem aut.", new DateTime(2020, 6, 6, 13, 4, 30, 485, DateTimeKind.Local).AddTicks(2214), 13, new DateTime(2020, 6, 6, 13, 4, 30, 485, DateTimeKind.Local).AddTicks(2231) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 15, "Rerum laudantium excepturi tempore voluptas fugit non maxime.", new DateTime(2020, 6, 6, 13, 4, 30, 485, DateTimeKind.Local).AddTicks(2308), 10, new DateTime(2020, 6, 6, 13, 4, 30, 485, DateTimeKind.Local).AddTicks(2314) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 14, "Sint et nemo sit eligendi ullam quas eos amet magni.", new DateTime(2020, 6, 6, 13, 4, 30, 485, DateTimeKind.Local).AddTicks(2380), 2, new DateTime(2020, 6, 6, 13, 4, 30, 485, DateTimeKind.Local).AddTicks(2385) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 2, "Delectus ut saepe nostrum fugit consectetur atque voluptas ab.", new DateTime(2020, 6, 6, 13, 4, 30, 485, DateTimeKind.Local).AddTicks(2446), 4, new DateTime(2020, 6, 6, 13, 4, 30, 485, DateTimeKind.Local).AddTicks(2450) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 12, "Sint dolorum culpa dicta autem.", new DateTime(2020, 6, 6, 13, 4, 30, 485, DateTimeKind.Local).AddTicks(2498), 8, new DateTime(2020, 6, 6, 13, 4, 30, 485, DateTimeKind.Local).AddTicks(2502) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 7,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 11, "Enim numquam eaque eveniet est hic accusamus totam enim dignissimos.", new DateTime(2020, 6, 6, 13, 4, 30, 485, DateTimeKind.Local).AddTicks(2609), 1, new DateTime(2020, 6, 6, 13, 4, 30, 485, DateTimeKind.Local).AddTicks(2614) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 8,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 14, "Minus nihil temporibus modi ut.", new DateTime(2020, 6, 6, 13, 4, 30, 485, DateTimeKind.Local).AddTicks(2662), 10, new DateTime(2020, 6, 6, 13, 4, 30, 485, DateTimeKind.Local).AddTicks(2667) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 9,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 3, "Magni laudantium porro.", new DateTime(2020, 6, 6, 13, 4, 30, 485, DateTimeKind.Local).AddTicks(2704), 14, new DateTime(2020, 6, 6, 13, 4, 30, 485, DateTimeKind.Local).AddTicks(2709) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 10,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 12, "Cum et omnis vel.", new DateTime(2020, 6, 6, 13, 4, 30, 485, DateTimeKind.Local).AddTicks(2751), 6, new DateTime(2020, 6, 6, 13, 4, 30, 485, DateTimeKind.Local).AddTicks(2755) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 11,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 14, "Molestias facere et repellat.", new DateTime(2020, 6, 6, 13, 4, 30, 485, DateTimeKind.Local).AddTicks(2799), 4, new DateTime(2020, 6, 6, 13, 4, 30, 485, DateTimeKind.Local).AddTicks(2804) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 12,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 21, "Sit blanditiis id sunt ipsum.", new DateTime(2020, 6, 6, 13, 4, 30, 485, DateTimeKind.Local).AddTicks(2848), 16, new DateTime(2020, 6, 6, 13, 4, 30, 485, DateTimeKind.Local).AddTicks(2852) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 13,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 21, "Autem et qui non officiis tenetur eos placeat quia omnis.", new DateTime(2020, 6, 6, 13, 4, 30, 485, DateTimeKind.Local).AddTicks(2913), 16, new DateTime(2020, 6, 6, 13, 4, 30, 485, DateTimeKind.Local).AddTicks(2918) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 14,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 16, "Perspiciatis provident harum rerum.", new DateTime(2020, 6, 6, 13, 4, 30, 485, DateTimeKind.Local).AddTicks(2985), 5, new DateTime(2020, 6, 6, 13, 4, 30, 485, DateTimeKind.Local).AddTicks(3144) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 15,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 10, "Tenetur quis modi officia.", new DateTime(2020, 6, 6, 13, 4, 30, 485, DateTimeKind.Local).AddTicks(3183), 4, new DateTime(2020, 6, 6, 13, 4, 30, 485, DateTimeKind.Local).AddTicks(3187) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 16,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 14, "Tempore vero qui hic quo reprehenderit omnis consequuntur repudiandae libero.", new DateTime(2020, 6, 6, 13, 4, 30, 485, DateTimeKind.Local).AddTicks(3245), 9, new DateTime(2020, 6, 6, 13, 4, 30, 485, DateTimeKind.Local).AddTicks(3249) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 17,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 15, "Consequatur alias dolor eius.", new DateTime(2020, 6, 6, 13, 4, 30, 485, DateTimeKind.Local).AddTicks(3287), 8, new DateTime(2020, 6, 6, 13, 4, 30, 485, DateTimeKind.Local).AddTicks(3291) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 18,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 10, "Rem molestiae ut et.", new DateTime(2020, 6, 6, 13, 4, 30, 485, DateTimeKind.Local).AddTicks(3329), 15, new DateTime(2020, 6, 6, 13, 4, 30, 485, DateTimeKind.Local).AddTicks(3333) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 19,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 10, "Sapiente molestias rerum omnis totam porro ut.", new DateTime(2020, 6, 6, 13, 4, 30, 485, DateTimeKind.Local).AddTicks(3379), 18, new DateTime(2020, 6, 6, 13, 4, 30, 485, DateTimeKind.Local).AddTicks(3383) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 20,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 9, "Eum aliquam eum molestiae.", new DateTime(2020, 6, 6, 13, 4, 30, 485, DateTimeKind.Local).AddTicks(3491), 13, new DateTime(2020, 6, 6, 13, 4, 30, 485, DateTimeKind.Local).AddTicks(3496) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 6, 13, 4, 30, 299, DateTimeKind.Local).AddTicks(5388), "https://s3.amazonaws.com/uifaces/faces/twitter/vanchesz/128.jpg", new DateTime(2020, 6, 6, 13, 4, 30, 299, DateTimeKind.Local).AddTicks(9171) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 6, 13, 4, 30, 299, DateTimeKind.Local).AddTicks(9852), "https://s3.amazonaws.com/uifaces/faces/twitter/jayphen/128.jpg", new DateTime(2020, 6, 6, 13, 4, 30, 299, DateTimeKind.Local).AddTicks(9876) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 6, 13, 4, 30, 299, DateTimeKind.Local).AddTicks(9904), "https://s3.amazonaws.com/uifaces/faces/twitter/bublienko/128.jpg", new DateTime(2020, 6, 6, 13, 4, 30, 299, DateTimeKind.Local).AddTicks(9909) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 6, 13, 4, 30, 299, DateTimeKind.Local).AddTicks(9929), "https://s3.amazonaws.com/uifaces/faces/twitter/baires/128.jpg", new DateTime(2020, 6, 6, 13, 4, 30, 299, DateTimeKind.Local).AddTicks(9933) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 6, 13, 4, 30, 299, DateTimeKind.Local).AddTicks(9950), "https://s3.amazonaws.com/uifaces/faces/twitter/borges_marcos/128.jpg", new DateTime(2020, 6, 6, 13, 4, 30, 299, DateTimeKind.Local).AddTicks(9954) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 6, 13, 4, 30, 299, DateTimeKind.Local).AddTicks(9971), "https://s3.amazonaws.com/uifaces/faces/twitter/okansurreel/128.jpg", new DateTime(2020, 6, 6, 13, 4, 30, 299, DateTimeKind.Local).AddTicks(9976) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 7,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 6, 13, 4, 30, 299, DateTimeKind.Local).AddTicks(9992), "https://s3.amazonaws.com/uifaces/faces/twitter/muridrahhal/128.jpg", new DateTime(2020, 6, 6, 13, 4, 30, 299, DateTimeKind.Local).AddTicks(9996) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 8,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 6, 13, 4, 30, 300, DateTimeKind.Local).AddTicks(13), "https://s3.amazonaws.com/uifaces/faces/twitter/ryhanhassan/128.jpg", new DateTime(2020, 6, 6, 13, 4, 30, 300, DateTimeKind.Local).AddTicks(18) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 9,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 6, 13, 4, 30, 300, DateTimeKind.Local).AddTicks(34), "https://s3.amazonaws.com/uifaces/faces/twitter/markjenkins/128.jpg", new DateTime(2020, 6, 6, 13, 4, 30, 300, DateTimeKind.Local).AddTicks(38) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 10,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 6, 13, 4, 30, 300, DateTimeKind.Local).AddTicks(54), "https://s3.amazonaws.com/uifaces/faces/twitter/ninjad3m0/128.jpg", new DateTime(2020, 6, 6, 13, 4, 30, 300, DateTimeKind.Local).AddTicks(58) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 11,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 6, 13, 4, 30, 300, DateTimeKind.Local).AddTicks(75), "https://s3.amazonaws.com/uifaces/faces/twitter/aaronalfred/128.jpg", new DateTime(2020, 6, 6, 13, 4, 30, 300, DateTimeKind.Local).AddTicks(79) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 12,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 6, 13, 4, 30, 300, DateTimeKind.Local).AddTicks(95), "https://s3.amazonaws.com/uifaces/faces/twitter/victorquinn/128.jpg", new DateTime(2020, 6, 6, 13, 4, 30, 300, DateTimeKind.Local).AddTicks(99) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 13,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 6, 13, 4, 30, 300, DateTimeKind.Local).AddTicks(117), "https://s3.amazonaws.com/uifaces/faces/twitter/dmackerman/128.jpg", new DateTime(2020, 6, 6, 13, 4, 30, 300, DateTimeKind.Local).AddTicks(121) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 14,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 6, 13, 4, 30, 300, DateTimeKind.Local).AddTicks(142), "https://s3.amazonaws.com/uifaces/faces/twitter/ifarafonow/128.jpg", new DateTime(2020, 6, 6, 13, 4, 30, 300, DateTimeKind.Local).AddTicks(146) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 15,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 6, 13, 4, 30, 300, DateTimeKind.Local).AddTicks(162), "https://s3.amazonaws.com/uifaces/faces/twitter/josep_martins/128.jpg", new DateTime(2020, 6, 6, 13, 4, 30, 300, DateTimeKind.Local).AddTicks(166) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 16,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 6, 13, 4, 30, 300, DateTimeKind.Local).AddTicks(182), "https://s3.amazonaws.com/uifaces/faces/twitter/runningskull/128.jpg", new DateTime(2020, 6, 6, 13, 4, 30, 300, DateTimeKind.Local).AddTicks(186) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 17,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 6, 13, 4, 30, 300, DateTimeKind.Local).AddTicks(202), "https://s3.amazonaws.com/uifaces/faces/twitter/michzen/128.jpg", new DateTime(2020, 6, 6, 13, 4, 30, 300, DateTimeKind.Local).AddTicks(206) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 18,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 6, 13, 4, 30, 300, DateTimeKind.Local).AddTicks(223), "https://s3.amazonaws.com/uifaces/faces/twitter/martip07/128.jpg", new DateTime(2020, 6, 6, 13, 4, 30, 300, DateTimeKind.Local).AddTicks(227) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 19,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 6, 13, 4, 30, 300, DateTimeKind.Local).AddTicks(244), "https://s3.amazonaws.com/uifaces/faces/twitter/missaaamy/128.jpg", new DateTime(2020, 6, 6, 13, 4, 30, 300, DateTimeKind.Local).AddTicks(248) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 20,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 6, 13, 4, 30, 300, DateTimeKind.Local).AddTicks(264), "https://s3.amazonaws.com/uifaces/faces/twitter/simobenso/128.jpg", new DateTime(2020, 6, 6, 13, 4, 30, 300, DateTimeKind.Local).AddTicks(268) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 21,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 6, 13, 4, 30, 304, DateTimeKind.Local).AddTicks(2600), "https://picsum.photos/640/480/?image=545", new DateTime(2020, 6, 6, 13, 4, 30, 304, DateTimeKind.Local).AddTicks(3227) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 22,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 6, 13, 4, 30, 304, DateTimeKind.Local).AddTicks(3524), "https://picsum.photos/640/480/?image=1081", new DateTime(2020, 6, 6, 13, 4, 30, 304, DateTimeKind.Local).AddTicks(3550) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 23,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 6, 13, 4, 30, 304, DateTimeKind.Local).AddTicks(3572), "https://picsum.photos/640/480/?image=938", new DateTime(2020, 6, 6, 13, 4, 30, 304, DateTimeKind.Local).AddTicks(3577) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 24,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 6, 13, 4, 30, 304, DateTimeKind.Local).AddTicks(3594), "https://picsum.photos/640/480/?image=25", new DateTime(2020, 6, 6, 13, 4, 30, 304, DateTimeKind.Local).AddTicks(3598) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 25,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 6, 13, 4, 30, 304, DateTimeKind.Local).AddTicks(3615), "https://picsum.photos/640/480/?image=1008", new DateTime(2020, 6, 6, 13, 4, 30, 304, DateTimeKind.Local).AddTicks(3619) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 26,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 6, 13, 4, 30, 304, DateTimeKind.Local).AddTicks(3635), "https://picsum.photos/640/480/?image=706", new DateTime(2020, 6, 6, 13, 4, 30, 304, DateTimeKind.Local).AddTicks(3639) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 27,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 6, 13, 4, 30, 304, DateTimeKind.Local).AddTicks(3655), "https://picsum.photos/640/480/?image=268", new DateTime(2020, 6, 6, 13, 4, 30, 304, DateTimeKind.Local).AddTicks(3659) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 28,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 6, 13, 4, 30, 304, DateTimeKind.Local).AddTicks(3675), "https://picsum.photos/640/480/?image=789", new DateTime(2020, 6, 6, 13, 4, 30, 304, DateTimeKind.Local).AddTicks(3679) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 29,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 6, 13, 4, 30, 304, DateTimeKind.Local).AddTicks(3695), "https://picsum.photos/640/480/?image=238", new DateTime(2020, 6, 6, 13, 4, 30, 304, DateTimeKind.Local).AddTicks(3698) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 30,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 6, 13, 4, 30, 304, DateTimeKind.Local).AddTicks(3715), "https://picsum.photos/640/480/?image=596", new DateTime(2020, 6, 6, 13, 4, 30, 304, DateTimeKind.Local).AddTicks(3718) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 31,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 6, 13, 4, 30, 304, DateTimeKind.Local).AddTicks(3734), "https://picsum.photos/640/480/?image=410", new DateTime(2020, 6, 6, 13, 4, 30, 304, DateTimeKind.Local).AddTicks(3738) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 32,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 6, 13, 4, 30, 304, DateTimeKind.Local).AddTicks(3754), "https://picsum.photos/640/480/?image=85", new DateTime(2020, 6, 6, 13, 4, 30, 304, DateTimeKind.Local).AddTicks(3758) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 33,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 6, 13, 4, 30, 304, DateTimeKind.Local).AddTicks(3801), "https://picsum.photos/640/480/?image=1059", new DateTime(2020, 6, 6, 13, 4, 30, 304, DateTimeKind.Local).AddTicks(3805) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 34,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 6, 13, 4, 30, 304, DateTimeKind.Local).AddTicks(3823), "https://picsum.photos/640/480/?image=655", new DateTime(2020, 6, 6, 13, 4, 30, 304, DateTimeKind.Local).AddTicks(3827) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 35,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 6, 13, 4, 30, 304, DateTimeKind.Local).AddTicks(3843), "https://picsum.photos/640/480/?image=169", new DateTime(2020, 6, 6, 13, 4, 30, 304, DateTimeKind.Local).AddTicks(3847) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 36,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 6, 13, 4, 30, 304, DateTimeKind.Local).AddTicks(3862), "https://picsum.photos/640/480/?image=279", new DateTime(2020, 6, 6, 13, 4, 30, 304, DateTimeKind.Local).AddTicks(3866) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 37,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 6, 13, 4, 30, 304, DateTimeKind.Local).AddTicks(3882), "https://picsum.photos/640/480/?image=81", new DateTime(2020, 6, 6, 13, 4, 30, 304, DateTimeKind.Local).AddTicks(3885) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 38,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 6, 13, 4, 30, 304, DateTimeKind.Local).AddTicks(3901), "https://picsum.photos/640/480/?image=617", new DateTime(2020, 6, 6, 13, 4, 30, 304, DateTimeKind.Local).AddTicks(3904) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 39,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 6, 13, 4, 30, 304, DateTimeKind.Local).AddTicks(3920), "https://picsum.photos/640/480/?image=65", new DateTime(2020, 6, 6, 13, 4, 30, 304, DateTimeKind.Local).AddTicks(3924) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 40,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 6, 13, 4, 30, 304, DateTimeKind.Local).AddTicks(3940), "https://picsum.photos/640/480/?image=1060", new DateTime(2020, 6, 6, 13, 4, 30, 304, DateTimeKind.Local).AddTicks(3944) });

            migrationBuilder.InsertData(
                table: "PostReactions",
                columns: new[] { "Id", "CreatedAt", "IsLike", "PostId", "UpdatedAt", "UserId" },
                values: new object[,]
                {
                    { 10, new DateTime(2020, 6, 6, 13, 4, 30, 491, DateTimeKind.Local).AddTicks(2528), false, 9, new DateTime(2020, 6, 6, 13, 4, 30, 491, DateTimeKind.Local).AddTicks(2532), 9 },
                    { 11, new DateTime(2020, 6, 6, 13, 4, 30, 491, DateTimeKind.Local).AddTicks(2552), true, 2, new DateTime(2020, 6, 6, 13, 4, 30, 491, DateTimeKind.Local).AddTicks(2557), 15 },
                    { 12, new DateTime(2020, 6, 6, 13, 4, 30, 491, DateTimeKind.Local).AddTicks(2577), true, 7, new DateTime(2020, 6, 6, 13, 4, 30, 491, DateTimeKind.Local).AddTicks(2581), 1 },
                    { 13, new DateTime(2020, 6, 6, 13, 4, 30, 491, DateTimeKind.Local).AddTicks(2602), true, 14, new DateTime(2020, 6, 6, 13, 4, 30, 491, DateTimeKind.Local).AddTicks(2605), 14 },
                    { 15, new DateTime(2020, 6, 6, 13, 4, 30, 491, DateTimeKind.Local).AddTicks(2650), false, 14, new DateTime(2020, 6, 6, 13, 4, 30, 491, DateTimeKind.Local).AddTicks(2654), 9 },
                    { 18, new DateTime(2020, 6, 6, 13, 4, 30, 491, DateTimeKind.Local).AddTicks(2724), false, 16, new DateTime(2020, 6, 6, 13, 4, 30, 491, DateTimeKind.Local).AddTicks(2728), 14 },
                    { 16, new DateTime(2020, 6, 6, 13, 4, 30, 491, DateTimeKind.Local).AddTicks(2676), true, 1, new DateTime(2020, 6, 6, 13, 4, 30, 491, DateTimeKind.Local).AddTicks(2680), 16 },
                    { 17, new DateTime(2020, 6, 6, 13, 4, 30, 491, DateTimeKind.Local).AddTicks(2700), false, 6, new DateTime(2020, 6, 6, 13, 4, 30, 491, DateTimeKind.Local).AddTicks(2704), 7 },
                    { 9, new DateTime(2020, 6, 6, 13, 4, 30, 491, DateTimeKind.Local).AddTicks(2500), true, 3, new DateTime(2020, 6, 6, 13, 4, 30, 491, DateTimeKind.Local).AddTicks(2505), 19 },
                    { 20, new DateTime(2020, 6, 6, 13, 4, 30, 491, DateTimeKind.Local).AddTicks(2773), false, 20, new DateTime(2020, 6, 6, 13, 4, 30, 491, DateTimeKind.Local).AddTicks(2777), 19 },
                    { 19, new DateTime(2020, 6, 6, 13, 4, 30, 491, DateTimeKind.Local).AddTicks(2749), true, 3, new DateTime(2020, 6, 6, 13, 4, 30, 491, DateTimeKind.Local).AddTicks(2752), 18 },
                    { 14, new DateTime(2020, 6, 6, 13, 4, 30, 491, DateTimeKind.Local).AddTicks(2626), true, 13, new DateTime(2020, 6, 6, 13, 4, 30, 491, DateTimeKind.Local).AddTicks(2630), 9 },
                    { 8, new DateTime(2020, 6, 6, 13, 4, 30, 491, DateTimeKind.Local).AddTicks(2439), true, 14, new DateTime(2020, 6, 6, 13, 4, 30, 491, DateTimeKind.Local).AddTicks(2443), 4 },
                    { 3, new DateTime(2020, 6, 6, 13, 4, 30, 491, DateTimeKind.Local).AddTicks(2311), true, 19, new DateTime(2020, 6, 6, 13, 4, 30, 491, DateTimeKind.Local).AddTicks(2316), 1 },
                    { 6, new DateTime(2020, 6, 6, 13, 4, 30, 491, DateTimeKind.Local).AddTicks(2389), false, 17, new DateTime(2020, 6, 6, 13, 4, 30, 491, DateTimeKind.Local).AddTicks(2392), 21 },
                    { 5, new DateTime(2020, 6, 6, 13, 4, 30, 491, DateTimeKind.Local).AddTicks(2364), false, 11, new DateTime(2020, 6, 6, 13, 4, 30, 491, DateTimeKind.Local).AddTicks(2368), 19 },
                    { 1, new DateTime(2020, 6, 6, 13, 4, 30, 491, DateTimeKind.Local).AddTicks(1272), false, 7, new DateTime(2020, 6, 6, 13, 4, 30, 491, DateTimeKind.Local).AddTicks(1815), 7 },
                    { 4, new DateTime(2020, 6, 6, 13, 4, 30, 491, DateTimeKind.Local).AddTicks(2338), true, 11, new DateTime(2020, 6, 6, 13, 4, 30, 491, DateTimeKind.Local).AddTicks(2343), 1 },
                    { 2, new DateTime(2020, 6, 6, 13, 4, 30, 491, DateTimeKind.Local).AddTicks(2261), true, 19, new DateTime(2020, 6, 6, 13, 4, 30, 491, DateTimeKind.Local).AddTicks(2276), 19 },
                    { 7, new DateTime(2020, 6, 6, 13, 4, 30, 491, DateTimeKind.Local).AddTicks(2414), false, 6, new DateTime(2020, 6, 6, 13, 4, 30, 491, DateTimeKind.Local).AddTicks(2418), 13 }
                });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 4, "delectus", new DateTime(2020, 6, 6, 13, 4, 30, 478, DateTimeKind.Local).AddTicks(5616), 31, new DateTime(2020, 6, 6, 13, 4, 30, 478, DateTimeKind.Local).AddTicks(6148) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 10, @"Id quos suscipit debitis ducimus excepturi accusantium.
Laboriosam dolorem modi repellendus cum quidem.
Quia consectetur facilis.
Dolore quasi ab odio error.
Qui assumenda necessitatibus minima tempora et facere.", new DateTime(2020, 6, 6, 13, 4, 30, 479, DateTimeKind.Local).AddTicks(2437), 35, new DateTime(2020, 6, 6, 13, 4, 30, 479, DateTimeKind.Local).AddTicks(2464) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 6, "Non eaque voluptatibus quo aut cumque non eius ipsum.", new DateTime(2020, 6, 6, 13, 4, 30, 479, DateTimeKind.Local).AddTicks(3184), 22, new DateTime(2020, 6, 6, 13, 4, 30, 479, DateTimeKind.Local).AddTicks(3198) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 7, "eos", new DateTime(2020, 6, 6, 13, 4, 30, 479, DateTimeKind.Local).AddTicks(3242), 36, new DateTime(2020, 6, 6, 13, 4, 30, 479, DateTimeKind.Local).AddTicks(3247) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 18, @"Iusto nobis illo assumenda ex fuga.
Dolores magni aut sapiente totam beatae non neque.
Et sequi distinctio excepturi odio.", new DateTime(2020, 6, 6, 13, 4, 30, 479, DateTimeKind.Local).AddTicks(3480), 28, new DateTime(2020, 6, 6, 13, 4, 30, 479, DateTimeKind.Local).AddTicks(3486) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 12, "reprehenderit", new DateTime(2020, 6, 6, 13, 4, 30, 479, DateTimeKind.Local).AddTicks(3519), 30, new DateTime(2020, 6, 6, 13, 4, 30, 479, DateTimeKind.Local).AddTicks(3523) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 7,
                columns: new[] { "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { @"Ut ex non ut doloremque temporibus.
Nesciunt soluta ea explicabo libero sunt aspernatur aliquam.
Voluptatibus quae repellat debitis unde ipsum nulla quo.
Et nulla fugiat.
Saepe inventore eos iure facilis non.", new DateTime(2020, 6, 6, 13, 4, 30, 479, DateTimeKind.Local).AddTicks(3728), 33, new DateTime(2020, 6, 6, 13, 4, 30, 479, DateTimeKind.Local).AddTicks(3734) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 8,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 21, "Excepturi vitae rem corporis quam assumenda. Maxime delectus eligendi atque sint est officiis recusandae minus. Illum sed sapiente earum sint dolor temporibus.", new DateTime(2020, 6, 6, 13, 4, 30, 479, DateTimeKind.Local).AddTicks(4767), 26, new DateTime(2020, 6, 6, 13, 4, 30, 479, DateTimeKind.Local).AddTicks(4780) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 9,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 6, "Necessitatibus est omnis eos et dolorum commodi laudantium optio perspiciatis.", new DateTime(2020, 6, 6, 13, 4, 30, 479, DateTimeKind.Local).AddTicks(4886), 26, new DateTime(2020, 6, 6, 13, 4, 30, 479, DateTimeKind.Local).AddTicks(4891) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 10,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 12, "Accusamus nobis velit.", new DateTime(2020, 6, 6, 13, 4, 30, 479, DateTimeKind.Local).AddTicks(4933), 31, new DateTime(2020, 6, 6, 13, 4, 30, 479, DateTimeKind.Local).AddTicks(4938) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 11,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 4, @"Aspernatur consequuntur rerum eius et mollitia.
Nobis alias laudantium corrupti accusamus.
Ipsam non nisi est doloribus molestias totam aperiam delectus.
Itaque est voluptas ab explicabo autem possimus.
Voluptas numquam velit omnis harum dolores.", new DateTime(2020, 6, 6, 13, 4, 30, 479, DateTimeKind.Local).AddTicks(5154), 33, new DateTime(2020, 6, 6, 13, 4, 30, 479, DateTimeKind.Local).AddTicks(5160) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 12,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 15, "Ut eum est. Dolore omnis facere et saepe ut. Et dignissimos dolores.", new DateTime(2020, 6, 6, 13, 4, 30, 479, DateTimeKind.Local).AddTicks(5247), 33, new DateTime(2020, 6, 6, 13, 4, 30, 479, DateTimeKind.Local).AddTicks(5252) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 13,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 1, @"Quia nemo dolore aperiam est expedita ducimus.
Ullam nulla alias et maxime corporis voluptatem nihil eligendi.
Quis mollitia dolores nisi.
Eos necessitatibus omnis ut delectus.", new DateTime(2020, 6, 6, 13, 4, 30, 479, DateTimeKind.Local).AddTicks(5442), 29, new DateTime(2020, 6, 6, 13, 4, 30, 479, DateTimeKind.Local).AddTicks(5447) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 14,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 8, "Eius a nihil exercitationem autem ab placeat. Temporibus blanditiis modi saepe id voluptatem itaque quam impedit. Sed et asperiores ab et repellat eum qui delectus nobis.", new DateTime(2020, 6, 6, 13, 4, 30, 479, DateTimeKind.Local).AddTicks(5573), 30, new DateTime(2020, 6, 6, 13, 4, 30, 479, DateTimeKind.Local).AddTicks(5578) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 15,
                columns: new[] { "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { "Aut quia cupiditate.", new DateTime(2020, 6, 6, 13, 4, 30, 479, DateTimeKind.Local).AddTicks(5619), 27, new DateTime(2020, 6, 6, 13, 4, 30, 479, DateTimeKind.Local).AddTicks(5624) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 16,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 12, "ut", new DateTime(2020, 6, 6, 13, 4, 30, 479, DateTimeKind.Local).AddTicks(5650), 23, new DateTime(2020, 6, 6, 13, 4, 30, 479, DateTimeKind.Local).AddTicks(5654) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 17,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 8, "Sunt delectus sequi dolores doloremque unde impedit quia voluptatem debitis. Dolorem explicabo nihil voluptates est dicta et. Ducimus consectetur ipsum perferendis provident. Deserunt aut qui consequatur quam nihil velit nisi. Voluptates esse doloribus totam et recusandae harum qui repellendus consequatur.", new DateTime(2020, 6, 6, 13, 4, 30, 479, DateTimeKind.Local).AddTicks(5910), 33, new DateTime(2020, 6, 6, 13, 4, 30, 479, DateTimeKind.Local).AddTicks(5916) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 18,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 21, "Numquam magnam doloremque rem voluptatum quaerat eligendi.", new DateTime(2020, 6, 6, 13, 4, 30, 479, DateTimeKind.Local).AddTicks(5992), 39, new DateTime(2020, 6, 6, 13, 4, 30, 479, DateTimeKind.Local).AddTicks(5997) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 19,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 5, @"Ducimus consequuntur fugit nulla ipsum ea.
Fugit quibusdam quos distinctio et velit iure illum quae.
Consequatur non vel laboriosam corrupti consequatur.
Iure incidunt quisquam dicta quis asperiores laudantium consequatur impedit.
Corrupti molestias accusantium earum.", new DateTime(2020, 6, 6, 13, 4, 30, 479, DateTimeKind.Local).AddTicks(6175), 37, new DateTime(2020, 6, 6, 13, 4, 30, 479, DateTimeKind.Local).AddTicks(6181) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 20,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 18, @"Est officia esse veritatis sunt odio facilis provident hic.
Vel aspernatur sapiente omnis ea blanditiis itaque nobis.", new DateTime(2020, 6, 6, 13, 4, 30, 479, DateTimeKind.Local).AddTicks(6270), 35, new DateTime(2020, 6, 6, 13, 4, 30, 479, DateTimeKind.Local).AddTicks(6275) });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 1, new DateTime(2020, 6, 6, 13, 4, 30, 333, DateTimeKind.Local).AddTicks(4904), "Ralph37@hotmail.com", "2bv68z9XzyeyRdJcn/6wVhwxx/urD8hX9RKYAmw6qpg=", "/WagXrlo1qQDaRWDcU9+QUBRXscHZpFT/SoGEaGBFb0=", new DateTime(2020, 6, 6, 13, 4, 30, 333, DateTimeKind.Local).AddTicks(5574), "Sylvan_Senger94" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 14, new DateTime(2020, 6, 6, 13, 4, 30, 340, DateTimeKind.Local).AddTicks(83), "Tyson.Cole94@hotmail.com", "p0rpNFnPQqngLo1gao9xHkk5Mhv8f+83Z8XKLIuJ2d4=", "dffdTkgKK7xo3kmUpuM4bcWJ1pKXRZ4LYxHXB01qlEA=", new DateTime(2020, 6, 6, 13, 4, 30, 340, DateTimeKind.Local).AddTicks(114), "Roman46" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 2, new DateTime(2020, 6, 6, 13, 4, 30, 347, DateTimeKind.Local).AddTicks(9312), "Norris78@hotmail.com", "3aGnqaG/KbPOFjGp7yoyP+p/PPVzqpuFTkhe+YPzWgI=", "2NJ62U//qvIn/PUGdIKmq6OjaXKgCooXnqoHB0J9P/o=", new DateTime(2020, 6, 6, 13, 4, 30, 347, DateTimeKind.Local).AddTicks(9391), "Horace.Swift77" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 4, new DateTime(2020, 6, 6, 13, 4, 30, 358, DateTimeKind.Local).AddTicks(5448), "Lulu77@gmail.com", "mdSkYUGtRLEXr9nfQTvas5977C8xBBwpkccr9nydDyg=", "PGreumkHftkrjZYa6xJytfuRR62ZaqmHOlH7yNpJQiA=", new DateTime(2020, 6, 6, 13, 4, 30, 358, DateTimeKind.Local).AddTicks(5511), "Anna_Hagenes" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 18, new DateTime(2020, 6, 6, 13, 4, 30, 364, DateTimeKind.Local).AddTicks(9530), "Cayla_Murphy@yahoo.com", "crWevl2Z7Qx0ltUDSdLQSgIFvdFXFJh6c1Fj7jVnpCA=", "PM1abgkpreO6fQN8Z4aSKn2RG5J9ZaNYsfuD1shB3RA=", new DateTime(2020, 6, 6, 13, 4, 30, 364, DateTimeKind.Local).AddTicks(9569), "Carlie.Lindgren7" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 7, new DateTime(2020, 6, 6, 13, 4, 30, 371, DateTimeKind.Local).AddTicks(3496), "Sylvia.Harber73@yahoo.com", "GX+yjHDbGuj9bIkuA5OMZEgVN/IvWsg7zGbh8xGSaj0=", "kve5z0DQ+u2pVkQfP7qXdwFybpdHhOsGfi2pnZ+BsdY=", new DateTime(2020, 6, 6, 13, 4, 30, 371, DateTimeKind.Local).AddTicks(3511), "Valentine.Nicolas98" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 7,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 10, new DateTime(2020, 6, 6, 13, 4, 30, 377, DateTimeKind.Local).AddTicks(9577), "Nellie_Padberg77@gmail.com", "PS9s7SWK+mIDf6A4dyjzJ2WjtkRm6kYqFHJQ0hIazTU=", "vVK+FDJPDBmUcgbJPcaFFh2vXQoYQbxbZglZY5yN+Oo=", new DateTime(2020, 6, 6, 13, 4, 30, 377, DateTimeKind.Local).AddTicks(9626), "Modesto_Hagenes6" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 8,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 10, new DateTime(2020, 6, 6, 13, 4, 30, 384, DateTimeKind.Local).AddTicks(2818), "Garfield.Kilback@hotmail.com", "nk7DKaEkBQIrDi/JOB4bRmoumVORMq9FTXkf0o5A1jM=", "4wc5KIz58/Acvc+SSoxqaduyiSnGO4QVyLUb9ZRnx60=", new DateTime(2020, 6, 6, 13, 4, 30, 384, DateTimeKind.Local).AddTicks(2849), "Savanna38" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 9,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 3, new DateTime(2020, 6, 6, 13, 4, 30, 390, DateTimeKind.Local).AddTicks(5110), "Elmer_Morar@hotmail.com", "ILAcLyo6ANeqZQ61bmCVH1g7HzUlW4jOn6PzSI4Vxf0=", "MEy+m1LN+Dtd1mq5N+O+plKZz9a9nU2wdoEGXsZhjwM=", new DateTime(2020, 6, 6, 13, 4, 30, 390, DateTimeKind.Local).AddTicks(5147), "Lea.Fritsch79" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 10,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 4, new DateTime(2020, 6, 6, 13, 4, 30, 396, DateTimeKind.Local).AddTicks(6563), "Larue62@gmail.com", "c7xzvwf3pT4NnI7sO3YcHPFHQ1Jc42opJGhVyjHkpeQ=", "n6ysL6uRvI2GWkz8KihtOyBFji2uwNSNwHAajgLElZE=", new DateTime(2020, 6, 6, 13, 4, 30, 396, DateTimeKind.Local).AddTicks(6599), "Elmira_Haley54" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 11,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 3, new DateTime(2020, 6, 6, 13, 4, 30, 402, DateTimeKind.Local).AddTicks(6049), "Shane_Hauck@hotmail.com", "v5nX5dde8HMJFi3u2oi80bxLETGsJ9fsZS3bMdtRyzs=", "YnxfN73x9YuC3LJ7xq/sWfrNCs1l2xGnDqT/ty+z4E8=", new DateTime(2020, 6, 6, 13, 4, 30, 402, DateTimeKind.Local).AddTicks(6066), "Doyle_Barton" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 12,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 10, new DateTime(2020, 6, 6, 13, 4, 30, 408, DateTimeKind.Local).AddTicks(6000), "Judy.Lemke66@gmail.com", "oGHUgzNXZnyQnYmAK5UPhG0jlcNBM6XIuhs5RRTKDTw=", "eEwA9F7R0uJvShAGSAw+8SHc5AfWb2t8OQxRDiFj+C4=", new DateTime(2020, 6, 6, 13, 4, 30, 408, DateTimeKind.Local).AddTicks(6029), "Louie15" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 13,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 12, new DateTime(2020, 6, 6, 13, 4, 30, 414, DateTimeKind.Local).AddTicks(8163), "Johnny_Mills@hotmail.com", "nBZqRzHEbxD0FKd5q/gvzSbCufbQCG6mLs0pYYams94=", "w8wQwDJEbPmh6yt0oWLrigIEa2gnD8WrCOgTY+7Z1ts=", new DateTime(2020, 6, 6, 13, 4, 30, 414, DateTimeKind.Local).AddTicks(8216), "Viviane.Stokes" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 14,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 4, new DateTime(2020, 6, 6, 13, 4, 30, 420, DateTimeKind.Local).AddTicks(8757), "Cristian52@hotmail.com", "CeqVpPI2PHu+E/pf6cJKwCmLngzbwJ1LyuzjdOYLmmU=", "fSeDFPifxzlEAwmQjLsjA6JRPn4qjreD5QDBFHBoDsI=", new DateTime(2020, 6, 6, 13, 4, 30, 420, DateTimeKind.Local).AddTicks(8768), "Justice51" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 15,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 13, new DateTime(2020, 6, 6, 13, 4, 30, 427, DateTimeKind.Local).AddTicks(1925), "Vicky.Kautzer41@gmail.com", "uF/0ZvgNDXtPmeTziLaLIIbZAYyfGIkmNOhTbna8M7c=", "rPvNSyehdwQ8mUkFmBH+0iXSLUIN8zG2+9tb8pmeSOU=", new DateTime(2020, 6, 6, 13, 4, 30, 427, DateTimeKind.Local).AddTicks(1985), "Wilfred_Green96" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 16,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 17, new DateTime(2020, 6, 6, 13, 4, 30, 433, DateTimeKind.Local).AddTicks(5314), "Claud_Larkin@gmail.com", "eaMY7jSgMZo0sjT4TE6o0J1x+rOOqWoT13G0oqO7ZcQ=", "rlsbuGx9e1q+pNboeru17rntw3gbqH1R1fshrsJdJsY=", new DateTime(2020, 6, 6, 13, 4, 30, 433, DateTimeKind.Local).AddTicks(5352), "Penelope.Littel67" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 17,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 7, new DateTime(2020, 6, 6, 13, 4, 30, 444, DateTimeKind.Local).AddTicks(267), "Bertram.Lang20@hotmail.com", "YKxW+74m2SOIY/QgRVCzW1U4vwxqyGO4CUpWhzT4oFI=", "TL1NeE5Nkuszskp+kILjb1d+3b/rY4nsAzpgTgWRPow=", new DateTime(2020, 6, 6, 13, 4, 30, 444, DateTimeKind.Local).AddTicks(316), "Ciara.Raynor6" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 18,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 20, new DateTime(2020, 6, 6, 13, 4, 30, 454, DateTimeKind.Local).AddTicks(1925), "Alexanne36@gmail.com", "KghPj6CeEgJfQJMcqLz93Src7RxNFE1qIN+/2bZaMW0=", "p3NMiAwyzLWYVZsaAUzpr0tv803B7vFkxtg6K0Dk9Pc=", new DateTime(2020, 6, 6, 13, 4, 30, 454, DateTimeKind.Local).AddTicks(1959), "Glennie.Nicolas70" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 19,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 2, new DateTime(2020, 6, 6, 13, 4, 30, 460, DateTimeKind.Local).AddTicks(5603), "Luna_Marquardt@yahoo.com", "r71QOe/ZCvPGTllMw6JYAaLroiPsJVV1ko08xHqL/+s=", "EYD4uYa1xSskVIm4uXTGjziQn0gksOHuOEtkVqW9xU4=", new DateTime(2020, 6, 6, 13, 4, 30, 460, DateTimeKind.Local).AddTicks(5648), "Nona_Murphy6" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 20,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 16, new DateTime(2020, 6, 6, 13, 4, 30, 466, DateTimeKind.Local).AddTicks(6680), "Noel24@yahoo.com", "h1X1c1Av8Jiza9yC6MOggGd34U55K+UQ0ua27AavjuQ=", "BZ6sQDQBv2ObnG9rHs+mQMUc4xl5sIdZaMs36RVlE5Y=", new DateTime(2020, 6, 6, 13, 4, 30, 466, DateTimeKind.Local).AddTicks(6695), "Chet_Miller" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 21,
                columns: new[] { "CreatedAt", "Password", "Salt", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 6, 13, 4, 30, 472, DateTimeKind.Local).AddTicks(8049), "FMSuFW9UwBmcCWU6ov0reHvcTZWdV/w0fv3wNzWc/qA=", "I++YOzymuGdYB0vwhQSrqTR4AzhMbk9RXbktG5tIYSs=", new DateTime(2020, 6, 6, 13, 4, 30, 472, DateTimeKind.Local).AddTicks(8049) });
        }
    }
}
