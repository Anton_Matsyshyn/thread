﻿using System;
using Thread_.NET.DAL.Entities.Abstract;

namespace Thread_.NET.DAL.Entities
{
    public sealed class PostReaction: Reaction, IEquatable<PostReaction>
    {
        public int PostId { get; set; }
        public Post Post { get; set; }

        public bool Equals(PostReaction other)
        {
            return PostId == other.PostId && base.Equals(other);
        }
    }
}
