﻿using System;
using Thread_.NET.DAL.Entities.Abstract;

namespace Thread_.NET.DAL.Entities
{
    public sealed class CommentReaction : Reaction, IEquatable<CommentReaction>
    {
        public int CommentId { get; set; }
        public Comment Comment { get; set; }

        public bool Equals(CommentReaction other)
        {
            return CommentId == other.CommentId && base.Equals(other);
        }
    }
}
