﻿using System;

namespace Thread_.NET.DAL.Entities.Abstract
{
    public abstract class Reaction : BaseEntity, IEquatable<Reaction>
    {
        public int UserId { get; set; }
        public User User { get; set; }

        public bool IsLike { get; set; }

        public bool Equals(Reaction other)
        {
            return UserId == other.UserId && base.Equals(other);
        }
    }
}
