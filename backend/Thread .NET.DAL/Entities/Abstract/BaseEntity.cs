﻿using System;

namespace Thread_.NET.DAL.Entities.Abstract
{
    public abstract class BaseEntity : IEquatable<BaseEntity>
    {
        private DateTime _createdAt;

        public BaseEntity()
        {
            CreatedAt = UpdatedAt = DateTime.Now;
        }

        public int Id { get; set; }
        public DateTime CreatedAt
        {
            get => _createdAt;
            set => _createdAt = (value == null || value == DateTime.MinValue) ? DateTime.Now : value;
        }
        public DateTime UpdatedAt { get; set; }

        public bool Equals(BaseEntity other)
        {
            return Id == other.Id;
        }
    }
}
