﻿using Microsoft.AspNetCore.SignalR;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using System.Threading.Tasks;
using Thread_.NET.Common.DTO.Like;

namespace Thread_.NET.BLL.Hubs
{
    public class LikeHub:Hub
    {
        public async Task Send(string whoLike)
        {
            await Clients.All.SendAsync("NewPost", whoLike);
        }
    }
}
