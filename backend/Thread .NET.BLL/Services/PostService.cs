﻿using AutoMapper;
using Microsoft.AspNetCore.SignalR;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Thread_.NET.BLL.Exceptions;
using Thread_.NET.BLL.Hubs;
using Thread_.NET.BLL.Services.Abstract;
using Thread_.NET.Common.DTO.Post;
using Thread_.NET.Common.DTO.User;
using Thread_.NET.DAL.Context;
using Thread_.NET.DAL.Entities;

namespace Thread_.NET.BLL.Services
{
    public sealed class PostService : BaseService
    {
        private readonly IHubContext<PostHub> _postHub;
        private readonly EmailService _emailService;
        public PostService(ThreadContext context, IMapper mapper, IHubContext<PostHub> postHub, EmailService emailService) : base(context, mapper)
        {
            _postHub = postHub;
            _emailService = emailService;
        }

        public async Task<ICollection<PostDTO>> GetAllPosts()
        {
            var posts = await _context.Posts
                .Include(post => post.Author)
                    .ThenInclude(author => author.Avatar)
                .Include(post => post.Preview)
                .Include(post => post.Reactions)
                    .ThenInclude(reaction => reaction.User)
                        .ThenInclude(user => user.Avatar)
                .Include(post => post.Comments)
                    .ThenInclude(comment => comment.Reactions)
                        .ThenInclude(reaction => reaction.User)
                .Include(post => post.Comments)
                    .ThenInclude(comment => comment.Author)
                .OrderByDescending(post => post.CreatedAt)
                .ToListAsync();

            return _mapper.Map<ICollection<PostDTO>>(posts);
        }

        public async Task<ICollection<PostDTO>> GetAllPosts(int userId)
        {
            var posts = await _context.Posts
                .Include(post => post.Author)
                    .ThenInclude(author => author.Avatar)
                .Include(post => post.Preview)
                .Include(post => post.Comments)
                    .ThenInclude(comment => comment.Author)
                .Where(p => p.AuthorId == userId) // Filter here
                .ToListAsync();

            return _mapper.Map<ICollection<PostDTO>>(posts);
        }

        public async Task<PostDTO> CreatePost(PostCreateDTO postDto)
        {
            var postEntity = _mapper.Map<Post>(postDto);

            _context.Posts.Add(postEntity);
            await _context.SaveChangesAsync();

            var createdPost = await _context.Posts
                .Include(post => post.Author)
					.ThenInclude(author => author.Avatar)
                .FirstAsync(post => post.Id == postEntity.Id);

            var createdPostDTO = _mapper.Map<PostDTO>(createdPost);
            await _postHub.Clients.All.SendAsync("NewPost", createdPostDTO);

            return createdPostDTO;
        }
    
        public async Task<PostDTO> UpdatePost(PostDTO updatedPost)
        {
            var post = await _context.Posts.Where(p => p.Id == updatedPost.Id)
                                    .Include(post => post.Author)
                                        .ThenInclude(author => author.Avatar)
                                    .Include(post => post.Preview)
                                    .Include(post => post.Reactions)
                                        .ThenInclude(reaction => reaction.User)
                                            .ThenInclude(user => user.Avatar)
                                    .Include(post => post.Comments)
                                        .ThenInclude(comment => comment.Reactions)
                                            .ThenInclude(reaction => reaction.User)
                                    .Include(post => post.Comments)
                                        .ThenInclude(comment => comment.Author)
                                    .FirstOrDefaultAsync();

            if (post == null)
                throw new NotFoundException(nameof(post));

            post.Body = updatedPost.Body;
            if(updatedPost.PreviewImage != null)
            {
                post.Preview ??= new Image();
                post.Preview.URL = updatedPost.PreviewImage;
            }

            _context.Entry(post).State = EntityState.Modified;
            await _context.SaveChangesAsync();

            return _mapper.Map<PostDTO>(post);
        }

        public async Task<PostDTO> DeletePost(int id)
        {
            var post = await _context.Posts.Where(p => p.Id == id)
                                    .Include(post => post.Author)
                                        .ThenInclude(author => author.Avatar)
                                    .Include(post => post.Preview)
                                    .Include(post => post.Reactions)
                                        .ThenInclude(reaction => reaction.User)
                                            .ThenInclude(user => user.Avatar)
                                    .Include(post => post.Comments)
                                        .ThenInclude(comment => comment.Reactions)
                                            .ThenInclude(reaction => reaction.User)
                                    .Include(post => post.Comments)
                                        .ThenInclude(comment => comment.Author)
                                    .FirstOrDefaultAsync();

            if (post == null)
                throw new NotFoundException(nameof(post));

            post.IsDeleted = !post.IsDeleted;
            _context.Entry(post).State = EntityState.Modified;

            await _context.SaveChangesAsync();

            return _mapper.Map<PostDTO>(post);
        }

        public async Task SharePostByEmail(string email, int postId, int whoShareId)
        {
            var user = await _context.Users.FirstOrDefaultAsync(u => u.Id == whoShareId);

            if (user == null)
                throw new NotFoundException(nameof(user));

            var post = await _context.Posts.Where(p => p.Id == postId)
                                     .Include(p => p.Author)
                                     .FirstOrDefaultAsync();

            if (post == null)
                throw new NotFoundException(nameof(post));

            await _emailService.SharePostByEmail(email, _mapper.Map<PostDTO>(post), user.UserName);
        }
    }
}
