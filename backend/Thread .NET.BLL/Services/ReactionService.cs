﻿using AutoMapper;
using Microsoft.AspNetCore.SignalR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Threading.Tasks;
using Thread_.NET.BLL.Hubs;
using Thread_.NET.BLL.Services.Abstract;
using Thread_.NET.Common.DTO.Like;
using Thread_.NET.Common.DTO.Post;
using Thread_.NET.Common.DTO.User;
using Thread_.NET.DAL.Context;

namespace Thread_.NET.BLL.Services
{
    public sealed class ReactionService : BaseService
    {
        readonly EmailService _emailService;
        readonly IHubContext<LikeHub> _likeHub;
        public ReactionService(ThreadContext context,
                               IMapper mapper,
                               EmailService emailService,
                               IHubContext<LikeHub> likeHub) : base(context, mapper)
        {
            _emailService = emailService;
            _likeHub = likeHub;
        }

        public async Task AddReactionToPost(NewReactionDTO reaction)
        {
            var reactions = _context.PostReactions.Where(x => x.UserId == reaction.UserId && x.PostId == reaction.EntityId);

            if(reactions.Any(l => l.IsLike == reaction.IsLike))
            {
                _context.PostReactions.RemoveRange(reactions);
                await _context.SaveChangesAsync();

                return;
            }

            if (reactions.Any())
            {
                _context.PostReactions.RemoveRange(reactions);
                await _context.SaveChangesAsync();
            }

            _context.PostReactions.Add(new DAL.Entities.PostReaction
            {
                PostId = reaction.EntityId,
                IsLike = reaction.IsLike,
                UserId = reaction.UserId
            });

            await _context.SaveChangesAsync();

            if (reaction.IsLike)
            {
                var userWhoLiked = await _context.Users.FirstOrDefaultAsync(u => u.Id == reaction.UserId);
                var post = await _context.Posts.Where(p => p.Id == reaction.EntityId)
                                               .Include(p => p.Author)
                                               .FirstOrDefaultAsync();

                //send notification to email

                await _emailService.SendLikePostNotification(_mapper.Map<UserDTO>(post.Author),
                                                             _mapper.Map<PostDTO>(post),
                                                             userWhoLiked.UserName);

                //send notification to app

                await _likeHub.Clients.All.SendAsync("NewLike", new { post.Author.Id, userWhoLiked.UserName });
            }
        }

        public async Task AddReactionToComment(NewReactionDTO reaction)
        {
            var reactions = _context.CommentReactions.Where(c => c.UserId == reaction.UserId && c.CommentId == reaction.EntityId);

            if(reactions.Any(l => l.IsLike == reaction.IsLike))
            {
                _context.CommentReactions.RemoveRange(reactions);
                await _context.SaveChangesAsync();

                return;
            }

            if(reactions.Any())
            {
                _context.CommentReactions.RemoveRange(reactions);
                await _context.SaveChangesAsync();
            }

            _context.CommentReactions.Add(new DAL.Entities.CommentReaction
            {
                CommentId = reaction.EntityId,
                IsLike = reaction.IsLike,
                UserId = reaction.UserId
            });

            await _context.SaveChangesAsync();
        }
    }
}
