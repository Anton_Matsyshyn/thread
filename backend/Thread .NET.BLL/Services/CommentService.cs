﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Thread_.NET.BLL.Exceptions;
using Thread_.NET.BLL.Services.Abstract;
using Thread_.NET.Common.DTO.Comment;
using Thread_.NET.Common.DTO.User;
using Thread_.NET.DAL.Context;
using Thread_.NET.DAL.Entities;

namespace Thread_.NET.BLL.Services
{
    public sealed class CommentService : BaseService
    {
        public CommentService(ThreadContext context, IMapper mapper) : base(context, mapper) { }

        public async Task<CommentDTO> CreateComment(NewCommentDTO newComment)
        {
            var commentEntity = _mapper.Map<Comment>(newComment);

            _context.Comments.Add(commentEntity);
            await _context.SaveChangesAsync();

            var createdComment = await _context.Comments
                .Include(comment => comment.Author)
                    .ThenInclude(user => user.Avatar)
                .FirstAsync(comment => comment.Id == commentEntity.Id);

            return _mapper.Map<CommentDTO>(createdComment);
        }

        public async Task<CommentDTO> DeleteComment(int commentId)
        {
            var comment = await _context.Comments.Where(c => c.Id == commentId)
                                                 .Include(c => c.Author)
                                                        .ThenInclude(a => a.Avatar)
                                                 .Include(c => c.Reactions)
                                                    .ThenInclude(r => r.User)
                                                 .FirstOrDefaultAsync();

            if (comment == null)
                throw new NotFoundException(nameof(comment));

            comment.IsDeleted = !comment.IsDeleted;
            _context.Entry(comment).State = EntityState.Modified;

            await _context.SaveChangesAsync();

            return _mapper.Map<CommentDTO>(comment);
        }

        public async Task<CommentDTO> UpdateComment(CommentDTO newComment)
         {
            var comment = await _context.Comments.Where(c => c.Id == newComment.Id)
                                                    .Include(c => c.Author)
                                                        .ThenInclude(a => a.Avatar)
                                                 .Include(c => c.Reactions)
                                                    .ThenInclude(r => r.User)
                                                 .FirstOrDefaultAsync();

            if (comment == null)
                throw new NotFoundException(nameof(comment));

            //var updatedComment = _mapper.Map(newComment, comment);

            comment.Body = newComment.Body;

            _context.Entry(comment).State = EntityState.Modified;
            await _context.SaveChangesAsync();

            return _mapper.Map<CommentDTO>(comment);
        }
    
    }
}
