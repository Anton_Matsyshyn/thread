﻿using AutoMapper;
using Bogus.DataSets;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using Thread_.NET.BLL.Services.Abstract;
using Thread_.NET.Common.DTO.Post;
using Thread_.NET.Common.DTO.User;
using Thread_.NET.DAL.Context;
using Thread_.NET.DAL.Entities;

namespace Thread_.NET.BLL.Services
{
    public class EmailService:BaseService
    {
        readonly string SMTP_SERVER;
        readonly int SMTP_PORT;
        readonly string SMTP_ACCOUNT;
        readonly string SMTP_PASSWORD;

        readonly IConfiguration _configuration;

        public EmailService(ThreadContext context, IMapper mapper, IConfiguration configuration) : base(context, mapper) 
        {
            _configuration = configuration;

            SMTP_SERVER = Environment.GetEnvironmentVariable(nameof(SMTP_SERVER));
            SMTP_PORT = int.Parse(Environment.GetEnvironmentVariable(nameof(SMTP_PORT)));
            SMTP_ACCOUNT = Environment.GetEnvironmentVariable(nameof(SMTP_ACCOUNT));
            SMTP_PASSWORD = Environment.GetEnvironmentVariable(nameof(SMTP_PASSWORD));
        }

        public async Task SendLikePostNotification(UserDTO to, PostDTO post, string whoLiked)
        {
            string userName = to.UserName;
            string body = $"Your post was liked by <em>{whoLiked}</em>. Click the link above and check it yourself: ";
            string buttonLink = "#" + post.Id;
            string buttonText = "TO MY POST";

            MailAddress fromAddres = new MailAddress(SMTP_ACCOUNT, "Thread .NET");
            MailAddress toAddres = new MailAddress(to.Email);
            MailMessage m = new MailMessage(fromAddres, toAddres);
            m.Subject = "One of your posts was liked";
            m.Body = await GetEmailTemplate(userName, body, buttonLink, buttonText);
            m.IsBodyHtml = true;
            SmtpClient smtp = new SmtpClient(SMTP_SERVER, SMTP_PORT);
            smtp.Credentials = new NetworkCredential(SMTP_ACCOUNT, SMTP_PASSWORD);
            smtp.EnableSsl = true;
            smtp.Send(m);
        }

        public async Task SharePostByEmail(string email, PostDTO post, string whoShare)
        {
            string userName = whoShare;
            string body = $"User <em>{userName}</em> share post to you. Click link above to watch it: ";
            string buttonLink = "#" + post.Id;
            string buttonText = "OPEN POST";

            MailAddress fromAddres = new MailAddress(SMTP_ACCOUNT, "Thread .NET");
            MailAddress toAddres = new MailAddress(email);
            MailMessage m = new MailMessage(fromAddres, toAddres);
            m.Subject = $"Check {post.Author.UserName} post!";
            m.Body = await GetEmailTemplate(email, body, buttonLink, buttonText);
            m.IsBodyHtml = true;
            SmtpClient smtp = new SmtpClient(SMTP_SERVER, SMTP_PORT);
            smtp.Credentials = new NetworkCredential(SMTP_ACCOUNT, SMTP_PASSWORD);
            smtp.EnableSsl = true;
            smtp.Send(m);
        }

        public async Task SendPasswordToEmail(string email, string password)
        {
            string userName = email;
            string body = $"Your password was succesfull reseted. Here is your new password: <strong>{password}</strong>";
            string buttonLink = "";
            string buttonText = "LOGIN";

            MailAddress fromAddres = new MailAddress(SMTP_ACCOUNT, "Thread .NET");
            MailAddress toAddres = new MailAddress(email);
            MailMessage m = new MailMessage(fromAddres, toAddres);
            m.Subject = $"Password reset";
            m.Body = await GetEmailTemplate(userName, body, buttonLink, buttonText);
            m.IsBodyHtml = true;
            SmtpClient smtp = new SmtpClient(SMTP_SERVER, SMTP_PORT);
            smtp.Credentials = new NetworkCredential(SMTP_ACCOUNT, SMTP_PASSWORD);
            smtp.EnableSsl = true;
            smtp.Send(m);
        }
        private async Task<string> GetEmailTemplate(string userName, string body, string buttonLink, string buttonText)
        {
            string emailMarkUp = await File.ReadAllTextAsync(_configuration.GetValue<string>("Emails:MarkUp"));
            emailMarkUp = emailMarkUp.Replace("{{userName}}", userName)
                                     .Replace("{{emailBody}}", body)
                                     .Replace("{{buttonLink}}", "http://localhost:4200/" + buttonLink)
                                     .Replace("{{buttonText}}", buttonText);
            return emailMarkUp;
        }
    }
}
