import { Directive } from '@angular/core';
import { NG_VALIDATORS, Validator, AbstractControl } from '@angular/forms';

@Directive({
  selector: '[appEmailValidator]',
  providers: [{ provide: NG_VALIDATORS, useExisting: EmailValidatorDirective, multi: true }]
})
export class EmailValidatorDirective implements Validator {

  constructor() { }

  validate(control: AbstractControl): { [key: string]: any } | null {
    return this.emailValidator(control.value) ? null : { "email": "Email is not valid" };
  }

  emailValidator(email: string): boolean {
    return /[A-Za-z0-9_]+@(?!@)+[A-Za-z0-9_]+\.+[A-Za-z0-9_]/g.test(email);
  }
}
