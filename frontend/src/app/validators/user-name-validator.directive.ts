import { Directive } from '@angular/core';
import { NG_VALIDATORS, Validator, AbstractControl } from '@angular/forms';

@Directive({
  selector: '[appUserNameValidator]',
  providers: [{ provide: NG_VALIDATORS, useExisting: UserNameValidatorDirective, multi: true }]

})
export class UserNameValidatorDirective implements Validator {

  constructor() { }

  validate(control: AbstractControl): { [key: string]: any } | null {
    return this.validateUserName(control.value) ? null : { "username": "Username contain forbidden characters" };
  }

  validateUserName(userName: string): boolean {
    let specialCharacters = /[`!@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?~]/;
    return !specialCharacters.test(userName);
  }
}
