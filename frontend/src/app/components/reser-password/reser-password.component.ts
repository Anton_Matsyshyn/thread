import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { User } from 'src/app/models/user';
import { UserService } from 'src/app/services/user.service';
import { SnackBarService } from 'src/app/services/snack-bar.service';

@Component({
  selector: 'app-reser-password',
  templateUrl: './reser-password.component.html',
  styleUrls: ['./reser-password.component.sass']
})
export class ReserPasswordComponent implements OnInit {

  public email: string;

  constructor(private dialog: MatDialogRef<ReserPasswordComponent>,
    private userService: UserService,
    private snackBar: SnackBarService,
    @Inject(MAT_DIALOG_DATA) public data: any) { }

  ngOnInit(): void {
  }

  reset() {
    this.dialog.close();
    this.userService.resetPassword(this.email)
      .subscribe((resp) => {
        if (resp.ok) {
          this.snackBar.showUsualMessage('Password was succesfull reseted.');
        }
        else {
          this.snackBar.showErrorMessage('Password was not succesfull reseted.');
        }
        return this.email;
      });
  }
}
