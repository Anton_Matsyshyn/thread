import { Component, Input, OnInit } from '@angular/core';
import { User } from 'src/app/models/user';
import { MatDialog } from '@angular/material/dialog';
import { UserLikesListFullComponent } from '../user-likes-list-full/user-likes-list-full.component';

@Component({
  selector: 'app-users-likes-list',
  templateUrl: './users-likes-list.component.html',
  styleUrls: ['./users-likes-list.component.sass']
})
export class UsersLikesListComponent implements OnInit {

  @Input() users: User[];

  constructor(private dialog: MatDialog) { }

  public ngOnInit() {
  }

  public openUserLikeList() {
    this.dialog.open(UserLikesListFullComponent, {
      data: { users: this.users },
    });
  }
}
