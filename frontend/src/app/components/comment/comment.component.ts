import { Component, Input, Output, EventEmitter, OnInit } from '@angular/core';
import { Comment } from '../../models/comment/comment';
import { AuthenticationService } from '../../services/auth.service';
import { LikeService } from '../../services/like.service';
import { DislikeService } from '../../services/dislike.service';
import { User } from 'src/app/models/user';
import { switchMap, map } from 'rxjs/operators';
import { CommentService } from 'src/app/services/comment.service';

@Component({
    selector: 'app-comment',
    templateUrl: './comment.component.html',
    styleUrls: ['./comment.component.sass']
})
export class CommentComponent implements OnInit {
    public isUserAuthor: boolean;
    public onEdit: boolean;
    public usersWhoLikeComment: User[];

    @Input() public comment: Comment;
    @Input() public currentUser: User;

    @Output('onChangeCommentVisability') onChangeCommentVisability: EventEmitter<number> = new EventEmitter();

    constructor(private authService: AuthenticationService,
        private likeService: LikeService,
        private dislikeService: DislikeService,
        private commentService: CommentService) { }

    public ngOnInit() {
        if (!this.currentUser)
            this.authService.getUser()
                .subscribe((userResp) => this.currentUser = userResp);

        if (this.currentUser)
            this.isUserAuthor = this.currentUser.id == this.comment.author.id;
    }

    public changeVisability() {
        if (!this.currentUser)
            this.authService.getUser()
                .subscribe((userResp) => this.currentUser = userResp);

        if (this.currentUser.id != this.comment.author.id)
            return;

        this.comment.isDeleted = !this.comment.isDeleted;
        this.onChangeCommentVisability.emit(this.comment.id);

        this.commentService.changeVisability(this.comment.id)
            .subscribe(() => this.comment);
    }

    public changeEditState() {
        if (!this.currentUser)
            this.authService.getUser()
                .subscribe((userResp) => this.currentUser = userResp);

        if (this.currentUser.id != this.comment.author.id)
            return;

        if (this.onEdit)
            this.commentService.updateComment(this.comment)
                .subscribe(c => this.comment = c);

        this.onEdit = !this.onEdit;
    }

    public dislikeComment() {
        if (this.comment.isDeleted)
            return;

        if (!this.currentUser) {
            this.authService.getUser().pipe(
                switchMap((userResp) => this.dislikeService.dislikeComment(this.comment, userResp)),
            )
                .subscribe(() => this.comment);

            return;
        }

        this.dislikeService
            .dislikeComment(this.comment, this.currentUser)
            .subscribe(() => this.comment);
    }

    public likeComment() {
        if (this.comment.isDeleted)
            return;

        if (!this.currentUser) {
            this.authService.getUser().pipe(
                switchMap((userResp) => {
                    this.currentUser = userResp;
                    return this.likeService.likeComment(this.comment, userResp);
                }),
            )
                .subscribe(() => this.comment);

            return;
        }

        this.likeService
            .likeComment(this.comment, this.currentUser)
            .subscribe(() => this.comment);
    }

    public usersWhoLikedComment(): User[] {
        // this.commentService.getUsersWhoLiked(this.comment.id)
        //     .subscribe(users => this.usersWhoLikeComment = users);
        let a = this.comment.reactions.filter(reaction => reaction.isLike).map((reaction) => reaction.user);

        // return this.usersWhoLikeComment;
        return this.comment.reactions.filter(reaction => reaction.isLike).map((reaction) => reaction.user);
    }
    public dislikeCount(): number {
        return this.comment.reactions.filter(c => !c.isLike).length;
    }

    public likeCount(): number {
        return this.comment.reactions.filter(c => c.isLike).length;
    }
}
