import { Component, Input, OnDestroy, Output, EventEmitter, OnInit } from '@angular/core';
import { Post } from '../../models/post/post';
import { AuthenticationService } from '../../services/auth.service';
import { AuthDialogService } from '../../services/auth-dialog.service';
import { empty, Observable, Subject } from 'rxjs';
import { DialogType } from '../../models/common/auth-dialog-type';
import { LikeService } from '../../services/like.service';
import { NewComment } from '../../models/comment/new-comment';
import { CommentService } from '../../services/comment.service';
import { User } from '../../models/user';
import { Comment } from '../../models/comment/comment';
import { catchError, switchMap, takeUntil } from 'rxjs/operators';
import { SnackBarService } from '../../services/snack-bar.service';
import { DislikeService } from 'src/app/services/dislike.service';
import { PostService } from 'src/app/services/post.service';
import { ImgurService } from 'src/app/services/imgur.service';
import { MatDialog } from '@angular/material/dialog';
import { SharePostComponent } from '../share-post/share-post.component';

@Component({
    selector: 'app-post',
    templateUrl: './post.component.html',
    styleUrls: ['./post.component.sass']
})
export class PostComponent implements OnDestroy, OnInit {
    @Input() public post: Post;
    @Input() public currentUser: User;

    public isUserAuthor = false;
    public showComments = false;
    public onEdit = false;
    public newComment = {} as NewComment;

    public imageUrl: string;
    public imageFile: File;

    @Output('onChangePostVisability') onChangePostVisability: EventEmitter<number> = new EventEmitter();
    @Output('onChangeReaction') onChangeReaction: EventEmitter<number> = new EventEmitter();

    private unsubscribe$ = new Subject<void>();

    public constructor(
        private authService: AuthenticationService,
        private authDialogService: AuthDialogService,
        private imgurService: ImgurService,
        private postService: PostService,
        private likeService: LikeService,
        private dislikeService: DislikeService,
        private commentService: CommentService,
        private snackBarService: SnackBarService,
        private dialog: MatDialog
    ) { }

    public ngOnDestroy() {
        this.unsubscribe$.next();
        this.unsubscribe$.complete();
    }

    public ngOnInit() {
        if (!this.currentUser)
            this.authService.getUser()
                .subscribe((userResp) => this.currentUser = userResp);

        if (this.currentUser)
            this.isUserAuthor = this.currentUser.id == this.post.author.id;
    }

    public toggleComments() {
        if (!this.currentUser) {
            this.catchErrorWrapper(this.authService.getUser())
                .pipe(takeUntil(this.unsubscribe$))
                .subscribe((user) => {
                    if (user) {
                        this.currentUser = user;
                        this.showComments = !this.showComments;
                    }
                });
            return;
        }

        this.showComments = !this.showComments;
    }

    public likePost() {


        if (!this.currentUser) {
            this.catchErrorWrapper(this.authService.getUser())
                .pipe(
                    switchMap((userResp) => this.likeService.likePost(this.post, userResp)),
                    takeUntil(this.unsubscribe$)
                )
                .subscribe((post) => (this.post = post));
            this.onChangeReaction.emit(this.post.id);
            return;
        }

        this.likeService
            .likePost(this.post, this.currentUser)
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe((post) => (this.post = post));
        this.onChangeReaction.emit(this.post.id);
    }

    public dislikePost() {
        if (!this.currentUser) {
            this.catchErrorWrapper(this.authService.getUser())
                .pipe(
                    switchMap((userResp) => this.dislikeService.dislikePost(this.post, userResp)),
                    takeUntil(this.unsubscribe$)
                )
                .subscribe((post) => (this.post = post));
            this.onChangeReaction.emit(this.post.id);
            return;
        }

        this.dislikeService
            .dislikePost(this.post, this.currentUser)
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe((post) => (this.post = post));
        this.onChangeReaction.emit(this.post.id);
    }

    public likeCount(): number {
        return this.post.reactions.filter(r => r.isLike).length;
    }

    public dislikeCount(): number {
        return this.post.reactions.filter(r => !r.isLike).length;
    }

    public sendComment() {
        this.newComment.authorId = this.currentUser.id;
        this.newComment.postId = this.post.id;

        this.commentService
            .createComment(this.newComment)
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe(
                (resp) => {
                    if (resp) {
                        this.post.comments = this.sortCommentArray(this.post.comments.concat(resp.body));
                        this.newComment.body = undefined;
                    }
                },
                (error) => this.snackBarService.showErrorMessage(error)
            );
    }

    public openAuthDialog() {
        this.authDialogService.openAuthDialog(DialogType.SignIn);
    }

    public changeEditState() {
        if (!this.currentUser)
            this.authService.getUser()
                .subscribe((userResp) => this.currentUser = userResp);

        if (this.currentUser.id != this.post.author.id)
            return;

        if (this.onEdit) {
            this.post.previewImage = this.imageUrl ? this.imageUrl : this.post.previewImage;
            const postSubscription = !this.imageFile
                ? this.postService.updatePost(this.post)
                : this.imgurService.uploadToImgur(this.imageFile, 'title').pipe(
                    switchMap((imageData) => {
                        this.post.previewImage = imageData.body.data.link;
                        return this.postService.updatePost(this.post);
                    })
                );

            postSubscription.pipe(takeUntil(this.unsubscribe$)).subscribe(
                () => this.post,
                (error) => this.snackBarService.showErrorMessage(error)
            );
        }

        this.onEdit = !this.onEdit;
    }

    public changeVisability() {
        if (!this.currentUser)
            this.authService.getUser()
                .subscribe((userResp) => this.currentUser = userResp);

        if (this.currentUser.id != this.post.author.id)
            return;

        this.post.isDeleted = !this.post.isDeleted;
        this.onChangePostVisability.emit(this.post.id);

        this.postService.changeVisability(this.post.id)
            .subscribe(() => this.post);
    }

    public usersWhoLikedPost() {
        return this.post.reactions.filter(reaction => reaction.isLike).map(reaction => reaction.user);
    }

    public loadImage(target) {
        this.imageFile = target.files[0];

        if (!this.imageFile) {
            target.value = '';
            return;
        }

        if (this.imageFile.size / 1000000 > 5) {
            target.value = '';
            this.snackBarService.showErrorMessage(`Image can't be heavier than ~5MB`);
            return;
        }

        const reader = new FileReader();
        reader.addEventListener('load', () => (this.imageUrl = reader.result as string));
        reader.readAsDataURL(this.imageFile);
    }

    public sharePost() {
        this.dialog.open(SharePostComponent, {
            data: { postId: this.post.id },
            backdropClass: 'dialog-backdrop'
        });
    }
    private catchErrorWrapper(obs: Observable<User>) {
        return obs.pipe(
            catchError(() => {
                this.openAuthDialog();
                return empty();
            })
        );
    }

    private sortCommentArray(array: Comment[]): Comment[] {
        return array.sort((a, b) => +new Date(b.createdAt) - +new Date(a.createdAt));
    }
}
