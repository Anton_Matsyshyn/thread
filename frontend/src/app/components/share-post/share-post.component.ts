import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { PostService } from 'src/app/services/post.service';

@Component({
  selector: 'app-share-post',
  templateUrl: './share-post.component.html',
  styleUrls: ['./share-post.component.sass']
})
export class SharePostComponent implements OnInit {

  public email: string;

  constructor(private dialog: MatDialogRef<SharePostComponent>,
    private postService: PostService,
    @Inject(MAT_DIALOG_DATA) public data: any) { }

  public shareByEmail() {
    this.postService.sharePost(this.email, this.data.postId)
      .subscribe(() => this.email);
    this.dialog.close();
  }

  public ngOnInit() { }
}
