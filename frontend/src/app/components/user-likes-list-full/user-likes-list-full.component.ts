import { User } from 'src/app/models/user';
import { Component, OnInit, Inject, OnDestroy } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'app-user-likes-list-full',
  templateUrl: './user-likes-list-full.component.html',
  styleUrls: ['./user-likes-list-full.component.sass']
})
export class UserLikesListFullComponent {

  constructor(private dialog: MatDialogRef<UserLikesListFullComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any) { }

}
