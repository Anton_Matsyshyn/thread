import { Component, OnInit, OnDestroy, ViewChild, ElementRef, AfterViewChecked, AfterViewInit } from '@angular/core';
import { ActivatedRoute } from "@angular/router"
import { Post } from '../../models/post/post';
import { Comment } from '../../models/comment/comment';
import { User } from '../../models/user';
import { Subject } from 'rxjs';
import { MatSlideToggleChange, MatSlideToggle } from '@angular/material/slide-toggle';
import { AuthenticationService } from '../../services/auth.service';
import { PostService } from '../../services/post.service';
import { AuthDialogService } from '../../services/auth-dialog.service';
import { DialogType } from '../../models/common/auth-dialog-type';
import { EventService } from '../../services/event.service';
import { ImgurService } from '../../services/imgur.service';
import { NewPost } from '../../models/post/new-post';
import { switchMap, takeUntil, filter } from 'rxjs/operators';
import { HubConnectionBuilder, HubConnection } from '@aspnet/signalr';
import { SnackBarService } from '../../services/snack-bar.service';

@Component({
    selector: 'app-main-thread',
    templateUrl: './main-thread.component.html',
    styleUrls: ['./main-thread.component.sass']
})
export class MainThreadComponent implements OnInit, OnDestroy, AfterViewChecked {
    public posts: Post[] = [];
    public cachedPosts: Post[] = [];
    public isOnlyMine = false;
    public isOnlyLiked = false;

    public currentUser: User;
    public imageUrl: string;
    public imageFile: File;
    public post = {} as NewPost;
    public showPostContainer = false;
    public loading = false;
    public loadingPosts = false;
    public firstTimeLoading = true;

    public postHub: HubConnection;
    public likeHub: HubConnection;

    private unsubscribe$ = new Subject<void>();

    public constructor(
        private snackBarService: SnackBarService,
        private authService: AuthenticationService,
        private postService: PostService,
        private imgurService: ImgurService,
        private authDialogService: AuthDialogService,
        private eventService: EventService,
        private router: ActivatedRoute
    ) { }

    public ngOnDestroy() {
        this.unsubscribe$.next();
        this.unsubscribe$.complete();
        this.postHub.stop();
        this.likeHub.stop();
    }

    public ngOnInit() {
        this.registerHub();
        this.getPosts();
        this.getUser();

        this.eventService.userChangedEvent$.pipe(takeUntil(this.unsubscribe$)).subscribe((user) => {
            this.currentUser = user;
            this.post.authorId = this.currentUser ? this.currentUser.id : undefined;
        });
    }

    public ngAfterViewChecked() {
        if (!this.firstTimeLoading)
            return;

        let fragment;
        this.router.fragment.subscribe((f) => fragment = f);

        if (!fragment)
            return;

        let el = document.getElementById(fragment);
        if (el) {
            this.firstTimeLoading = false;

            el.scrollIntoView();
        }
    }

    public getPosts() {
        this.loadingPosts = true;
        this.postService
            .getPosts()
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe(
                (resp) => {
                    this.loadingPosts = false;
                    this.posts = this.cachedPosts = resp.body.map(p => this.filterComments(p));
                    this.posts = this.filterPosts(this.posts);
                },
                (error) => (this.loadingPosts = false)
            );
    }

    public sendPost() {
        const postSubscription = !this.imageFile
            ? this.postService.createPost(this.post)
            : this.imgurService.uploadToImgur(this.imageFile, 'title').pipe(
                switchMap((imageData) => {
                    this.post.previewImage = imageData.body.data.link;
                    return this.postService.createPost(this.post);
                })
            );

        this.loading = true;

        postSubscription.pipe(takeUntil(this.unsubscribe$)).subscribe(
            (respPost) => {
                this.addNewPost(respPost.body);
                this.removeImage();
                this.post.body = undefined;
                this.post.previewImage = undefined;
                this.loading = false;
            },
            (error) => this.snackBarService.showErrorMessage(error)
        );
    }

    public loadImage(target: any) {
        this.imageFile = target.files[0];

        if (!this.imageFile) {
            target.value = '';
            return;
        }

        if (this.imageFile.size / 1000000 > 5) {
            target.value = '';
            this.snackBarService.showErrorMessage(`Image can't be heavier than ~5MB`);
            return;
        }

        const reader = new FileReader();
        reader.addEventListener('load', () => (this.imageUrl = reader.result as string));
        reader.readAsDataURL(this.imageFile);
    }

    public removeImage() {
        this.imageUrl = undefined;
        this.imageFile = undefined;
    }

    public onlyMineChanged() {
        let newPosts = this.cachedPosts;
        if (!this.isOnlyMine) {
            newPosts = newPosts.filter((x) => x.author.id === this.currentUser.id);
        }

        if (this.isOnlyLiked) {
            newPosts = newPosts.filter((x) => x.reactions.filter(reaction => reaction.isLike)
                .map(r => r.user.id)
                .includes(this.currentUser.id));
        }

        this.isOnlyMine = !this.isOnlyMine;
        this.posts = newPosts;
    }

    public onlyLikedChanged() {
        let newPosts = this.cachedPosts;

        if (!this.isOnlyLiked) {
            newPosts = newPosts.filter((x) => x.reactions.filter(reaction => reaction.isLike)
                .map(r => r.user.id)
                .includes(this.currentUser.id));
        }

        if (this.isOnlyMine) {
            newPosts = newPosts.filter((x) => x.author.id === this.currentUser.id);
        }

        this.isOnlyLiked = !this.isOnlyLiked;
        this.posts = newPosts;
    }

    public checkPostList() {
        if (this.isOnlyLiked) {
            this.posts = this.posts.filter((x) => x.reactions.filter(reaction => reaction.isLike)
                .map(r => r.user.id)
                .includes(this.currentUser.id));
        }
    }
    public toggleNewPostContainer() {
        this.showPostContainer = !this.showPostContainer;
    }

    public openAuthDialog() {
        this.authDialogService.openAuthDialog(DialogType.SignIn);
    }

    public registerHub() {
        this.postHub = new HubConnectionBuilder().withUrl('https://localhost:44344/notifications/post').build();
        this.postHub.start().catch((error) => this.snackBarService.showErrorMessage(error));

        this.postHub.on('NewPost', (newPost: Post) => {
            if (newPost) {
                this.addNewPost(newPost);
            }
        });

        this.likeHub = new HubConnectionBuilder().withUrl('https://localhost:44344/notifications/like').build();
        this.likeHub.start().catch((error) => this.snackBarService.showErrorMessage(error));

        this.likeHub.on('NewLike', (obj) => {

            if (!obj.id || obj.id != this.currentUser.id) {
                return;
            }

            if (obj.userName) {
                this.snackBarService.showUsualMessage(`Your post was liked by ${obj.userName}`);
            }
            console.log('Like hub working...');
        });
    }

    public addNewPost(newPost: Post) {
        if (!this.cachedPosts.some((x) => x.id === newPost.id)) {
            this.cachedPosts = this.sortPostArray(this.cachedPosts.concat(newPost));
            if (!this.isOnlyMine || (this.isOnlyMine && newPost.author.id === this.currentUser.id)) {
                this.posts = this.sortPostArray(this.posts.concat(newPost));
            }
        }
    }

    private getUser() {
        this.authService
            .getUser()
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe((user) => (this.currentUser = user));
    }

    private sortPostArray(array: Post[]): Post[] {
        return array.sort((a, b) => +new Date(b.createdAt) - +new Date(a.createdAt));
    }

    private filterComments(post: Post): Post {
        post.comments = post.comments.filter(c => !c.isDeleted || c.author.id == this.currentUser.id);
        return post;
    }

    private filterPosts(posts: Post[]): Post[] {
        return posts.filter(p => !p.isDeleted || p.author.id == this.currentUser.id);
    }
}
