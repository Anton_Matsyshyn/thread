import { Component, OnInit, Inject, OnDestroy } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA, MatDialog } from '@angular/material/dialog';
import { DialogType } from '../../models/common/auth-dialog-type';
import { Subject } from 'rxjs';
import { AuthenticationService } from '../../services/auth.service';
import { takeUntil } from 'rxjs/operators';
import { SnackBarService } from '../../services/snack-bar.service';
import { ReserPasswordComponent } from '../reser-password/reser-password.component';

@Component({
    templateUrl: './auth-dialog.component.html',
    styleUrls: ['./auth-dialog.component.sass']
})
export class AuthDialogComponent implements OnInit, OnDestroy {
    public dialogType = DialogType;
    public userName: string;
    public password: string;
    public avatar: string;
    public email: string;

    public hidePass = true;
    public title: string;
    private unsubscribe$ = new Subject<void>();

    constructor(
        private authDialogRef: MatDialogRef<AuthDialogComponent>,
        private resetPasswordDialog: MatDialog,
        @Inject(MAT_DIALOG_DATA) public data: any,
        private authService: AuthenticationService,
        private snackBarService: SnackBarService
    ) { }

    public ngOnInit() {
        this.avatar = 'https://avatars.mds.yandex.net/get-ott/374297/2a000001616b87458162c9216ccd5144e94d/orig';
        this.title = this.data.dialogType === DialogType.SignIn ? 'Sign In' : 'Sign Up';
    }

    public ngOnDestroy() {
        this.unsubscribe$.next();
        this.unsubscribe$.complete();
    }

    public close() {
        this.authDialogRef.close(false);
    }

    public signIn() {
        this.authService
            .login({ email: this.email, password: this.password })
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe((response) => {
                document.location.reload();
                return this.authDialogRef.close(response);
            }, (error) => this.snackBarService.showErrorMessage(error));
    }

    public signUp() {
        this.authService
            .register({ userName: this.userName, password: this.password, email: this.email, avatar: this.avatar })
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe((response) => {
                document.location.reload();
                return this.authDialogRef.close(response);
            }, (error) => this.snackBarService.showErrorMessage(error));
    }

    public resetPassword() {
        this.resetPasswordDialog.open(ReserPasswordComponent);
    }
}
