import { Injectable } from '@angular/core';
import { AuthenticationService } from './auth.service';
import { Post } from '../models/post/post';
import { Comment } from '../models/comment/comment';
import { NewReaction } from '../models/reactions/newReaction';
import { PostService } from './post.service';
import { CommentService } from './comment.service';
import { User } from '../models/user';
import { map, catchError } from 'rxjs/operators';
import { of } from 'rxjs';

@Injectable({ providedIn: 'root' })
export class DislikeService {
    public constructor(private authService: AuthenticationService,
        private postService: PostService,
        private commentService: CommentService) { }

    public dislikePost(post: Post, currentUser: User) {
        const innerPost = post;

        const reaction: NewReaction = {
            entityId: innerPost.id,
            isLike: false,
            userId: currentUser.id
        };

        let hasSameReaction = innerPost.reactions.some(x => x.user.id === currentUser.id && x.isLike === false);

        if (hasSameReaction) {
            innerPost.reactions = innerPost.reactions.filter((x) => x.user.id !== currentUser.id);
            return this.postService.addReaction(reaction).pipe(
                map(() => innerPost),
                catchError(() => {
                    innerPost.reactions = hasReaction
                        ? innerPost.reactions.filter((x) => x.user.id !== currentUser.id)
                        : innerPost.reactions.concat({ isLike: false, user: currentUser });

                    return of(innerPost);
                })
            );
        }

        let hasReaction = innerPost.reactions.some(x => x.user.id === currentUser.id);

        if (hasReaction) {
            innerPost.reactions = innerPost.reactions.filter((x) => x.user.id !== currentUser.id);
        }

        innerPost.reactions = innerPost.reactions.concat({ isLike: false, user: currentUser });

        hasReaction = innerPost.reactions.some(x => x.user.id === currentUser.id);

        return this.postService.addReaction(reaction).pipe(
            map(() => innerPost),
            catchError(() => {
                innerPost.reactions = hasReaction
                    ? innerPost.reactions.filter((x) => x.user.id !== currentUser.id)
                    : innerPost.reactions.concat({ isLike: false, user: currentUser });

                return of(innerPost);
            })
        );
    }

    public dislikeComment(comment: Comment, currentUser: User) {
        const innerComment = comment;

        const reaction: NewReaction = {
            entityId: innerComment.id,
            isLike: false,
            userId: currentUser.id
        };

        let hasSameReaction = innerComment.reactions.some(x => x.user.id === currentUser.id && x.isLike === false);

        if (hasSameReaction) {
            innerComment.reactions = innerComment.reactions.filter((x) => x.user.id !== currentUser.id);
            return this.commentService.addReaction(reaction).pipe(
                map(() => innerComment),
                catchError(() => {
                    innerComment.reactions = hasReaction
                        ? innerComment.reactions.filter((x) => x.user.id !== currentUser.id)
                        : innerComment.reactions.concat({ isLike: false, user: currentUser });

                    return of(innerComment);
                })
            );
        }

        let hasReaction = innerComment.reactions.some(x => x.user.id === currentUser.id);

        if (hasReaction) {
            innerComment.reactions = innerComment.reactions.filter((x) => x.user.id !== currentUser.id);
        }

        innerComment.reactions = innerComment.reactions.concat({ isLike: false, user: currentUser });

        hasReaction = innerComment.reactions.some(x => x.user.id === currentUser.id);

        return this.commentService.addReaction(reaction).pipe(
            map(() => innerComment),
            catchError(() => {
                innerComment.reactions = hasReaction
                    ? innerComment.reactions.filter((x) => x.user.id !== currentUser.id)
                    : innerComment.reactions.concat({ isLike: false, user: currentUser });

                return of(innerComment);
            })
        );
    }
}