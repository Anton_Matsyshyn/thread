import { Injectable } from '@angular/core';
import { HttpInternalService } from './http-internal.service';
import { NewComment } from '../models/comment/new-comment';
import { Comment } from '../models/comment/comment';
import { NewReaction } from '../models/reactions/newReaction';
import { User } from '../models/user';

@Injectable({ providedIn: 'root' })
export class CommentService {
    public routePrefix = '/api/comments';

    constructor(private httpService: HttpInternalService) { }

    public createComment(comment: NewComment) {
        return this.httpService.postFullRequest<Comment>(`${this.routePrefix}`, comment);
    }

    public addReaction(reaction: NewReaction) {
        return this.httpService.postFullRequest<Comment>(`${this.routePrefix}/reaction`, reaction);
    }

    public getUsersWhoLiked(commentId: number) {
        return this.httpService.getRequest<User[]>(`${this.routePrefix}/reaction/like/users/${commentId}`);
    }

    public changeVisability(commentId: number) {
        return this.httpService.deleteRequest<Comment>(`${this.routePrefix}/${commentId}`, null);
    }

    public updateComment(comment: Comment) {
        return this.httpService.putRequest<Comment>(`${this.routePrefix}`, comment);
    }
}
