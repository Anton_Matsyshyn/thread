import { Injectable } from '@angular/core';
import { HttpInternalService } from './http-internal.service';
import { Post } from '../models/post/post';
import { NewReaction } from '../models/reactions/newReaction';
import { NewPost } from '../models/post/new-post';

@Injectable({ providedIn: 'root' })
export class PostService {
    public routePrefix = '/api/posts';

    constructor(private httpService: HttpInternalService) { }

    public getPosts() {
        return this.httpService.getFullRequest<Post[]>(`${this.routePrefix}`);
    }

    public createPost(post: NewPost) {
        return this.httpService.postFullRequest<Post>(`${this.routePrefix}`, post);
    }

    public updatePost(post: Post) {
        return this.httpService.putRequest<Post>(`${this.routePrefix}`, post);
    }

    public changeVisability(id: number) {
        return this.httpService.deleteRequest<Post>(`${this.routePrefix}/${id}`);
    }

    public addReaction(reaction: NewReaction) {
        return this.httpService.postFullRequest<Post>(`${this.routePrefix}/reaction`, reaction);
    }

    public sharePost(targetEmail: string, postId: number) {
        return this.httpService.postRequest<any>(`${this.routePrefix}/share/${targetEmail}/${postId}`, null);
    }
}
